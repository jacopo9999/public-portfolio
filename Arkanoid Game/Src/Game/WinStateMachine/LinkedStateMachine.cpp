#include "LinkedStateMachine.h"

//It calls an event for the actual state
void LinkedStateMachine::eventHappens(const char* eventoStr){
	unsigned int posizione;
	for(	posizione=0;
		(strcmp(currentState->events.at(posizione)->name, eventoStr) != 0) && (posizione < (states.size()));
		posizione++
		){}
	if(posizione< states.size() ){
		currentState->outFunction();
		currentState = currentState->events.at(posizione)->destination;
		currentState->inFunction();
	}
}

//It calls an event for the actual state
void LinkedStateMachine::eventHappens(Event* evento){
	unsigned int posizione;
	//controllo che effettivamente l'evento ci sia
	for(	posizione=0;
		(currentState->events.at(posizione)!=evento) && posizione < states.size();
		posizione++
		){}
	if(posizione<states.size()){
		currentState->outFunction();
		currentState=evento->destination;
		currentState->inFunction();
	}
}

//Adds a state to the state machine
void LinkedStateMachine::addState(State* state){
	states.push_back(state);
}

//Returns the actual state of the state machine
State* LinkedStateMachine::getState(const char* stateStr) const{
	unsigned int posizione;
	for(posizione = 0 ; (strcmp(states.at(posizione)->name, stateStr) != 0) && posizione < states.size() ; posizione++){}
	if(posizione<states.size()){
		return states.at(posizione);
	}
	return NULL;
}

//Set the actual state of the stateMachine (Used only for the initial state)
void LinkedStateMachine::setState(State* state){
	this->currentState = state;
}

//Set the actual state of the stateMachine (Used only for the initial state)
void LinkedStateMachine::setState(const char* stateStr){
	this->currentState = getState(stateStr);
}

//Adds an event to the state machine
void LinkedStateMachine::addEvent(const char* nome, State* origin, State* destination){
	//l'ownership dell'evento creato viene passata allo stato origin, ci pensa il distruttore di State a distruggerlo
	Event* evento = new Event(nome,origin,destination);
	origin->events.push_back(evento);
}

