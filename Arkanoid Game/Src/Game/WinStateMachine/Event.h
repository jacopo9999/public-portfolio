#pragma once

class State;

class Event{
private:
	Event(const Event&) {}
	Event& operator=(const Event&) {}

public:
	const char* name;
	State* origin;
	State* destination;

	Event(const char* name, State* origin, State* destination) : name(name), origin(origin), destination(destination)
	{}
	~Event() {}
};
