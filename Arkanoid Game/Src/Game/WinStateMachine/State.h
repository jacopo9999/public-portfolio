#pragma once

#include "Event.h"
#include "../WinContainers/WinContainers.h"

class State{
public:
	const char* name;
	LinkedList<Event*> events;

	State(const State& other) : name(other.name)
	{
		for(unsigned int i = 0; i < other.events.size(); ++i){
			events.push_back
				(new Event( 
							other.events.at(i)->name,
							this,
							other.events.at(i)->destination)
				);
		}
	}

	State(const char* nomeStato) : name(nomeStato)
	{
	}

	virtual ~State(){
		for(unsigned int i=0; i< events.size(); i++){
			SAFE_DELETE(events.at(i));
		}
	}
	
	virtual void addEvent(const char* eventoStr, State* destination);

	//Function called when the machine came in this state
	virtual void inFunction(){}

	//Function called when the machine exit from this state
	virtual void outFunction(){}
};
