#pragma once

#include "../WinDraw/String.h"

#include "State.h"
#include "Event.h"

class LinkedStateMachine
{
public:
	LinkedList<State*> states;
	State* currentState;
	
	LinkedStateMachine(): currentState(NULL){}

	LinkedStateMachine(const LinkedStateMachine& other)													 
	{
		for(unsigned int i = 0; i < other.states.size(); ++i){
			states.push_back(other.states.at(i));
		}
		currentState = states.at( other.states.positionOf(other.currentState) );
	}

	~LinkedStateMachine()
	{}

	void eventHappens(Event* i_evento);
	void eventHappens(const char* i_eventoStr);
	void setState(State* ow_state);
	void setState(const char* stateStr);
	void addState(State* ow_state);
	State* getState(const char* i_stateStr) const;

	void addEvent(const char* i_evento, State* i_origin, State* i_destination);

};
