#pragma once
#include "X2DRectangle.h"

enum CollisionMargin {
	UP,
	DOWN,
	LEFT,
	RIGHT,
};

class X2DEventListener
{
public:
	X2DEventListener(void) {};

	~X2DEventListener(void)	{};

	/* Called when a dynamic object collides with another object (either fixed or dynamic) */
	virtual void collisionWithObject(X2DRectangle *movingObj, X2DRectangle *otherObj) {}

	/* Called when a dynamic object collides with a scene margin */
	virtual void collisionWithMargin(X2DRectangle *movingObj, CollisionMargin sceneMargin) {}


protected:
	X2DEventListener(const X2DEventListener& other) {}
	X2DEventListener& operator=(const X2DEventListener& other) {}

};
