#pragma once
#include <math.h>

class X2DVector
{
public:

	float x, y;

	X2DVector() : x(0), y(0)
	{}

	X2DVector(const float x, const float y) : x(x), y(y)
	{}

	~X2DVector()
	{}

	X2DVector(const X2DVector& other) : x(other.x), y(other.y)
	{}

	X2DVector& operator=(const X2DVector& other)
	{
		if (this == &other)
			return *this;

		x = other.x;
		y = other.y;

		return *this;
	}

	float magnitude() { return sqrtf(x*x + y*y); }

	void set(const float x, const float y) { this->x = x; this->y = y; }
	void setX(const float x) { this->x = x; }
	void setY(const float y) { this->y = y; }
	
	void inverseX() { x = -x; }
	void inverseY() { y = -y; }
	void inverseXY() { x = -x; y = -y; }
	
	void normalize()
	{
		float magnitude = sqrt(x*x + y*y);
		this->x = x / magnitude;
		this->y = y / magnitude;
	}

	/* Operations with other vectors or scalars */
	X2DVector plus(const X2DVector &other) { return (X2DVector(x + other.x, y + other.y)); }
	X2DVector minus(const X2DVector &other) { return (X2DVector(x - other.x, y - other.y)); }
	float dotProduct(const X2DVector &other) { return (x * other.x) + (y * other.y); }
	X2DVector scale(const float scalar) { return X2DVector(x * scalar, y * scalar); }
	void add(const X2DVector &other) { this->x += other.x, this->y += other.y; }
	void sub(const X2DVector &other) { this->x -= other.x, this->y -= other.y; }
};
