#pragma once
#include <math.h>
#include "X2DRectangle.h"
#include "X2DEventListener.h"

enum SceneState
{
	RESUMED,
	PAUSED,
	STOPPED
};

class X2DScene : public X2DRectangle
{
private:
	LinkedList<X2DRectangle*> sceneObjects;
	SceneState actualState;
	X2DEventListener *eventListener;

	/* Manages collision between 2 rectangles by checking and eventually applying collision effects. */
	void manageCollisionWithObject(X2DRectangle &movingObj, X2DRectangle &other);

	/* Checks collision between 2 rectangles */
	bool checkRectCollision(X2DRectangle &movingObject, X2DRectangle &otherObject);

	/* Checks two collided objects and returns witch side collided */
	CollisionMargin checkRectCollisionDirection(X2DRectangle &movingObject, X2DRectangle &otherObject);

	/* Applies collision effects to collided dynamic objects by modifying their speed vectors */
	void applyDynamicCollision(X2DRectangle &movingObj, X2DRectangle &otherObj);

	/* Applies the effect of a collision with a fixed object to the specified dynamic obj */
	void applyFixedCollision(X2DRectangle &obj, CollisionMargin collidedSide);

	/* Apply a special collision effect (SPECIALRADIAL behaviour) that bounces obj radially. */
	void applyRadialCollision(X2DRectangle &obj, X2DRectangle &radial);

	/* Manage collisions with scene bounds */
	void manageCollisionWithBounds(X2DRectangle &gameObject);

	/* Returns true if the specified area field is free */
	bool isFreePosition(const float x, const float y, const float width, const float height);

	/* Check for object intersection */
	bool checkIntersection(X2DRectangle &movingObj, X2DRectangle &otherObj);
	
	/* Free a dynamic object from an intersection, aligning it to the other object */
	bool fixCollisionPosition(X2DRectangle &movingObj, X2DRectangle &otherObj);

public:

	// constructor
	X2DScene(const int xpos, const int ypos, const int width, const int height, X2DEventListener *eventListener, const char* filename = NULL, const float alpha = 1.0f) :
		X2DRectangle(xpos, ypos, width, height, FIXEDRECT, filename, alpha, SCENE), eventListener(eventListener)
	{}

	// destructor
	~X2DScene(void)
	{}

	/* Register an event listener, engine messages (collision events) will be sent to this class */
	void registerEventListener(X2DEventListener *listener) {
		eventListener = listener;
	}

	virtual void onRender(const Bitmap* renderTarget) 
	{
		for(unsigned int i = 0; i < sceneObjects.size(); i++)
		{
			sceneObjects.at(i)->onRender(renderTarget);
		}
	}

	/* Add a new object to the scene */
	void addObject(X2DRectangle *gameObject) { sceneObjects.push_back(gameObject); }

	const LinkedList<X2DRectangle*>* getSceneObjectsRef() const { return &sceneObjects; }

	/* Update scene managing collisions and transforming dynamic objects */
	void updatePhysics(const float dt);

	void updateScreen();

	/* Scene states management */
	void pause() { actualState = PAUSED; }
	void resume() { actualState = RESUMED; }
	void stop() { actualState = STOPPED; }

	SceneState getState() const { return actualState; }


private:
	X2DScene(const X2DScene& other) : X2DRectangle(other) {}
	X2DScene& operator=(const X2DScene& other) {}

};
