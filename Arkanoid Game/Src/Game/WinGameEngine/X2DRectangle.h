#pragma once
#include "../WinUIWidgets/WinUIWidgets.h"
#include "../WinContainers/LinkedList.h"
#include "X2DVector.h"

 //default speed vector
#define DEFAULT_SPEED 0.0f, 0.0f
#define DEFAULT_MASS 1
#define DEFAULT_TYPE COLLIDER

/* Enum for implementing different collision behaviours */
enum collisionBehaviour
{
	/* Fixed behaviour. This Object won't move by collision reactions (still can programmatically). */
	FIXEDRECT,
	/* Dynamic behaviour. This Object can be moved by collision reactions. */
	DYNAMICRECT,
	/* Special behaviour. This Object won't move by collision reactions and bounces other objects radially. */
	SPECIALRADIAL,
};

enum Type
{
	/* The object can not collide */
	NO_COLLIDER,
	/* The object can collide */
	COLLIDER,
	/* The rectangle is a scene */
	SCENE
};

class X2DRectangle : public UIWidget
{
protected:
	collisionBehaviour collBehaviour;
	Type type;
	X2DVector speed;
	X2DVector position;
	int mass;
	bool moving;

public:
	X2DRectangle(const int xpos,
				 const int ypos,
				 const int width,
				 const int height,
				 collisionBehaviour behaviour,
				 const char* filename,
				 const float alpha = 1.0f,
				 int mass = DEFAULT_MASS,
				 Type type = COLLIDER) :
		UIWidget(xpos, ypos, filename, width, height, alpha),
			speed(DEFAULT_SPEED),
			position((float)xpos, (float)ypos),
			collBehaviour(behaviour),
			mass(mass),
			type(type),
			moving(true)
	{}

	X2DRectangle(const int xpos,
			const int ypos,
			const int width,
			const int height,
			collisionBehaviour behaviour,
			const float alpha = 1.0f,
			int mass = DEFAULT_MASS,
			Type type = DEFAULT_TYPE) :
		UIWidget(xpos, ypos, width, height, alpha),
			speed(DEFAULT_SPEED),
			position((float)xpos, (float)ypos),
			collBehaviour(behaviour),
			mass(mass),
			type(type),
			moving(true)
		{}

	virtual ~X2DRectangle()
	{}

	X2DRectangle(const X2DRectangle& other) : UIWidget(other),
		position(other.position),
		collBehaviour(other.collBehaviour),
		type(other.type),
		speed(other.speed),
		mass(other.mass),
		moving(other.moving)
	{}

	X2DRectangle& operator=(const X2DRectangle& other)
	{
		if (this == &other)
			return *this;

		UIWidget::operator=(other);
		collBehaviour = other.collBehaviour;
		position = other.position;
		type = other.type;
		speed = other.speed;
		mass = other.mass;
		moving = other.moving;

		return *this;
	}

	void setEnabled(const bool enabled) { this->enabled = enabled; }

	void toggleMoving() { moving = !moving; }

	/* Update position according to actual speed vector */
	virtual void updatePhysics(const float dt)
	{
		if (moving) {
			position.set(position.x + speed.x*dt, position.y + speed.y*dt);
		}
	}

	void updateScreen()
	{
		setX(int(position.x));
		setY(int(position.y));
	}

	void bounceHorizontally() { speed.inverseX(); }
	void bounceVertically() { speed.inverseY(); }
	void bounceDiagonally() { speed.inverseXY(); }

	/* set position by specifying its center coords */
	void setPositionByCenter(const float centralX, const float centralY) { position.set(centralX - getWidth() * 0.5f, centralY - getHeight() * 0.5f); }

	/* set position by specifying its x,y components */
	void setPositionX(const float x) { position.setX(x); }
	void setPositionY(const float y) { position.setY(y); }

	/* offset position by dx, dy */
	void offsetBy(const float dx, const float dy) { position.x += dx, position.y += dy; }

	/* set speed vector by specifying its x,y components */
	void setSpeed(const float x, const float y) { speed.set(x, y); }
	void setSpeedX(const float x) { speed.setX(x); }
	void setSpeedY(const float y) { speed.setY(y); }

	/* change rect size with specified values */
	void setSize(const int width, const int height) { this->setWidth(width), this->setHeight(height); }

	/* change actual collision behaviour */
	void setCollBehaviour(collisionBehaviour collBehaviour) { this->collBehaviour = collBehaviour; }
	const collisionBehaviour getCollBehaviour() const { return collBehaviour; }

	void setMass(int mass) { this->mass = mass; }

	/* Getters */

	/* get a pointer to position. Coords are top-left relative */
	inline X2DVector getPosition() const { return position; }

	/* get a vector containing center coords */
	inline X2DVector getCentralPosVector() const { return X2DVector(getCenterX(), getCenterY()); }

	/* get a pointer to speed vector */
	inline X2DVector getSpeed() const { return speed; }

	inline int getMass() const { return mass; }

	inline float getCenterX() const { return position.x + getWidth() * 0.5f; }
	inline float getCenterY() const { return position.y + getHeight() * 0.5f; }
	inline float getXSpeed() const { return speed.x; }
	inline float getYSpeed() const { return speed.y; }

	inline float getLeftMargin() const { return position.x; }
	inline float getUpperMargin() const { return position.y; }
	inline float getRightMargin() const { return position.x + getWidth(); }
	inline float getLowerMargin() const { return position.y + getHeight(); }

	inline Type getType() const
	{
		return type;
	}
};

