#include "X2DScene.h"

void X2DScene::updatePhysics(const float dt)
{
	if (actualState == RESUMED) {
		for(unsigned int i = 0; i < sceneObjects.size(); i++) {
			X2DRectangle *obj = sceneObjects.at(i);

			// check for objects collisions (test only dynamic objects with others)
			if (obj->getCollBehaviour() == DYNAMICRECT) {

				obj->updatePhysics(dt);

				//test with other objects
				for(unsigned int j = 0; j < sceneObjects.size(); j++) {
					if (j != i) {
						manageCollisionWithObject(*obj, *sceneObjects.at(j));						
					}
				}
				//manage collisions with scene margins
				manageCollisionWithBounds(*obj);
			}
		}
	}
}


void X2DScene::updateScreen()
{
	for (unsigned int i = 0; i < sceneObjects.size(); i++) {
		sceneObjects.at(i)->updateScreen();
	}
}


void X2DScene::manageCollisionWithObject(X2DRectangle &movingObj, X2DRectangle &other)
{
	// checks for collision
	if (other.isEnabled() && checkRectCollision(movingObj, other)) {

		//report collision to the listener
		eventListener->collisionWithObject(&movingObj, &other);

		//apply collision effects
		switch (other.getCollBehaviour())
		{
		case DYNAMICRECT:
			applyDynamicCollision(movingObj, other);
			break;

		case FIXEDRECT:
			applyFixedCollision(movingObj, checkRectCollisionDirection(movingObj, other));
			break;

		case SPECIALRADIAL:
			applyRadialCollision(movingObj, other);
			break;

		default:
			break;
		}
	}
}


bool X2DScene::checkRectCollision(X2DRectangle &movingObj, X2DRectangle &otherObj)
{
	//checks for same vertical range
	if (movingObj.getLowerMargin() > otherObj.getUpperMargin() && movingObj.getUpperMargin() < otherObj.getLowerMargin()) {
		//checks for same horizontal range
		if (movingObj.getRightMargin() > otherObj.getLeftMargin() && movingObj.getLeftMargin() < otherObj.getRightMargin())
		{
			/* intersection detected, move object at collision position */
			X2DVector direction = movingObj.getSpeed();
			direction.normalize();
			direction.inverseXY();
			movingObj.offsetBy(direction.x, direction.y);

			while(checkIntersection(movingObj, otherObj))
			{
				movingObj.offsetBy(direction.x, direction.y);
			}
			
			return true;
		}
	}
	return false;
}


CollisionMargin X2DScene::checkRectCollisionDirection(X2DRectangle &movingObj, X2DRectangle &otherObj)
{
	//compare hx and wy resulting from the Minkowski sum to define collision side
	float w = float(movingObj.getWidth()) + float(otherObj.getWidth());
	float h = float(movingObj.getHeight()) + float(otherObj.getHeight());
	float wy = w * (movingObj.getCenterY() - otherObj.getCenterY());
	float hx = h * (movingObj.getCenterX() - otherObj.getCenterX());

	if (wy > hx) {
		if (wy > -hx) {
			return UP;
		}
		else {
			return LEFT;
		}
	}
	else {
		if (wy > -hx) {
			return RIGHT;
		}
		else {
			return DOWN;
		}
	}
}


void X2DScene::manageCollisionWithBounds(X2DRectangle &obj)
{
	if (obj.getLeftMargin() < getLeftMargin()) {
		eventListener->collisionWithMargin(&obj, LEFT);
		obj.setPositionX(getLeftMargin());
		obj.bounceHorizontally();
	}
	else if (obj.getUpperMargin() < getUpperMargin()) {
		eventListener->collisionWithMargin(&obj, UP);
		obj.setPositionY(getUpperMargin());
		obj.bounceVertically();
	}
	else if (obj.getRightMargin() > getRightMargin()) {
		eventListener->collisionWithMargin(&obj, RIGHT);
		obj.setPositionX(getRightMargin() - obj.getWidth());
		obj.bounceHorizontally();
	}
	else if (obj.getLowerMargin() > getLowerMargin()) {
		eventListener->collisionWithMargin(&obj, DOWN);
		obj.setPositionY(getLowerMargin() - obj.getHeight());
		obj.bounceVertically();
	}
	else {
		//no collision
	}
}


void X2DScene::applyDynamicCollision(X2DRectangle &movingObj, X2DRectangle &otherObj)
{
	//normalized vectors along the axis connecting the objects
	X2DVector movingAxis = movingObj.getCentralPosVector().minus(otherObj.getCentralPosVector());
	X2DVector otherAxis = otherObj.getCentralPosVector().minus(movingObj.getCentralPosVector());
	movingAxis.normalize();
	otherAxis.normalize();

	//speed component along the axis before and after collision
	float movingSpeedComponentInit, otherSpeedComponentInit;
	float movingSpeedComponentFinal, otherSpeedComponentFinal;

	//dot product the normalized vector along the axis and the speed vector to get the speed component along the axis of collision
	movingSpeedComponentInit = movingObj.getSpeed().dotProduct(movingAxis);
	otherSpeedComponentInit = otherObj.getSpeed().dotProduct(otherAxis);

	movingObj.setSpeed(movingObj.getXSpeed() - movingAxis.x*movingSpeedComponentInit, movingObj.getYSpeed() - movingAxis.y*movingSpeedComponentInit);
	otherObj.setSpeed(otherObj.getXSpeed() - otherAxis.x*otherSpeedComponentInit, otherObj.getYSpeed() - otherAxis.y*otherSpeedComponentInit);

	//speed after collision using 1D collision calculations
	movingSpeedComponentFinal = (movingSpeedComponentInit * (movingObj.getMass()-otherObj.getMass()) + 2*otherObj.getMass()*otherSpeedComponentInit) / (movingObj.getMass()+otherObj.getMass());
	otherSpeedComponentFinal = (otherSpeedComponentInit * (otherObj.getMass()-movingObj.getMass()) + 2*movingObj.getMass()*movingSpeedComponentInit) / (movingObj.getMass()+otherObj.getMass());

	//multiply this by the vector along the axis to get speed vector along the axis
	movingAxis = movingAxis.scale(movingSpeedComponentFinal);
	otherAxis = otherAxis.scale(otherSpeedComponentFinal);

	//add it back to the original speed vector
	movingObj.setSpeed(movingObj.getXSpeed() - movingAxis.x, movingObj.getYSpeed() - movingAxis.y);
	otherObj.setSpeed(otherObj.getXSpeed() - otherAxis.x, otherObj.getYSpeed() - otherAxis.y);
}


void X2DScene::applyFixedCollision(X2DRectangle &obj, CollisionMargin collisionSide)
{
	if(collisionSide == LEFT || collisionSide == RIGHT) {
		obj.bounceHorizontally();
	}
	else if(collisionSide == UP || collisionSide == DOWN) {
		obj.bounceVertically();
	}
	else {
		obj.bounceDiagonally();
	}
}


void X2DScene::applyRadialCollision(X2DRectangle &obj, X2DRectangle &radial)
{
	//object's speed magnitude
	float movingSpeed = obj.getSpeed().magnitude();

	//obect's position relative to the center of the radial object (normalized by objects widths)
	float relPosX = (obj.getCenterX() - radial.getCenterX()) / (radial.getWidth() + obj.getWidth());
	
	/* Calculate X speed proportional to the ball speed and it's relative position, rescaled by an influence factor.
	Notice that influence factor must be less than 2 so that speedX would be less than the overall speed */
 	obj.setSpeedX(movingSpeed * relPosX * 1.96f);

	//calculate new Y speed based on the resulting X
	obj.setSpeedY(sqrt(movingSpeed*movingSpeed - obj.getXSpeed()*obj.getXSpeed()) * (obj.getYSpeed() > 0? -1 : 1));
}


bool X2DScene::isFreePosition(const float x, const float y, const float width, const float height)
{
	float leftMargin = x;
	float rightMargin = x + width;
	float upperMargin = y;
	float lowerMargin = y + height;

	for(unsigned int i = 0; i < sceneObjects.size(); i++){
		X2DRectangle *obj = sceneObjects.at(i);

		//checks for same vertical range
		if (obj->getLowerMargin() > upperMargin && obj->getUpperMargin() < lowerMargin) {
			//checks for same horizontal range
			if (obj->getRightMargin() > leftMargin && obj->getLeftMargin() < rightMargin) {
				return false;
			}
		}
	}

	return true;
}


bool X2DScene::checkIntersection(X2DRectangle &movingObj, X2DRectangle &otherObj)
{
	//checks for same vertical range
	if (movingObj.getLowerMargin() > otherObj.getUpperMargin() && movingObj.getUpperMargin() < otherObj.getLowerMargin()) {
		//checks for same horizontal range
		if (movingObj.getRightMargin() > otherObj.getLeftMargin() && movingObj.getLeftMargin() < otherObj.getRightMargin())
		{
			return true;
		}
	}
	return false;
}


bool X2DScene::fixCollisionPosition(X2DRectangle &movingObj, X2DRectangle &otherObj)
{
	X2DVector direction = movingObj.getSpeed();
	direction.normalize();
	direction.inverseXY();
	movingObj.offsetBy(direction.x, direction.y);

	while(checkIntersection(movingObj, otherObj))
	{
		movingObj.offsetBy(direction.x, direction.y);
	}

	return true;
}