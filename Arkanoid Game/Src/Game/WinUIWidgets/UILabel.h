#pragma once

#include "UIWidget.h"
#include "UIFont.h"

/*A label displays text using a specified font, labels are not interactive objects*/
class UILabel : public UIWidget
{
private:
	UIFont * font; //The text format
	String text; //The label text

	/*COPY CONSTRUCTOR/COPY ASSIGNMENT OPERATOR ARE INHERITED*/

public:
	//Constructor
	UILabel(const int x, 
			const int y, 
			UIFont* font,
			const String text = "",
			const float alpha = 1.f) : 
		font(font),
		text(text),
		UIWidget(x, y, NULL, font->getCharWidth() * text.size(), font->getCharHeight() * text.getLines().size(), alpha)
	{}

	//Destructor
	virtual ~UILabel(void)
	{}

	//Getters:
	const String getText() const { return text; }
	UIFont* getFont() const { return font; }

	//Setters:
	void setText(String text) { this->text = text; }

	void setEnabled(const bool enabled) { /* Do nothing */ } //Set the label enabled, this method has no effect
	void onRender(const Bitmap* renderTarget); 
};