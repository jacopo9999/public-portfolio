#pragma once

#include "../WinContainers/WinContainers.h"

#include "UIWidget.h"
#include "Defines.h"

#include <iostream>

/*A frame is a widgets container, it can contain more than one widget, all the properties changed for a frame are automatically changed for all the
contained widgets and sub-frames, all the actions and render events are automatically forwarded to all the contained widgets and sub-frames. Frames
cannot be copied*/
class UIFrame : public UIWidget
{
private:
	LinkedList<UIWidget*> widgets; //The list of contained widgets

	//Copy constructor - PRIVATE
	UIFrame(const UIFrame& other) : UIWidget(other) {}

	//Copy assignment operator - PRIVATE
	UIFrame& operator=(const UIFrame& other) {}

public:
	//Constructor 
	UIFrame(const unsigned int xpos, 
			const unsigned int ypos, 
			const unsigned int width, 
			const unsigned int height,
			const float alpha = 1.f,
			String basePath = "") : 
		UIWidget(xpos, ypos, basePath, width, height, alpha)
	{}

	//Destructor
	virtual ~UIFrame(void) 
	{
	}

	void addWidget(UIWidget* widget); //Add a new widget to the container/frame

	void removeWidgets(); //Empty the frame, removing all its widgets

	void onRender(const Bitmap* renderTarget);
	void onMouseInput(const int mousePosX, const int mousePosY, const MouseKey key, const KeyStatus status);
	void onKeyInput(const Key key, const KeyStatus status);

	void setVisible(const bool visible);
	void setEnabled(const bool enabled);

	void removeChildWidgets();
};

