﻿#pragma once

#include "../WinContainers/WinContainers.h"

#include "UIWidget.h"
#include "UIFrame.h"

#include <iostream>

/*The UIWindow is the base class for any GUI-based application, it is a frames container and cannot contain GUI elements directly. In order to respond
to the user input the window must contain at least one frame*/
class UIWindow : public MainWindow, public EventHandler
{
protected:
	LinkedList<UIFrame*> frames; //The frames of the window

	//Copy constructor - PRIVATE
	UIWindow(const UIWindow& other) {}

	//Copy assignment operator - PRIVATE
	UIWindow& operator=(const UIWindow& other) {}

public:

	//Constructor
	UIWindow() : MainWindow()
	{
		SetEventHandler(this);
	}

	//Destructor
	virtual ~UIWindow()
	{}

	void addFrame(UIFrame* frame);

	void onKeyInput(const Key key, const KeyStatus status); 
	void onMouseInput(const int mousePosX, const int mousePosY, const MouseKey key, const KeyStatus status);
	void onRender(const Bitmap* renderTarget); 
};
