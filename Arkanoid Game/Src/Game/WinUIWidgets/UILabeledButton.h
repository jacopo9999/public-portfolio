#pragma once

#include "UIFrame.h"
#include "UIButton.h"
#include "UILabel.h"
#include "UIColor.h"
#include "UIFont.h"
#include "UILayout.h"

/*The labeled button is a standard button with an associated label. The label appears centered in the button and its size is automatically
adapted to the button size. The labeled button is a frame/container. Labeled buttons are not copyable, they must be re-initialized using
a copy of their contained objects (the button and the label)*/
class UILabeledButton : public UIFrame
{
private:
	UILabel *buttonLabel; //The label object
	UIButton *button; //The button object

	void create(const int x,
		const int y,
		const unsigned int width,
		const unsigned int height,
		const String text,
		UIFont* font,
		const float alpha = 1.f);

	/*COPY/ASSIGNMENT ARE NOT ALLOWED BECAUSE THE PARENT CLASS IS NOT COPYABLE*/

public:
	//Constructor
	UILabeledButton(const int x,
					const int y,
					const unsigned int width,
					const unsigned int height,
					char* text,
					UIFont* font,
					const float alpha = 1.f)  : 
		UIFrame(x, y, width, height, alpha, NULL)
	{
		create(x, y, width, height, text, font, alpha);
	}

	//Destructor
	virtual ~UILabeledButton(void)
	{
		SAFE_DELETE(buttonLabel);
		SAFE_DELETE(button);
	}

	UILabel* getLabel() const {return buttonLabel;}

	//Set a mouse action for the labeled button
	void setOnMouseAction(const MouseKey key,
		const KeyStatus status,
		void(*mouseAction)(const MouseKey, const KeyStatus, const int, const int))
	{
		button->setOnMouseAction(key, status, mouseAction);
	}
};

