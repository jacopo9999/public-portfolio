#include "UIRadioButtonsController.h"

//Add a check box (or radio button) to the controller
void UIRadioButtonsController::addRadioButton(UICheckBox* radioButton)
{
	radioButton->controller = this; //Set this as the button controller
	radioButton->id = radioButtons.size(); //Set the ID to recognise the action on the button
	radioButtons.push_back(radioButton);
	radioButton->setChecked(false); //Uncheck the added checkbox
}

//Check the check box (or radio button) at the specified index
void UIRadioButtonsController::checkAt(const unsigned int index)
{
	assert(index < radioButtons.size());
	this->selectedCheckBoxIndex = index;
	for(unsigned int i = 0; i < radioButtons.size(); i++)
	{
		UICheckBox *radioButton = radioButtons.at(i);
		if(i == this->selectedCheckBoxIndex)
		{
			radioButton->setChecked(true); //Check the selected radio button
		}
		else
		{
			radioButton->setChecked(false); //Uncheck all the other radio buttons
		}
	}
}