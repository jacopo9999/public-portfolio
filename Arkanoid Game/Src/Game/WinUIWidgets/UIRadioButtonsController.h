#pragma once

#include "UICheckBox.h"

class UICheckBox;

/*The Radio Buttons Controller is a class which controls a set of UICheckBox-es, it is not a widget nor a frame, but check boxes can be added to it. When
some of them are inside the controller, only one can be checked at a time*/
class UIRadioButtonsController
{
private:
	LinkedList<UICheckBox*> radioButtons; //The radio buttons controlled by the controller, the controller has not the ownership of them
	unsigned int selectedCheckBoxIndex; //The number of the check box which is currently checked

	/*COPY CONSTRUCTOR/COPY ASSIGNMENT OPERATOR ARE INHERITED*/

public:
	//Constructor
	UIRadioButtonsController() :
		selectedCheckBoxIndex(0)
	{}

	//Destructor
	virtual ~UIRadioButtonsController()
	{}

	void addRadioButton(UICheckBox* radioButton);
	void checkAt(const unsigned int index);
};

