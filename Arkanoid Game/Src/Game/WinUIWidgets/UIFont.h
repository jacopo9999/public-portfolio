#pragma once

#include "../WinDraw/WinDraw.h"

#include "UIColor.h"

/*UIFont defines the format for a text, i.e. its size and style*/
class UIFont : public Font
{
private:
	const unsigned int cHeight, //The height of the characters
					   cWidth, //The width of the characters
					   cWeight; //The weight of the characters
	const bool italic, //Whether the string is italic or not
			   underline, //Whether the string is underlined or not
			   strikeOut; //Whether the string is striked out or not
	UIColor fontColor; //The color of the Font

public:
	//Constructor
	UIFont(const unsigned int cWidth,
		   const unsigned int cHeight, 	 
		   const unsigned int cWeight = 8,
		   const bool italic = false,
		   const bool underline = false,
		   const bool strikeOut = false,
		   const UIColor fontColor = UIColor(0,0,0)) :
		Font(cWidth, cHeight, cWeight, italic, underline, strikeOut),
		cHeight(cHeight),
		cWidth(cWidth),
		cWeight(cWeight),
		italic(italic),
		underline(underline),
		strikeOut(strikeOut),
		fontColor(fontColor)
	{}

	//Destructor
	~UIFont() {}

	//Getters:
	inline const unsigned int getCharHeight() const { return cHeight; };
	inline const unsigned int getCharWidth() const { return cWidth; }
	inline const unsigned int getCharWeight() const { return cWeight; }
	inline const bool isItalic() const { return italic; }
	inline const bool isUnderline() const { return underline; }
	inline const bool isStrikeOut() const { return strikeOut; }
	inline const UIColor getFontColor() const { return fontColor; }

};

