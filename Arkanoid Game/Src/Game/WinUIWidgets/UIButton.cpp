
#include "UIButton.h"

//Executes all the actions associated to this (key, status) mouse combination
void UIButton::onMouseInput(int mouseX, int mouseY, MouseKey key, KeyStatus status)
{
	if(isVisible() && isEnabled())
	{
		if(status == KeyPressed)
		{
			setCurrentBitmapIndex(isEnabled(), true);
		}
		if(status == KeyReleased)
		{
			for(unsigned int i = 0; i < UIWidget::getMouseActions().size(); i++)
			{
				if(UIWidget::getMouseActions().at(i).isEqual(key))
				{
					UIWidget::getMouseActions().at(i).action(key, status, mouseX, mouseY);
				}
				setCurrentBitmapIndex(isEnabled(), false);
			}
		}
	}
}

//Init the button
void UIButton::create(const String basePath)
{
	String enabledImageFilename = basePath + FILEN_APPEND_ENABLED;
	String disabledImageFilename = basePath + FILEN_APPEND_DISABLED;
	String pressedImageFilename = basePath + FILEN_APPEND_PRESSED;

	addBitmap(enabledImageFilename.get()); //0 is enabled
	addBitmap(disabledImageFilename.get()); //1 is disabled
	addBitmap(pressedImageFilename.get()); //2 is pressed

	setCurrentBitmapIndex(isEnabled(), false);
}

//Set the button as enabled/disabled, if it is enabled it will react to the user input
void UIButton::setEnabled(const bool enabled)
{
	this->enabled = enabled;
	setCurrentBitmapIndex(enabled, false);
}

//Set the index of the bitmap which will be displayed (change the rendered widget image)
void UIButton::setCurrentBitmapIndex(const bool enabled, const bool pressed)
{
	if(!enabled)
	{
		UIWidget::setCurrentBitmapIndex(BITMAP_DISABLED); 
	}
	else
	{
		if(pressed)
		{
			UIWidget::setCurrentBitmapIndex(BITMAP_PRESSED); 
		}
		else
		{
			UIWidget::setCurrentBitmapIndex(BITMAP_ENABLED); 
		}
	}
}