
#include "UICheckBox.h"

//Executes all the actions associated to this (key, status) mouse combination
void UICheckBox::onMouseInput(const int mousePosX, const int mousePosY, const MouseKey key, const KeyStatus status)
{
	if(isVisible() && isEnabled())
	{
		if(status == KeyStatus::KeyPressed)
		{
			if(controller && id != NO_BITMAP)
			{
				controller->checkAt(id);
			}
			else
			{
				if(isChecked())
					setChecked(false);
				else
					setChecked(true);
			}		
		}
	}
}

//Checks the check box and executes the associated action
void UICheckBox::setChecked(const bool checked) 
{
	this->checked = checked;
	setCurrentBitmapIndex(isEnabled(), checked);
	if(checked)
	{
		for(unsigned int i = 0; i < checkedBoxActions.size(); i++)
		{
			checkedBoxActions.at(i)();
		}
	}
	else
	{
		for(unsigned int i = 0; i < uncheckedBoxActions.size(); i++)
		{
			uncheckedBoxActions.at(i)();
		}
	}
}

//Checks the check box and executes the associated action
void UICheckBox::setEnabled(const bool enabled) 
{
	this->enabled = enabled;
	setCurrentBitmapIndex(enabled, checked);
}

//Set the index of the bitmap which will be displayed (change the rendered widget image)
void UICheckBox::setCurrentBitmapIndex(const bool enabled, const bool checked)
{
	if(enabled)
	{
		if(checked)
		{
			UIWidget::setCurrentBitmapIndex(BITMAP_CHECKED_ENABLED);
		}
		else
		{
			UIWidget::setCurrentBitmapIndex(BITMAP_UNCHECKED_ENABLED);
		}
	}
	else
	{
		if(checked)
		{
			UIWidget::setCurrentBitmapIndex(BITMAP_CHECKED_DISABLED); 
		}
		else
		{
			UIWidget::setCurrentBitmapIndex(BITMAP_UNCHECKED_DISABLED); 
		}
	}
}

//Init the check box
void UICheckBox::create(String basePath,
						const bool checked /*= false*/)
{
	this->checked = checked;

	String checkedImageFilename = basePath + FILEN_APPEND_ENA_CHECKED;
	String uncheckedImageFilename = basePath + FILEN_APPEND_ENA_UNCHECKED;		
	String checkedDisabledImageFilename = basePath + FILEN_APPEND_DIS_CHECKED;
	String uncheckedDisabledImageFilename = basePath + FILEN_APPEND_DIS_UNCHECKED;

	addBitmap(checkedImageFilename.get()); //0 is checked/enabled
	addBitmap(uncheckedImageFilename.get()); //1 is unchecked/enabled
	addBitmap(checkedDisabledImageFilename.get()); //2 is checked/disabled
	addBitmap(uncheckedDisabledImageFilename.get()); //3 is unchecked/disabled

	setCurrentBitmapIndex(isEnabled(), checked);
}

//Set the action which will be executed when checking the check box
void UICheckBox::setOnCheckedAction(functionPtr checkAction)
{
	checkedBoxActions.push_back(checkAction);
}

//Set the action which will be executed when un-checking the check box
void UICheckBox::setOnUncheckedAction(functionPtr uncheckAction)
{
	uncheckedBoxActions.push_back(uncheckAction);
}