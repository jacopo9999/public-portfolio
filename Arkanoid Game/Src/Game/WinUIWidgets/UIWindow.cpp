#include "UIWindow.h"

//Add a new frame to the container, at least one frame must be added for the window to receive input from the user
void UIWindow::addFrame(UIFrame* frame)
{
	this->frames.push_back(frame);
}

//Called when rendering the widget
void UIWindow::onRender(const Bitmap* renderTarget)
{
	//Forward the onRender function to the contained frames:
	for(unsigned int i = 0; i < frames.size(); i++)
	{
		frames.at(i)->onRender(renderTarget);
	}
}

//Executes all the actions associated to this (key, status) keyboard combination
void UIWindow::onKeyInput(const Key key, const KeyStatus status)
{
	//Forward the onMouseInput function to the contained frames:
	for(unsigned int i = 0; i < frames.size(); i++)
	{
		frames.at(i)->onKeyInput(key, status);
	}
}

//Executes all the actions associated to this (key, status) mouse combination
void UIWindow::onMouseInput(const int mousePosX, const int mousePosY, const MouseKey key, const KeyStatus status)
{
	//Forward the onMouseInput function to the contained frames:
	for(unsigned int i = 0; i < frames.size(); i++)
	{
		frames.at(i)->onMouseInput(mousePosX, mousePosY, key, status);
	}
}

