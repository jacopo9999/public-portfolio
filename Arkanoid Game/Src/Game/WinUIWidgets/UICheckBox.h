#pragma once

#include "UIWidget.h"
#include "UIRadioButtonsController.h"
#include "Defines.h"

#define BITMAP_CHECKED_ENABLED 0
#define BITMAP_UNCHECKED_ENABLED 1
#define BITMAP_CHECKED_DISABLED 2
#define BITMAP_UNCHECKED_DISABLED 3

class UIRadioButtonsController;

typedef void (*functionPtr)(void);

/*The check box is a GUI element which can react to the mouse input received from the user. It can be disabled/enabled and checked/unchecked, 
the actions are typically executed when checking/unchecking it. The check box can be connected to a radio buttons controller (UIRadioButtonsController)
in this case it will be handled together with other check boxes by the controller*/
class UICheckBox : public UIWidget
{
private:
	bool checked;

	//Radio Controller Data
	friend class UIRadioButtonsController;
	UIRadioButtonsController *controller;
	int id;

	LinkedList<functionPtr> checkedBoxActions; //The actions that are executed when the box is checked
	LinkedList<functionPtr> uncheckedBoxActions; //The actions that are executed when the box is unchecked

	void setCurrentBitmapIndex(const bool enabled, const bool checked);
	void create(String basePath, const bool checked = false);

	/*COPY CONSTRUCTOR/COPY ASSIGNMENT OPERATOR ARE INHERITED*/

public:
	//Constructor
	UICheckBox(const int x, 
			   const int y, 
			   const unsigned int width,
		       const unsigned int height,
			   const float alpha = 1.f,
			   const String basePath = "..\\Resources\\UICheckBox\\checkbox",
		       const bool checked = false) :
		UIWidget(x, y, width, height, alpha),
		checked(checked),
		controller(NULL),
		id(NO_BITMAP)
	{
		create(basePath, checked);
	}

	//Destructor
	virtual ~UICheckBox() {}

	//Getters 
	bool isChecked() const { return checked; }

	//Setters
	void setChecked(const bool checked);
	void setEnabled(const bool enabled); //Overriding UIWidget::setEnabled(bool)

	void setOnCheckedAction(functionPtr checkAction);
	void setOnUncheckedAction(functionPtr uncheckAction);

	void onMouseInput(const int mousePosX, const int mousePosY, const MouseKey key, const KeyStatus status);
};

