#include "UIFrame.h"

//Add a new widget to a frame
void UIFrame::addWidget(UIWidget* widget)
{
	//It checks if the pointer of the widget is in the list. If it is already in it's not added any more
	bool isIn = false;
	for(unsigned int i = 0; i < widgets.size(); ++i)
	{
		if(widget == widgets.at(i))
		{
			isIn = true;
			break;
		}
	}
	if(!isIn)
	{
		this->widgets.push_back(widget);
	}
}

//Set the frame and all the sub-objects visible/invisible
void UIFrame::setVisible(bool visible)
{
	UIWidget::setVisible(visible);
	for(unsigned int i = 0; i < widgets.size(); i++)
	{
		widgets.at(i)->setVisible(visible);
	}
}

//Set the frame and all the sub-objects enabled/disabled
void UIFrame::setEnabled(bool enabled)
{
	this->enabled = enabled;
	for(unsigned int i = 0; i < widgets.size(); i++)
	{
		widgets.at(i)->setEnabled(enabled);
	}
}

//Render the frame and all its sub-objects
void UIFrame::onRender(const Bitmap* renderTarget)
{
	if(isVisible()) 
	{
		getDisplayedBitmap()->DrawTo(renderTarget, getWidth(), getHeight(), getX(), getY());
		for(unsigned int i = 0; i < widgets.size(); i++)
		{
			widgets.at(i)->onRender(renderTarget);
		}
	}
}

//Forward the mouse input to the frame sub-objects
void UIFrame::onMouseInput(const int mousePosX, const int mousePosY, const MouseKey key, const KeyStatus status)
{
	if(isVisible() && isEnabled())
	{
		for(unsigned int i = 0; i < widgets.size(); i++)
		{
			const unsigned int minX = widgets.at(i)->getX();
			const unsigned int maxX = minX + widgets.at(i)->getWidth();
			const unsigned int minY = widgets.at(i)->getY();
			const unsigned int maxY = minY + widgets.at(i)->getHeight();
			if(unsigned(mousePosX) >= minX &&
			   unsigned(mousePosX) <= maxX &&
			   unsigned(mousePosY) >= minY &&
			   unsigned(mousePosY) <= maxY)
			{
				widgets.at(i)->onMouseInput(mousePosX, mousePosY, key, status);
				//return;   Re-add this if we want the action to be catched only by the item which was added first (the one on top).
			}
		}
	}
}

//Forward the keyboard input to the frame sub-objects
void UIFrame::onKeyInput(const Key key, const KeyStatus status)
{
	if(isVisible() && isEnabled())
	{
		for(unsigned int i = 0; i < widgets.size(); i++)
		{
			widgets.at(i)->onKeyInput(key, status);
		}
	}
}

//Remove all the widgets into the frame (other frames as well as sub-objects)
void UIFrame::removeWidgets()
{
	//this clear() is enough, the ownership of the widgets is of Level.cpp:
	widgets.clear();
}