#pragma once
#include "uiwidget.h"

//This class represents a rectangular widget which is part of the game (i.e. ball, bat, brick, etc.)
class UIGameWidget : public UIWidget
{
public:
	UIGameWidget(const int xpos, const int ypos, const char* filename) :
		UIWidget(xpos, ypos, filename)
	{}
	~UIGameWidget(void)
	{}
};

