#pragma once

#include "../WinContainers/WinContainers.h"

#include "UIWidget.h"
#include "Defines.h"
#include <stdio.h>

#define BITMAP_ENABLED 0
#define BITMAP_DISABLED 1
#define BITMAP_PRESSED 2

/*The button is a GUI element which can react to the mouse input received from the user. It can be disabled/enabled and pressed/unpressed*/
class UIButton : public UIWidget
{
private:
	void create(const String basePath);
	void setCurrentBitmapIndex(const bool enabled, const bool pressed);

	Timer deactivationTimer; //The time after which the buttons becomes unpressed

	/*COPY CONSTRUCTOR/COPY ASSIGNMENT OPERATOR ARE INHERITED*/

public:
	//Constructors
	UIButton(const unsigned int x, 
			 const unsigned int y, 
			 const unsigned int width,
			 const unsigned int height,
			 const float alpha = 1.f,
			 const String basePath = "..\\Resources\\UIButton\\button") :
		UIWidget(x, y, width, height, alpha)
	{
		create(basePath);
	}
	UIButton(const unsigned int x, 
			 const unsigned int y, 
			 const float alpha = 1.f,
			 const String basePath = "..\\Resources\\UIButton\\button") :
		UIWidget(x, y, 0, 0, alpha)
	{
		create(basePath);
	}

	//Destructor
	virtual ~UIButton()
	{}

	void onMouseInput(const int mouseX, const int mouseY, const MouseKey key, const KeyStatus status);

	//Getters

	//Setters
	void setEnabled(const bool enabled);
};
