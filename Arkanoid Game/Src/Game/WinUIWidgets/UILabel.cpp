
#include "UILabel.h"

//Override onRender from UIWidget, this function is called when drawing the widget
void UILabel::onRender(const Bitmap* renderTarget)  
{
	if(isVisible())
	{
		LinkedList<String> lines = text.getLines();
		for(unsigned int i = 0; i < lines.size(); i++)
		{
			const_cast<Bitmap*>(renderTarget)->RenderText(getX() - font->getCharWidth()/2, 
														  getY() + i*font->getCharHeight(),
														  lines.at(i),
														  font,
														  font->getFontColor().red(),
														  font->getFontColor().green(),
														  font->getFontColor().blue());
		}

	}
}