#pragma once

/*UILayout defines a set of margins, horizontal and vertical*/
class UILayout
{
private:
	const unsigned int horMargin; //The horizontal margin
	const unsigned int verMargin; //The vertical margin

public:
	//Constructor
	UILayout(const unsigned int horMargin,
			 const unsigned int verMargin) :
		horMargin(horMargin),
		verMargin(verMargin)
	{}

	//Destructor
	~UILayout() {}
};