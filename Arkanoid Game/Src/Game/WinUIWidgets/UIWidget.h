#pragma once

#include "../WinDraw/WinDraw.h"
#include "../WinContainers/WinContainers.h"

#include <assert.h>

#include "Defines.h"

#define NO_BITMAP -1

class UIKeyAction;
class UIMouseAction;

/*The UIWidget is the base abstract class for all the GUI elements, it contains all the information common to all the widgets. To create
a new widget it is necessary to create subclass and override the onRender, onKeyInput and onMouseInput methods which are used respectively
to draw the widget and handle the user input. Also other methods can be optionally overridden to customize the widget behavior*/
class UIWidget : public EventHandler
{
private:
	LinkedList<Bitmap*> bitmaps; //The list of the bitmaps which can be displayed for the widget
	float alpha; //The alpha channel of the widget (of all of its bitmaps)
	bool externalBitmap;
	int x, y; //Position
	int currentBitmapIndex; //The index of the bitmap currently displayed (NO_BITMAP means no bitmaps are displayed)
	bool visible; //Whether the widget is visible (rendered) or not
	unsigned int height, width; //Max. size of the widget

	LinkedList<UIMouseAction> mouseActions; //The list of mouse actions associated with the widget
	LinkedList<UIKeyAction> keyActions; //The list of keyboard actions associated with the widget

protected:
	bool enabled; //Whether the input reacts to the user input or not

	//Constructors
	UIWidget(const int x, 
		     const int y, 
			 const unsigned int width,
			 const unsigned int height,
			 const float alpha = 1.f);
	UIWidget(const int x, 
			 const int y, 
			 const String filename,
			 const unsigned int width, 
			 const unsigned int height,
			 const float alpha = 1.f);

	//Copy constructor
	UIWidget(const UIWidget& other);

	//Copy assignment operator
	UIWidget& operator=(const UIWidget& other);

	void addBitmap(const String filename);
	inline void addBitmapsList(const LinkedList<const String> filenames);

public:
	//Destructor
	virtual ~UIWidget(void);

	//Getters:
	inline const int getX() const { return this->x; }
	inline const int getY() const { return this->y; }
	inline const unsigned int getHeight() const { return this->height; }
	inline const unsigned int getWidth() const { return this->width; }
	inline const unsigned int getIndex() const { return this->currentBitmapIndex; }
	inline Bitmap* getDisplayedBitmap() const { return bitmaps.at(currentBitmapIndex); }
	inline const bool isVisible() const { return this->visible; }
	inline const bool isEnabled() const { return this->enabled; }
	inline const float getAlpha() const { return this->alpha; }
	inline const int getCurrentBitmapIndex() const { return this->currentBitmapIndex; }
	inline const LinkedList<UIMouseAction> getMouseActions() const {return this->mouseActions; } 
	inline const LinkedList<UIKeyAction> getKeyActions() const {return this->keyActions; } 

	//Setters:
	inline void setX(const int x) { this->x = x; }
	inline void setY(const int y) { this->y = y; }
	inline void setIndex(const unsigned int currentBitmapIndex) { this->currentBitmapIndex = currentBitmapIndex; }
	inline void setVisible(const bool visible) { this->visible = visible; }
	virtual void setEnabled(const bool enabled) = 0; //When overriding specify: this->enabled = enabled and remember to change the displayed bitmap
	inline void setHeight(const unsigned int height) { this->height = height; }
	inline void setWidth(const unsigned int width) { this->width = width; }
	inline void setSize(const unsigned int width, const unsigned int height) { this->width = width; this->height = height; }
	inline void setCurrentBitmapIndex(const int index) { this->currentBitmapIndex = index; }

	//Set a new key action for this widget, associated to a particular (key, status) keyboard input
	void setOnKeyAction(const Key key,
		const KeyStatus status,	
		void(*keyAction)(const Key, const KeyStatus));

	//Set a new key action for this widget, associated to a particular (key, status) mouse input
	void setOnMouseAction(const MouseKey key,
		const KeyStatus status,
		void(*mouseAction)(const MouseKey, const KeyStatus, const int mouseX, const int mouseY));

	void setBitmap(Bitmap* bitmap);

	//Overridable methods:
	inline virtual void onRender(const Bitmap* renderTarget);
	inline virtual void onKeyInput(const Key key, const KeyStatus status);
	inline virtual void onMouseInput(const int mousePosX, const int mousePosY, const MouseKey key, const KeyStatus status);
};

//The class represents an action which depends on a particular combination (key, status) from the mouse input
class UIMouseAction
{
private:
	MouseKey key; //The mouse key 
	KeyStatus status; //The status of the key (pressed, released...)
	unsigned int mouseX; //The X coord of the mouse
	unsigned int mouseY; //The Y coord of the mouse

public:
	void (*action)(const MouseKey, const KeyStatus, const int mouseX, const int mouseY); //The pointer to the mouse action

	//Constructors
	UIMouseAction() {}
	UIMouseAction(const MouseKey key,
				  const KeyStatus status,
				  void(*action)(const MouseKey, const KeyStatus, const int mouseX, const int mouseY)) :
		key(key),
		status(status),
		mouseX(mouseX),
		mouseY(mouseY),
		action(action)
	{}

	//Destructor
	~UIMouseAction()
	{}

	//Copy constructor
	UIMouseAction(const UIMouseAction& otherAction) : 
		key(otherAction.key),
		status(otherAction.status),
		mouseX(otherAction.mouseX),
		mouseY(otherAction.mouseY),
		action(otherAction.action)
	{}

	//Copy assignment operator
	UIMouseAction& operator= (const UIMouseAction &otherAction) 
	{
		if (this != &otherAction)
		{
			key = otherAction.key;
			status = otherAction.status;
			mouseX = otherAction.mouseX;
			mouseY = otherAction.mouseY;
			action = otherAction.action;
		}
		return *this;
	}

	//Check whether the couple key-status is the same as this one
	inline bool isEqual(const MouseKey otherKey, const KeyStatus otherStatus) const
		{return (key == otherKey && status == otherStatus); }

	//Check whether the key in a couple key-status is the same as this one
	inline bool isEqual(const MouseKey otherKey) const
	{return (key == otherKey); }
};

//The class represents an action which depends on a particular combination (key, status) from the keyboard input
class UIKeyAction
{
private:
	Key key; //The keyboard key
	KeyStatus status; //The status of the key (pressed, released...)

public:
	void (*action)(const Key, const KeyStatus); //The pointer to the key action

	//Constructors
	UIKeyAction() {}
	UIKeyAction(const Key key, const KeyStatus status, void(*action)(const Key, const KeyStatus)) :
		key(key),
		status(status),
		action(action)
	{}

	//Destructor
	~UIKeyAction()
	{}

	//Copy constructor
	UIKeyAction(const UIKeyAction& otherAction) : 
		key(otherAction.key),
		status(otherAction.status),
		action(otherAction.action)
	{}

	//Copy assignment operator
	UIKeyAction& operator= (const UIKeyAction &otherAction) 
	{
		if (this != &otherAction)
		{
			key = otherAction.key;
			status = otherAction.status;
			action = otherAction.action;
		}
		return *this;
	}

	//Check whether the couple key-status is the same as this one
	bool isEqual(const Key otherKey, const KeyStatus otherStatus) const
		{return (key == otherKey && status == otherStatus); }

};