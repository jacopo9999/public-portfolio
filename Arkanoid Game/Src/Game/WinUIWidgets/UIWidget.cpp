#include "UIWidget.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Constructors

//Create an empty widget with the specified size, all the inserted bitmaps will be resized to it
UIWidget::UIWidget(const int x, const int y, const unsigned int width, const unsigned int height, const float alpha /*= 1.f*/) : 
	x(x),
	y(y),
	currentBitmapIndex(NO_BITMAP),
	visible(true),
	enabled(true),
	alpha(alpha),
	height(height),
	width(width),
	externalBitmap(false)
{}

//Create a widget with the specified image and size, all the inserted bitmaps will be resized to this size
UIWidget::UIWidget(const int x, const int y, const String filename, const unsigned int width,  const unsigned int height, const float alpha /*= 1.f*/) : 
	x(x), 
	y(y),
	width(width),
	height(height),
	currentBitmapIndex(0),
	visible(true),
	enabled(true),
	alpha(alpha),
	externalBitmap(false)
{
	Bitmap* bitmap;
	if( strcmp(filename.get(),"") != 0 )
	{
		bitmap = BitmapCache::getInstance()->getBitmap(filename, width, height, alpha);
	}
	else
	{
		bitmap = BitmapCache::getInstance()->getBitmap("", width, height, alpha);
	}
	bitmaps.push_back(bitmap);
}

//Copy constructor
UIWidget::UIWidget(const UIWidget& other) : 
	x(other.x), 
	y(other.y),
	currentBitmapIndex(other.currentBitmapIndex),
	visible(other.visible),
	enabled(other.enabled),
	width(other.width),
	height(other.height),
	bitmaps(other.bitmaps),
	alpha(other.alpha),
	externalBitmap(other.externalBitmap)
{}

//Copy assignment operator
UIWidget& UIWidget::operator=(const UIWidget& other)
{
	if (this == &other)
		return *this;

	x = other.x;
	y = other.y;
	currentBitmapIndex = other.currentBitmapIndex;
	visible = other.visible;
	enabled = other.enabled;
	width = other.width;
	height = other.height;
	bitmaps = other.bitmaps;
	alpha = other.alpha;		
	externalBitmap = other.externalBitmap;

	return *this;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Destructor
UIWidget::~UIWidget(void) 
{
	bitmaps.clear();
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Called when rendering the widget
void UIWidget::onRender(const Bitmap* renderTarget) 
{
	if(visible && currentBitmapIndex != NO_BITMAP)
	{
		bitmaps.at(currentBitmapIndex)->DrawTo(renderTarget, width, height, x, y);
	}
}

//Add a new bitmap to the widget bitmaps
void UIWidget::addBitmap(const String filename)
{	
	Bitmap* bitmap = BitmapCache::getInstance()->getBitmap(filename, alpha);
	bitmaps.push_back(bitmap);
}

//Add the bitmaps into an existing list to the widget bitmaps list
void UIWidget::addBitmapsList(const LinkedList<const String> filenames)
{
	for(unsigned int i = 0; i < filenames.size(); i++)
	{
		addBitmap(filenames.at(i));
	}
}

//Set a keyboard action for the widget
void UIWidget::setOnKeyAction(const Key key,
							  const KeyStatus status,	
							  void(*keyAction)(const Key, const KeyStatus))
{
	UIKeyAction newAction(key, status, keyAction);
	for(unsigned int i = 0; i < keyActions.size(); i++)
	{
		if(keyActions.at(i).isEqual(key, status))
		{
			keyActions.at(i) = newAction;
			return;
		}
	}
	this->keyActions.push_back(newAction);
}

//Set a mouse action for the widget
void UIWidget::setOnMouseAction(const MouseKey key,
								const KeyStatus status,
								void(*mouseAction)(const MouseKey, const KeyStatus, const int mouseX, const int mouseY))
{
	UIMouseAction newAction(key, status, mouseAction);
	for(unsigned int i = 0; i < mouseActions.size(); i++)
	{
		if(mouseActions.at(i).isEqual(key, status))
		{
			mouseActions.at(i) = newAction;
			return;
		}
	}
	this->mouseActions.push_back(newAction);
}

//Set the current bitmap (the one which is displayed) for the widget
void UIWidget::setBitmap(Bitmap* bitmap)
{
	if(!externalBitmap)
	{
		bitmaps.push_front(bitmap);
		currentBitmapIndex=0;
		externalBitmap=true;
	}
	else
	{
		if(bitmap!=NULL)
		{
			bitmaps.setAt(bitmap, currentBitmapIndex);
		}
	}
}

//Executes all the actions associated to this (key, status) keyboard combination
void UIWidget::onKeyInput(const Key key, const KeyStatus status)
{
	for(unsigned int i = 0; i < keyActions.size(); i++)
	{
		if(keyActions.at(i).isEqual(key, status))
		{
			keyActions.at(i).action(key, status);
		}
	}
}

//Executes all the actions associated to this (key, status) mouse combination
void UIWidget::onMouseInput(const int mouseX, const int mouseY, const MouseKey key, const KeyStatus status)
{
	for(unsigned int i = 0; i < mouseActions.size(); i++)
	{
		if(mouseActions.at(i).isEqual(key, status))
		{
			mouseActions.at(i).action(key, status, mouseX, mouseY);
		}
	}
}

