#pragma once

#include "..//WinDraw//WinDraw.h"

#include "UIWidget.h"
#include "Defines.h"

/*The animated widget is a widget which changes the displayed bitmap in a cyclic way (with or without a loop)*/
class UIAnimatedWidget : virtual public UIWidget, virtual public Animation
{
private:
	String animationFilename; //The name of the animation bitmap file

public:

	//Constructors
	UIAnimatedWidget(const int x,
					 const int y,
					 const unsigned int frameWidth,
					 const unsigned int frameHeight,
					 const float frameTime, //Seconds
					 const String animationFilename,
					 const bool looping = false,
					 const float alpha = 1.f) :
		animationFilename(animationFilename),
		Animation(BitmapCache::getInstance()->getBitmap(animationFilename , alpha), frameWidth, frameHeight, frameTime, looping),
		UIWidget(x, y, animationFilename, true, true, alpha)
	{}

	//Destructor
	virtual ~UIAnimatedWidget()
	{}

	//Copy constructor
	UIAnimatedWidget(const UIAnimatedWidget& other) : 
		UIWidget(other),
		Animation(other),
		animationFilename(animationFilename)
	{}

	//Copy assignment operator
	UIAnimatedWidget& operator=(const UIAnimatedWidget& other)
	{
		if (this == &other)
			return *this;

		UIWidget::operator=(other);
		Animation::operator=(other);
		animationFilename = other.animationFilename;

		return *this;
	}

	//Overridden from UIWidget. Called when drawing the widget
	virtual void onRender(const Bitmap* renderTarget) 
	{
		if(isVisible())
		{
			Draw(renderTarget, getX(), getY());
		}
	}
};

