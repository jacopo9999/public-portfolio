#pragma once

//Bitmap files default extension
#define FILEN_BITMAP_EXTENSION ".tga"

//Checkbox/RadioButton states
#define FILEN_APPEND_ENA_CHECKED "_checked"
#define FILEN_APPEND_ENA_UNCHECKED "_unchecked"
#define FILEN_APPEND_DIS_CHECKED "_disabled_checked"
#define FILEN_APPEND_DIS_UNCHECKED "_disabled_unchecked"

//Button States
#define FILEN_APPEND_ENABLED ""
#define FILEN_APPEND_DISABLED "_disabled"
#define FILEN_APPEND_PRESSED "_pressed"