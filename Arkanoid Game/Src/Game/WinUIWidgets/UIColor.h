#pragma once

/*UIColor defines a color, i.e. a red-green-blue structure*/
class UIColor
{
private:
	unsigned char mRed, mGreen, mBlue; //Red-Green-Blue colors

public:

	//Constructor
	UIColor(const unsigned char red, 
		    const unsigned char green, 
		    const unsigned char blue) :
		mRed(red),
		mGreen(green),
		mBlue(blue)
	{}

	//Destructor
	~UIColor() {}

	//Getters:
	const unsigned char red() const {return mRed;}
	const unsigned char green() const {return mGreen;}
	const unsigned char blue() const {return mBlue;}
};
