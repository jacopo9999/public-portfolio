
#include "UILabeledButton.h"
#include "UIColor.h"

//Init the labeled button
void UILabeledButton::create(const int x,
							 const int y,
							 const unsigned int width,
							 const unsigned int height,
							 const String text,
							 UIFont* font,
							 const float alpha /*= 1.f*/)
{
	//Change the width/height of the label to adapt them to the button size
	const unsigned int labelWidth = font->getCharWidth() * text.size();
	const unsigned int labelHeight = font->getCharHeight() * text.getLines().size();
	const unsigned int verMargin = (height - labelHeight) / 2;
	const unsigned int horMargin = (width - labelWidth) / 2 - labelWidth/10;

	buttonLabel = new UILabel(x + horMargin, 
							  y + verMargin,	
							  font,
							  text,
							  alpha);
	button = new UIButton(x, y, width, height);
	addWidget(button); 
	addWidget(buttonLabel); 
}