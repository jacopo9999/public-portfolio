#pragma once

#include "../WinDraw/WinDraw.h"
#include "../WinUIWidgets/Defines.h"
#include "../WinContainers/VectorList.h"

/*
Singleton class which will contain all the Bitmaps used in game
If the Bitmap is new it will be loaded from HD, else it returns the pointer to the already loaded bitmap.
*/

class BitmapCache
{
private:
	//Simple storage class for the bitmap and his path
	class Image
	{
	public:
		Bitmap* image;
		String path;

		Image(String path, Bitmap* image):path(path), image(image){}
		~Image(){ SAFE_DELETE(image); }
	};

	static BitmapCache* instance;
	static VectorList<BitmapCache::Image*>* bitmaps;

	BitmapCache(void){}
	BitmapCache(const BitmapCache&){}
	BitmapCache& operator= (const BitmapCache&){}

public:
	~BitmapCache(void){}

	//return the instance of this singleton
	static BitmapCache* getInstance()
	{
		if (instance == nullptr)
		{
			static VectorList<BitmapCache::Image*> newBitmaps(25);
			bitmaps = &newBitmaps;
			static BitmapCache self;
			instance = &self;
		}
		return instance;
	}

	//Search in the list of loaded bitmaps by path, if it's not already loads it will take it from HD and it's put in the list
	static Bitmap* getBitmap(const String imagePath, const float alpha = 1.f);
	static Bitmap* getBitmap(const String fileName, const unsigned int width, const unsigned int height, const float alpha = 1.f);

	//Free the memory deleting all the loaded bitmaps
	static void cleanResources();

};

