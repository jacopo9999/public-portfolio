#ifndef ANIMATION_H
#define ANIMATION_H

#include "Timer.h"
#include "Bitmap.h"

class Animation
{
protected: 
	Timer timer; 
	Bitmap* bitmap; 
	struct Frame
	{
		int x; 
		int y; 
		int w; 
		int h; 
		float frameTime; 
	}; 
	Frame* frames; 
	int frameCount;

	int currentFrame; 
	bool complete; 
	bool looping; 

	bool UpdateAndCheckCompletion(); 
	Animation(const Animation&) {/*animations cannot be copied*/}
public: 
	//constructor
	Animation(Bitmap* bmp,
			  const unsigned int frameWidth,
			  const unsigned int frameHeight,
			  const unsigned int* indices,
			  const unsigned int indicesCount,
			  const float frameTime,
			  const bool looping = false); 
	Animation(Bitmap* bmp,
			  const unsigned int frameWidth,
			  const unsigned int frameHeight,
			  const float frameTime,
			  const bool looping = false);
	~Animation(); 
	
	void Start(const bool loop = false);  //Starts animation playback
	void Pause(const bool paused = true);  //Put the animation on pause
	void Draw(const Bitmap* renderTarget, 
			  const unsigned int x, 
			  const unsigned int y); // updates and draws an animation based on time at (x, y) onto the renderTarget. 
	bool IsComplete(); //true if an animation is complete. 
}; 

#endif ANIMATION_H