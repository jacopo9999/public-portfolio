
#include "Timer.h"
#include "Bitmap.h"
#include "Animation.h"

bool Animation::UpdateAndCheckCompletion()
{
	float currentTime = timer.TimeFromStart(); 
	for(int i=currentFrame; i<frameCount; i++) 
	{
		if(frames[i].frameTime > currentTime) 
		{
			currentFrame=i; 
			complete = false; 
			return false; 
		}
	}
	currentFrame = frameCount-1; 
	complete = true; 
	return true; 
}

Animation::Animation(Bitmap* bmp,
				     const unsigned int frameWidth,
					 const unsigned int frameHeight,
					 const float frameTime,
					 const bool looping) :
	bitmap(bmp), currentFrame(0), complete(false), looping(looping)
{
	const unsigned int indices[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 
		12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22}; 
	const unsigned int indicesCount = sizeof(indices) / sizeof(int); 
	int bmw = bitmap->GetWidth(); 
	int fx = bmw / frameWidth; 
	frameCount = indicesCount; 
	frames = new Frame[indicesCount]; 
	float animationTime = frameTime; 
	for(int i=0; i<frameCount; i++) 
	{
		int xIndex = (indices[i] % fx); 
		int yIndex = (indices[i] / fx); 
		frames[i].x = xIndex * frameWidth; 
		frames[i].y = yIndex * frameHeight; 
		frames[i].w = frameWidth; 
		frames[i].h = frameHeight; 
		frames[i].frameTime = animationTime; 
		animationTime+=frameTime; 
	}
}

Animation::Animation(Bitmap* bmp, 
					 const unsigned int frameWidth, 
					 const unsigned int frameHeight, 
					 const unsigned int* indices, 
					 const unsigned int indicesCount, 
					 const float frameTime,
					 const bool looping) : 
		bitmap(bmp), currentFrame(0), complete(false), looping(looping)
{
	int bmw = bitmap->GetWidth(); 
	int fx = bmw / frameWidth; 
	frameCount = indicesCount; 
	frames = new Frame[indicesCount]; 
	float animationTime = frameTime; 
	for(int i=0; i<frameCount; i++) 
	{
		int xIndex = (indices[i] % fx); 
		int yIndex = (indices[i] / fx); 
		frames[i].x = xIndex * frameWidth; 
		frames[i].y = yIndex * frameHeight; 
		frames[i].w = frameWidth; 
		frames[i].h = frameHeight; 
		frames[i].frameTime = animationTime; 
		animationTime+=frameTime; 
	}
}

// Starts animation playback
void Animation::Start(const bool loop)
{
	currentFrame = 0; 
	looping = loop; 
	complete = false; 
	timer.Reset(); 
}

void Animation::Pause(const bool paused)
{
	timer.Pause(paused);
}

// updates and draws an animation based on time at (x, y) onto the renderTarget. 
void Animation::Draw(const Bitmap* renderTarget, const unsigned int x, const unsigned int y) 
{
	if(UpdateAndCheckCompletion() && looping) 
	{
		currentFrame = 0; 
		timer.Reset(); 
	}
	Frame* f = &frames[currentFrame]; 
	bitmap->DrawRectTo(renderTarget, x, y, f->x, f->y, f->w, f->h); 
}

//true if an animation is complete. 
bool Animation::IsComplete()
{
	return complete; 
}

Animation::~Animation()
{
	SAFE_DELETE_ARRAY(frames); 
}
