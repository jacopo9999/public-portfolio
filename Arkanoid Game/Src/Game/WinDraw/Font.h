#ifndef WINDRAW_FONT_H
#define WINDRAW_FONT_H

#include "Bitmap.h"
#include <wingdi.h>

class Font
{
protected:

	HFONT hFont;

	//font handler is accessible from these functions
	friend void Bitmap::RenderText(const int x,
								   const int y,
								   const String text,
								   const Font* font,
								   const unsigned char colorR,
								   const unsigned char colorG,
								   const unsigned char colorB); //draws text on this bitmap
	friend void Bitmap::RenderText(const int x,
								   const int y,
								   const unsigned int w,
								   const unsigned int h,
								   const String text,
								   const Font* font,
								   const unsigned char colorR,
								   const unsigned char colorG,
								   const unsigned char colorB); //draws text on this bitmap, in a box

public:

	Font(const Font&) {}
	Font& operator=(const Font&) {}

	Font(const unsigned int cWidth,
		const unsigned int cHeight, 	 
		const unsigned int cWeight,
		const bool italic,
		const bool underline,
		const bool strikeOut) :
	hFont(CreateFontA(cHeight, cWidth, 0, 0, cWeight, italic, underline, strikeOut, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, ""))
	{}

	virtual ~Font()
	{
		if(hFont)
		{
			DeleteObject(hFont);
		}
	}

	bool IsValid() const
	{
		return hFont != NULL;
	}
};

#endif