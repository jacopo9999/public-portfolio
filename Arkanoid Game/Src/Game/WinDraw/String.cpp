#include "String.h"

//Constructor
String::String()
{
	content = (char*)malloc(1);
	strcpy_s(content, strlen("") + 1, "");
}

//Creates a string from a characters sequence
String::String(const char* charPtr)
{
	if(charPtr == NULL)
	{
		charPtr = "";
	}
	content = (char*)malloc(1 + strlen(charPtr));
	strcpy_s(content, strlen(charPtr) + 1, charPtr);
}

//Copy constructor
String::String(const String& other) 
{
	content = (char*)malloc(1 + strlen(other.content));
	strcpy_s(content, strlen(other.content) + 1, other.content);
}

//Assign a string (copy assignement operator)
String& String::operator=(const String& other) {
	char* newContent = (char*)realloc(content, 1 + strlen(other.content));
	if(newContent != NULL)
	{
		strcpy_s(newContent, strlen(other.content) + 1, other.content);
	}
	else
	{
		std::cout << "ERROR: String.h(operator=) : realloc failed!" << std::endl;
		free(content);
		exit(1);
	}
	content = newContent;
	return *this;
}

//Append two strings
String String::operator+(const String& other) const
{         
	String result;
	result.content = (char*)malloc(1 + strlen(content) + strlen(other.content));
	memset(result.content, 0, sizeof(result.content));
	strcpy_s(result.content, strlen(this->content) + 1, this->content);
	strcat_s(result.content, strlen(this->content) + strlen(other.content) + 1, other.content);
	return result;
}

//Append a string to an existing string
String& String::operator+=(const String& other)
{
	char* newContent = (char*)realloc(content, 1 + strlen(this->content) + strlen(other.content));
	if(newContent != NULL)
	{
		strcpy_s(newContent, strlen(this->content) + 1, this->content);
		strcat_s(newContent, strlen(this->content) + strlen(other.content) + 1, other.content);
	}
	else
	{
		std::cout << "ERROR: String.h(operator+=) : realloc failed!" << std::endl;
		free(content);
		exit(1);
	}
	content = newContent;
	return *this;
}

//Destructor
String::~String() 
{
	if(content != NULL)
	{
		free(content);
	}
}

//Compare the string with another string, return 0 whether the two strings are equal (like strcmp)
const int String::compare(const String otherString) const
{
	return strcmp(this->get(), otherString.get());
}

//Split the string at each newline and returns a list of the lines.
LinkedList<String> String::getLines() const
{
	LinkedList<String> lines;
	char *next = NULL;
	char *copy = (char*)malloc(1 + strlen(content));
	strcpy_s(copy, strlen(content) + 1, content);
	char *pch;
	pch = strtok_s(copy, "\n", &next);
	while (pch != NULL)
	{
		lines.push_back(String(pch));
		pch = strtok_s(NULL, "\n", &next);
	}

	free(copy);
	return lines;
}

