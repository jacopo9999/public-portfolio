#include "BitmapCache.h"

BitmapCache* BitmapCache::instance = nullptr;

VectorList<BitmapCache::Image*>* BitmapCache::bitmaps = nullptr;

Bitmap* BitmapCache::getBitmap(const String imagePath, const unsigned int width, const unsigned int height, const float alpha)
{
	for(unsigned int i = 0; i < bitmaps->size(); ++i)
	{
		if(bitmaps->at(i)->path.compare(imagePath) == 0){
			return bitmaps->at(i)->image;
		}
	}

	BitmapCache::Image* newImage;

	if(imagePath.compare(""))
	{
		String path = imagePath + FILEN_BITMAP_EXTENSION;
		newImage = new BitmapCache::Image( imagePath, new Bitmap(path.get(), width, height, alpha) );
	}
	else
	{
		newImage = new BitmapCache::Image("", new Bitmap(1,1));
	}

	bitmaps->push_back(newImage);

	return bitmaps->back()->image;
}


Bitmap* BitmapCache::getBitmap(const String imagePath, const float alpha)
{
	for(unsigned int i = 0; i < bitmaps->size(); ++i)
	{
		if(bitmaps->at(i)->path.compare(imagePath) == 0){
			return bitmaps->at(i)->image;
		}
	}

	BitmapCache::Image* newImage;

	if(imagePath.compare(""))
	{
		String path = imagePath + FILEN_BITMAP_EXTENSION;
		newImage = new BitmapCache::Image( imagePath, new Bitmap(path.get(), alpha) );
	}
	else
	{
		newImage = new BitmapCache::Image("", new Bitmap(1,1));
	}

	bitmaps->push_back(newImage);
	
	return bitmaps->back()->image;
}


void BitmapCache::cleanResources()
{
	for(unsigned int i = 0; i < bitmaps->size(); ++i)
	{
		SAFE_DELETE(bitmaps->at(i));
	}
	bitmaps->clear();
}