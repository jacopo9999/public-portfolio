#include "Bitmap.h"
#include "TGAFile.h"
#include "Font.h"

void Bitmap::Create(const unsigned int wPar,
					const unsigned int hPar,
					const TGAFile* tgaData,
					const int dimension){
						int w=wPar;
						int h=hPar;

						if(dimension>0){
							w*=dimension;
							h*=dimension;
						}else if(dimension<0){
							int decremento=-dimension;
							w/=decremento;
							if( (wPar % decremento)!=0 ){
								w++;
							}

							h/=decremento;
							if( (hPar % decremento)!=0 ){
								h++;
							}
						}

						width = w; 
						height = h;
						bilinearInterpolation( wPar,  hPar, tgaData,  dimension);
}

void Bitmap::Create(const unsigned int wPar,
					const unsigned int hPar,
					const TGAFile* tgaData,
					const unsigned int width, 
					const unsigned int height)
{
	int dimensionW = 0;
	int dimensionH = 0;

	this->width = width; 
	this->height = height;

	if(width>wPar){
		dimensionW=width/wPar;
	}
	if(height>hPar){
		dimensionH=height/hPar;
	}

	bilinearInterpolation( wPar,  hPar, tgaData,  (dimensionW>dimensionH)?dimensionW:dimensionH  );
}



void Bitmap::bilinearInterpolation(const unsigned int wPar,
								   const unsigned int hPar,
								   const TGAFile* tgaData,
								   const int dimension)
{
	

	int w=wPar;
	int h=hPar;
	int originalW = wPar;
	int originalH = hPar;
	//0 normale, >0 raddoppio, <0 dimezzo

	//dimensione=2;

	this->pixelData = pixelData;
	this->dimension = dimension;

	
	if(dimension>0){
		w*=dimension;
		h*=dimension;
	}else if(dimension<0){
		int decremento=-dimension;
		w/=decremento;
		if( (originalW % decremento)!=0 ){
			w++;
		}

		h/=decremento;
		if( (originalH % decremento)!=0 ){
			h++;
		}
	}


	this->originalWidth = w; 
	this->originalHeight = h;

	//Creo il bitmap con le nuove dimensioni
	hdc = CreateCompatibleDC(NULL); 
	BITMAPINFO bmi; 
	ZeroMemory(&bmi, sizeof(BITMAPINFO)); 
	bmi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmi.bmiHeader.biWidth = w; 
	bmi.bmiHeader.biHeight = h; 
	bmi.bmiHeader.biPlanes = 1;
	bmi.bmiHeader.biBitCount = 32;         
	bmi.bmiHeader.biCompression = BI_RGB;
	bmi.bmiHeader.biSizeImage = w * h * 4;

	HBITMAP hbitmap = CreateDIBSection(hdc, &bmi, DIB_RGB_COLORS, &pixelData, NULL, 0x0);
	SelectObject(hdc, hbitmap);

	unsigned __int32* ptr = (unsigned __int32*)pixelData; 

	if(tgaData != NULL) 
	{
		if(dimension>0){
			for(int y=0; y<originalH; y++){
				TGAFile::TgaColorData* colorData; 
				//Raddoppio con interpolazione bilineare
				for(int j=0;j<dimension;j++){
					int normalizedW;
					float proporzioneY = float(j)/float(dimension); //migliora la leggibilit�

					if((tgaData->Header.imagedescriptor & (1<<5)) != 0){		//top-left or bottom-left origin? 
						colorData= &tgaData->RawData[((originalH-1)-y) * originalW];			//top-left, reverse rows
						normalizedW=-originalW;
					}else{
						colorData= &tgaData->RawData[y * originalW];						//bottom-left, keep rows
						normalizedW=originalW;
					}
					for(int x = 0; x<originalW; x+=1, ptr++, colorData++){				//premultiply alpha

						unsigned char r;
						unsigned char g;
						unsigned char b;

						if(y<(originalH-1)){
							/* interpolazione bilineare Y*/

							float fPremultAlpha = (float((colorData)->BgraInfoData.a)*(1.f-proporzioneY) + float((colorData+normalizedW)->BgraInfoData.a) * proporzioneY)/ 255.f; 

							r = (unsigned char)((float(colorData->BgraInfoData.r)*(1.f-proporzioneY) + (float((colorData+normalizedW)->BgraInfoData.r) * proporzioneY))* fPremultAlpha *alpha); 

							g = (unsigned char)((float(colorData->BgraInfoData.g)*(1.f-proporzioneY) + (float((colorData+normalizedW)->BgraInfoData.g) * proporzioneY))* fPremultAlpha *alpha);

							b = (unsigned char)((float(colorData->BgraInfoData.b)*(1.f-proporzioneY) + (float((colorData+normalizedW)->BgraInfoData.b) * proporzioneY))* fPremultAlpha *alpha); 
							//fine interpolazione bilineare*/
						}else{
							/* Originale */
							float fPremultAlpha = float(colorData->BgraInfoData.a) / 255.f; //255.f
							r = (unsigned char)(float(colorData->BgraInfoData.r) * fPremultAlpha *alpha); 
							g = (unsigned char)(float(colorData->BgraInfoData.g) * fPremultAlpha *alpha); 
							b = (unsigned char)(float(colorData->BgraInfoData.b) * fPremultAlpha *alpha); 
							/*fine modifica*/
						}
						//*ptr = ((colorData->BgraInfoData.a) << 24) | (r << 16) | (g <<8) | (b);
						*ptr = ((unsigned char)(float(colorData->BgraInfoData.a) * alpha) << 24) | (r << 16) | (g <<8) | (b);
						//Raddoppio
						for(int i=1;i<dimension;i++){
							ptr++;

							/*interpolazione bilineare X*/

							float proporzioneX = float(i)/float(dimension); //migliora la leggibilit�

							if( (x<(originalW-1)) && (y<(originalH-1))){

								float fPremultAlpha = (
									(
									float((colorData)->BgraInfoData.a) * (1.f-proporzioneX)
									+ 
									float((colorData+1)->BgraInfoData.a) * proporzioneX
									)*(1.f-proporzioneY)
									+
									(
									float((colorData+normalizedW)->BgraInfoData.a) * (1.f-proporzioneX)
									+ 
									float((colorData+normalizedW+1)->BgraInfoData.a) * proporzioneX
									)*(proporzioneY)
									)/ 255.f; 

								r = (unsigned char)(
									(

									(
									float(colorData->BgraInfoData.r)*(1.f-proporzioneX) 
									+									
									float((colorData+1)->BgraInfoData.r) * proporzioneX
									)*(1.f-proporzioneY)

									+

									(
									float((colorData+normalizedW)->BgraInfoData.r)*(1.f-proporzioneX) 
									+									
									float((colorData+normalizedW+1)->BgraInfoData.r) * proporzioneX
									)*(proporzioneY)

									)* fPremultAlpha *alpha
									); 

								g = (unsigned char)(
									(

									(
									float(colorData->BgraInfoData.g)*(1.f-proporzioneX) 
									+									
									float((colorData+1)->BgraInfoData.g) * proporzioneX
									)*(1.f-proporzioneY)

									+

									(
									float((colorData+normalizedW)->BgraInfoData.g)*(1.f-proporzioneX) 
									+									
									float((colorData+normalizedW+1)->BgraInfoData.g) * proporzioneX
									)*(proporzioneY)

									)* fPremultAlpha *alpha
									);
								b = (unsigned char)(
									(

									(
									float(colorData->BgraInfoData.b)*(1.f-proporzioneX) 
									+									
									float((colorData+1)->BgraInfoData.b) * proporzioneX
									)*(1.f-proporzioneY)

									+

									(
									float((colorData+normalizedW)->BgraInfoData.b)*(1.f-proporzioneX) 
									+									
									float((colorData+normalizedW+1)->BgraInfoData.b) * proporzioneX
									)*(proporzioneY)

									)* fPremultAlpha *alpha
									);

							}else if( (x<(originalW-1))){

								//interpolazione lineare, se � l'ultima riga
								float fPremultAlpha = (float((colorData)->BgraInfoData.a)*(1.f-proporzioneX) + float((colorData+1)->BgraInfoData.a) * proporzioneX)/ 255.f; 

								r = (unsigned char)((float(colorData->BgraInfoData.r)*(1.f-proporzioneX) + (float((colorData+1)->BgraInfoData.r) * proporzioneX))* fPremultAlpha *alpha); 

								g = (unsigned char)((float(colorData->BgraInfoData.g)*(1.f-proporzioneX) + (float((colorData+1)->BgraInfoData.g) * proporzioneX))* fPremultAlpha *alpha);

								b = (unsigned char)((float(colorData->BgraInfoData.b)*(1.f-proporzioneX) + (float((colorData+1)->BgraInfoData.b) * proporzioneX))* fPremultAlpha *alpha); 								
							}
							/*fine interpolazione bilineare*/
							/*se vuoi togliere l'interpolazione basta commentare quello che c'� qui sopra quest'ultima riga*/

							//*ptr = (unsigned char)(float(colorData->BgraInfoData.a) * alpha) | (r << 16) | (g <<8) | (b);
							*ptr = ((unsigned char)(float(colorData->BgraInfoData.a) * alpha) << 24) | (r << 16) | (g <<8) | (b);
						}
					}
				}
			}
		}else if(dimension<0){
			//Ridotto:
			int y,x,decremento;
			decremento=-dimension;
			for(y=0; y<originalH; y+=decremento){
				TGAFile::TgaColorData* colorData; 
				if((tgaData->Header.imagedescriptor & (1<<5)) != 0)		//top-left or bottom-left origin? 
					colorData= &tgaData->RawData[((originalH-1)-y) * originalW];			//top-left, reverse rows
				else
					colorData= &tgaData->RawData[y * originalW];					//bottom-left, keep rows

				for(x = 0; x<originalW; x+=decremento, ptr++, colorData+=decremento){				//premultiply alpha
					float fPremultAlpha = float(colorData->BgraInfoData.a) / 255.f; 
					unsigned char r = (unsigned char)(float(colorData->BgraInfoData.r) * fPremultAlpha*alpha); 
					unsigned char g = (unsigned char)(float(colorData->BgraInfoData.g) * fPremultAlpha*alpha); 
					unsigned char b = (unsigned char)(float(colorData->BgraInfoData.b) * fPremultAlpha*alpha); 
					//*ptr = (colorData->BgraInfoData.a << 24) | (r << 16) | (g <<8) | (b);
					*ptr = ((unsigned char)(float(colorData->BgraInfoData.a) * alpha) << 24) | (r << 16) | (g <<8) | (b);
				}

			}
		}else{
			//Normale
			for(int y=0; y<originalH; y++){
				TGAFile::TgaColorData* colorData; 
				if((tgaData->Header.imagedescriptor & (1<<5)) != 0)		//top-left or bottom-left origin? 
					colorData= &tgaData->RawData[((originalH-1)-y) * originalW];			//top-left, reverse rows
				else
					colorData= &tgaData->RawData[y * originalW];					//bottom-left, keep rows

				for(int x = 0; x<originalW; x++, ptr++, colorData++){				//premultiply alpha


					float fPremultAlpha = float(colorData->BgraInfoData.a) / 255.f; 
					unsigned char r = (unsigned char)(float(colorData->BgraInfoData.r) * fPremultAlpha*alpha); 
					unsigned char g = (unsigned char)(float(colorData->BgraInfoData.g) * fPremultAlpha*alpha); 
					unsigned char b = (unsigned char)(float(colorData->BgraInfoData.b) * fPremultAlpha*alpha); 
					//*ptr = (colorData->BgraInfoData.a << 24) | (r << 16) | (g <<8) | (b);
					*ptr = ((unsigned char)(float(colorData->BgraInfoData.a) * alpha) << 24) | (r << 16) | (g <<8) | (b);
				}
			}
		}  
	}else{	
		memset(pixelData, 0, w * h * 4);
	}
	Setup(); 


}

Bitmap::Bitmap(const unsigned int w, const unsigned int h, const float alpha) : requiresDeletion(true), alpha(alpha)
{
	Create(w, h, NULL); 
}

Bitmap::Bitmap(const TGAFile* file, const float alpha) : requiresDeletion(true), alpha(alpha)
{
	Create(file->Header.width, file->Header.height, file); 
}

Bitmap::~Bitmap()
{
	if(!requiresDeletion) return; 
	DeleteDC(hdc); 
}

Bitmap::Bitmap(const HDC drawingDC, const unsigned int w, const unsigned int h, const float alpha) : requiresDeletion(true), alpha(alpha)
{
	hdc = drawingDC;
	originalHeight = height = h; 
	originalWidth = width = w;
	Setup(); 
}

Bitmap::Bitmap(const String fileName, const float alpha) : requiresDeletion(true), alpha(alpha)
{
	TGAFile f; 
	if(!f.load(fileName.get())) return; 
	Create(f.Header.width, f.Header.height, &f); 
}

Bitmap::Bitmap(const String fileName, const int dimension, const float alpha) : requiresDeletion(true), alpha(alpha)
{
	TGAFile f; 
	if(!f.load(fileName.get())) return; 
	Create(f.Header.width, f.Header.height, &f, dimension); 
}

Bitmap::Bitmap(const TGAFile* file, const unsigned int width, const unsigned int height, const float alpha) : requiresDeletion(true), alpha(alpha)
{
	Create(file->Header.width, file->Header.height, file, width,height);
}

Bitmap::Bitmap(const String fileName, const unsigned int width, const unsigned int height, const float alpha) : requiresDeletion(true), alpha(alpha)
{
	TGAFile f; 
	if(!f.load(fileName.get())) return; 
	Create(f.Header.width, f.Header.height, &f, width,height);
}

void Bitmap::Setup()
{
	SetBkMode(hdc, TRANSPARENT); 
	HFONT hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT); 
	SelectObject(hdc, hFont); 
}

void Bitmap::CopyTo(const Bitmap* other, const int x, const int y)
{
	BitBlt(other->hdc, x, y, originalWidth, originalHeight, hdc, 0, 0, SRCCOPY); 
}

void Bitmap::DrawTo(const Bitmap* other,
					const int x,
					const int y)
{
	BLENDFUNCTION bf;
	bf.BlendOp = AC_SRC_OVER;
	bf.BlendFlags = 0;
	bf.AlphaFormat = AC_SRC_ALPHA;
	bf.SourceConstantAlpha = 255;
	//Alphablend fa il ridimensionamento come ci pare, basta modificare le prime due width e height
	AlphaBlend(other->hdc, x, y, width, height, hdc, 0, 0, originalWidth, originalHeight, bf); 
}

void Bitmap::DrawTo(const Bitmap* other,
					const unsigned int width,
					const unsigned int height,
					const int x,
					const int y)
{
	BLENDFUNCTION bf;
	bf.BlendOp = AC_SRC_OVER;
	bf.BlendFlags = 0;
	bf.AlphaFormat = AC_SRC_ALPHA;   
	bf.SourceConstantAlpha = 255;
	//Alphablend fa il ridimensionamento come ci pare, basta modificare le prime due width e height
	AlphaBlend(other->hdc, x, y, width, height, hdc, 0, 0, originalWidth, originalHeight, bf); 
}

void Bitmap::DrawRectTo(const Bitmap* other,
						const int destx,
						const int desty,
						const int srcx,
						const int srcy,
						const unsigned int w,
						const unsigned int h)
{
	BLENDFUNCTION bf;
	bf.BlendOp = AC_SRC_OVER;
	bf.BlendFlags = 0;
	bf.AlphaFormat = AC_SRC_ALPHA;   
	bf.SourceConstantAlpha = 255;
	//Alphablend fa il ridimensionamento come ci pare, basta modificare le prime due width e height
	AlphaBlend(other->hdc, destx, desty, w, h, hdc, srcx, srcy, w, h, bf); 
}

void Bitmap::Clear(unsigned char ColorR, 
				   unsigned char ColorG,
				   unsigned char ColorB)
{
	HBRUSH br = CreateSolidBrush(RGB(ColorR, ColorG, ColorB)); 
	RECT rc = {0, 0, width, height}; 
	FillRect(hdc, &rc, br); 
	DeleteObject(br); 
}

void Bitmap::RenderText(const int x,
						const int y,
						const char* text,
						const unsigned char ColorR,
						const unsigned char ColorG,
						const unsigned char ColorB)
{
	COLORREF clr = RGB(ColorR, ColorG, ColorB); 
	SetTextColor(hdc, clr); 
	TextOutA(hdc, x, y, text, strlen(text)); 
}

void Bitmap::RenderText(const int x,
						const int y,
						const unsigned int w,
						const unsigned int h,
						const char* text,
						const unsigned char ColorR,
						const unsigned char ColorG,
						const unsigned char ColorB)
{
	COLORREF clr = RGB(ColorR, ColorG, ColorB); 
	SetTextColor(hdc, clr); 
	RECT rc= { x, y, x+w, y+h };
	DrawTextA(hdc, text, strlen(text), &rc, DT_TOP | DT_LEFT | DT_WORDBREAK ); 
}

void Bitmap::RenderText(const int x,
						const int y,
						const String text,
						const Font* font,
						const unsigned char ColorR,
						const unsigned char ColorG,
						const unsigned char ColorB)
{
	if(font && (font -> hFont))
	{
		HFONT hOldSelectedFont = (HFONT) SelectObject(hdc, font -> hFont);
		RenderText(x, y, text.get(), ColorR, ColorG, ColorB);
		SelectObject(hdc, hOldSelectedFont);
	}
	else
		RenderText(x, y, text.get(), ColorR, ColorG, ColorB);
}

void Bitmap::RenderText(const int x,
						const int y,
						const unsigned int w,
						const unsigned int h,
						const String text,
						const Font* font,
						const unsigned char ColorR,
						const unsigned char ColorG,
						const unsigned char ColorB)
{
	if(font && (font -> hFont))
	{
		HFONT hOldSelectedFont = (HFONT) SelectObject(hdc, font -> hFont);
		RenderText(x, y, w, h, text.get(), ColorR, ColorG, ColorB);
		SelectObject(hdc, hOldSelectedFont);
	}
	else
		RenderText(x, y, w, h, text.get(), ColorR, ColorG, ColorB);
}