#pragma once

#include <ios>
#include <sstream>

#include "../WinContainers/WinContainers.h"

/*This class is a wrapper for a dynamic chars array*/
class String 
{
private:
	char * content; //The content of the String

public:
	//Constructors
	String();
	String(const char* charPtr);

	//Copy constructor
	String(const String& other);

	//Copy assignment operator
	String& operator=(const String& other);

	//Append operator
	String operator+(const String& other) const;
	//Append operator
	String& operator+=(const String& other);

	//Destructor
	~String();

	char* get() const { return content; }
	const unsigned int size() const { return strlen(content); }
	LinkedList<String> getLines() const;

	const int compare(const String otherString) const;

};

