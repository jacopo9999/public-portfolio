#ifndef BITMAP_H
#define BITMAP_H

#include "WinDrawSystemApi.h"
#include "String.h"

#include <assert.h>

class TGAFile; 
class Font;

class Bitmap
{
protected: 
	bool requiresDeletion; 
	HDC hdc; 
	void* pixelData; 
	float alpha;
	int width; 
	int height; 
	int originalWidth;
	int originalHeight;

	int dimension;

	void Create(const unsigned int w, 
				const unsigned int h, 
				const TGAFile* tgaData,
				const int dimension = 0);
	void Create(const unsigned int w,
				const unsigned int h,
				const TGAFile* tgaData,
				const unsigned int width,
				const unsigned int height);
	void Setup(); 
	void bilinearInterpolation(const unsigned int wPar,
							   const unsigned int hPar,
							   const TGAFile* tgaData,
							   const int dimension);

	Bitmap(const String fileName, const int dimensione, const float alpha = 1.f); //Create a bitmap from a TGA file, it is scaled with the specified dimension (>0 to make it bigger, <0 to make it smaller).

public: 

	Bitmap(const unsigned int width, const unsigned int height, const float alpha=1.f); //Create an empty bitmap. 
	Bitmap(const HDC drawingDC, const unsigned int width, const unsigned int height, const float alpha=1.f); //Create a bitmap for drawing on the main window.
	Bitmap(const TGAFile* file,
		   const float alpha = 1.f); //Create a bitmap from a TGA file.
	Bitmap(const String fileName, 
		   const float alpha = 1.f); //Create a bitmap loading the file with the specified name.
	Bitmap(const TGAFile* file,
		   const unsigned int width,
		   const unsigned int height, 
		   const float alpha = 1.f); //Create a bitmap from a TGA file, it is resized using the specified width and height.
	Bitmap(const String fileName,
		   const unsigned int width,
		   const unsigned int height, 
		   const float alpha = 1.f); //Create a bitmap loading the file with the specified name, it is resized using the specified width and height.

	//Copy constructor
	Bitmap(const Bitmap&) { Create(width, height, static_cast<TGAFile*>(pixelData), dimension); }

	//Destructor
	~Bitmap(); 
  
	unsigned int GetWidth() { return originalWidth; }			
	unsigned int GetHeight() { return originalHeight; } 

	void CopyTo(const Bitmap* other, 
				const int x,
				const int y); 

	void DrawTo(const Bitmap* other,
				const int x,
				const int y); //Draw this bitmap at (x, y) on "other" using alpha blending 
	void DrawTo(const Bitmap* other,
				const unsigned int width,
				const unsigned int height,
				const int x,
				const int y); //Draw this bitmap at (x, y) on "other" using alpha blending and resizing it.

	void DrawRectTo(const Bitmap* other,
				    const int destx,
					const int desty,
					const int srcx,
					const int srcy,
					const unsigned int w,
					const unsigned int h); //Draw a rectangle (w x h) of this bitmap at (srcx, srcy) on "other" at (srcx, srcy) using alpha blending.

	void Clear(const unsigned char ColorR, 
			   const unsigned char ColorG,
			   const unsigned char ColorB); //Clear the bitmap content.

	//with default font
	void RenderText(const int x,
				    const int y,
					const char* text,
					const unsigned char ColorR,
					const unsigned char ColorG,
					const unsigned char ColorB); //Draw text on this bitmap.
	void RenderText(const int x,
					const int y,
					const unsigned int w,
					const unsigned int h,
					const char* text,
					const unsigned char ColorR,
					const unsigned char ColorG,
					const unsigned char ColorB); //Draw text on this bitmap, in a box.
	
	//and with custom font
	void RenderText(const int x,
					const int y,
					const String text,
					const Font* font,
					const unsigned char ColorR,
					const unsigned char ColorG,
					const unsigned char ColorB); //Draw text on this bitmap with custom Font.
	void RenderText(const int x,
					const int y,
					const unsigned int w,
					const unsigned int h,
					const String text,
					const Font* font,
					const unsigned char ColorR,
					const unsigned char ColorG,
					const unsigned char ColorB); //Draw text on this bitmap, in a box, with custom Font.
}; 

#endif