
#include "Timer.h"
#include "platform.h"

float Timer::now()
{
    return ::getHiResTime(frequency);
}

Timer::Timer() : paused(false)
{
	initHiResTimer(&frequency);
	Reset(); 
}

void Timer::Reset()
{
	float currentTime = now(); 
	startTime = currentTime; 
	lastTime = currentTime; 
}

void Timer::Pause(const bool paused)
{
	this->paused = paused;
}

bool Timer::isPaused()
{
	return this->paused;
}

float Timer::TimeFromStart()
{
	if(paused)
	{
		return 0;
	}
	float currentTime = now(); 
	lastTime = currentTime;
	return currentTime - startTime; 
}

float Timer::TimeFromLast()
{
	float currentTime = now(); 
	float elapsed = currentTime- lastTime;
	lastTime = currentTime; 
	return elapsed; 
}
 
