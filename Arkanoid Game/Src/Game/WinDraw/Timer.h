#ifndef WINDRAW_TIMER_H
#define WINDRAW_TIMER_H

class Timer
{
protected: 
	double frequency; 
	float startTime; 
	float lastTime; 
	bool paused;

	float now(); 
public: 
	Timer();				//creates a timer. 
	void Reset();			//resets a timer. 
	float TimeFromStart();  //returns time since creation
	float TimeFromLast();	//returns time since creation, Reset(), TimeFromStart() or TimeFromLast(), whatever happened latest. 

	void Pause(const bool paused); //Pause the timer.
	bool isPaused(); //Return a boolean that indicates whether the timer is paused or not.
}; 

#endif