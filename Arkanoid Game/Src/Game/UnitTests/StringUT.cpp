#include "stdafx.h"
#include "CppUnitTest.h"

#include "../WinDraw/WinDraw.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTests
{
	TEST_CLASS(StringUnitTest)
	{
	public:
		
		TEST_METHOD(StringTest1)
		{
			String mystring1("ciao ");
			String mystring2("come va??");
			String mystring3;
			mystring3 = mystring1 + mystring2 + "!";

			String mystring6("Allora ");
			mystring6 += mystring1;
			mystring6 += mystring2;
			Assert::AreEqual("Allora ciao come va??", mystring6.get());

			Assert::AreEqual("ciao ", mystring1.get());
			Assert::AreEqual("come va??", mystring2.get());
			Assert::AreEqual("ciao come va??!", mystring3.get());

			String mystring4(mystring3);

			Assert::AreEqual("ciao come va??!", mystring4.get());
			Assert::AreEqual(15U, mystring4.size());

			mystring4 = mystring1;
			String mystring5 = mystring1;

			Assert::AreEqual(0, strcmp(mystring4.get(), mystring5.get()));
			Assert::AreEqual(mystring4.size(), mystring5.size());
			Assert::AreEqual(5U, mystring4.size());
			Assert::AreEqual(5U, mystring5.size());

			String string7("Buongiorno\nOggi � una bella giornata!");
			String string8("");
			String string9("Salve!");
			String string10("Salve!\n");
			
			LinkedList<String> lines7 = string7.getLines();
			LinkedList<String> lines8 = string8.getLines();
			unsigned int size7 = lines7.size();
			unsigned int size8 = lines8.size();
			unsigned int size9 = string9.getLines().size();
			unsigned int size10 = string10.getLines().size();

			Assert::AreEqual("Buongiorno", lines7.at(0).get());
			Assert::AreEqual("Oggi � una bella giornata!", lines7.at(1).get());
			Assert::AreEqual("Buongiorno\nOggi � una bella giornata!", string7.get());
			Assert::AreEqual(2U, size7);
			Assert::AreEqual(0U, size8);
			Assert::AreEqual(1U, size9);
			Assert::AreEqual(1U, size10);
		}

	};
}