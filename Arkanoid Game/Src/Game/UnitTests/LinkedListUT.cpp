#include "stdafx.h"
#include "CppUnitTest.h"

#include "../WinContainers/WinContainers.h" //Because we are testing a class of WinContainers

#include <iostream>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace LinkedListUnitTests
{		
	TEST_CLASS(LinkedListUnitTest)
	{
	public:
		
		TEST_METHOD(LinkedListTest1)
		{
			// TODO: Your test code here
			LinkedList<int> list;
			//*
			list.push_back(-22);
			list.push_back(-34);
			list.push_back(88);
			list.pop_back();
			list.push_back(20);
			list.push_front(0);

			Assert::AreEqual(unsigned int(4), list.size());
			Assert::AreEqual(0, list.at(0));
			Assert::AreEqual(-22, list.at(1));
			Assert::AreEqual(-34, list.at(2));
			Assert::AreEqual(20, list.at(3));
			Assert::AreEqual(0, list.front());
			Assert::AreEqual(20, list.back());


			list.clear();
			Assert::AreEqual(unsigned int(0), list.size());
			

			list.push_back(1);
			list.push_back(2);
			Assert::AreEqual(unsigned int(2), list.size());
			Assert::AreEqual(1, list.at(0));
			Assert::AreEqual(2, list.at(1));
			
			
			list.pop_front();
			Assert::AreEqual(list.size(), unsigned int(1));
			Assert::AreEqual(list.at(0),2);


			list.setAt(3,0);
			Assert::AreEqual(unsigned int(1), list.size());
			Assert::AreEqual(3, list.at(0));

			
			list.push_front(4);
			Assert::AreEqual(unsigned int(2), list.size());
			Assert::AreEqual(4, list.at(0));
			Assert::AreEqual(3, list.at(1));
			

			list.push_back(5);
			Assert::AreEqual(unsigned int(3), list.size());
			Assert::AreEqual(4, list.at(0));
			Assert::AreEqual(3, list.at(1));
			Assert::AreEqual(5, list.at(2));


			list.push_at(6,1);
			Assert::AreEqual(unsigned int(4), list.size());
			Assert::AreEqual(4, list.at(0));
			Assert::AreEqual(6, list.at(1));
			Assert::AreEqual(3, list.at(2));
			Assert::AreEqual(5, list.at(3));
	
			Assert::AreEqual(list.positionOf(5),3L);
			
			list.deleteAt(1);
			Assert::AreEqual(unsigned int(3), list.size());
			Assert::AreEqual(4, list.at(0));
			Assert::AreEqual(3, list.at(1));
			Assert::AreEqual(5, list.at(2));

			
			Assert::AreEqual(2L, list.positionOf(5));
			Assert::AreEqual(-1L, list.positionOf(9999));
			
			list.pop_front();
			Assert::AreEqual(unsigned int(2), list.size());
			Assert::AreEqual(3, list.at(0));
			Assert::AreEqual(5, list.at(1));

			
			list.pop_back();
			Assert::AreEqual(unsigned int(1), list.size());
			Assert::AreEqual(3, list.at(0));

			list.pop_back();
			Assert::AreEqual(unsigned int(0), list.size());

			list.push_back(5);
			Assert::AreEqual(5, list.at(0));
			list.push_back(4);
			Assert::AreEqual(4, list.at(1));
			list.push_back(3);
			Assert::AreEqual(3, list.at(2));
			Assert::AreEqual(3U, list.size());
			list.deleteAt(1);
			Assert::AreEqual(2U, list.size());
			Assert::AreEqual(5, list.at(0));
			Assert::AreEqual(3, list.at(1));
			list.push_at(2,1);
			Assert::AreEqual(3U, list.size());
			Assert::AreEqual(5, list.at(0));
			Assert::AreEqual(2, list.at(1));
			Assert::AreEqual(3, list.at(2));
		}

	};
}