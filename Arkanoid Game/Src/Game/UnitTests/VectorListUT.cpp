#include "stdafx.h"
#include "CppUnitTest.h"

#include "../WinContainers/WinContainers.h" //Because we are testing a class of WinContainers

#include <iostream>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace VectorListUnitTests
{
	TEST_CLASS(VectorListUnitTest)
	{
	public:
		
		TEST_METHOD(VectorListTest1)
		{
			VectorList<int> vettore(unsigned int(0));
			Assert::AreEqual(unsigned int(0), vettore.size());
			vettore.push_back(-22);
			Assert::AreEqual(unsigned int(1), vettore.size());
			Assert::AreEqual(-22, vettore.at(0));
			Assert::AreEqual(-22, vettore.front());
			vettore.push_back(-34);
			Assert::AreEqual(unsigned int(2), vettore.size());
			Assert::AreEqual(-34, vettore.at(1));
			Assert::AreEqual(-34, vettore.back());
			vettore.push_front(88);
			Assert::AreEqual(88, vettore.at(0));
			Assert::AreEqual(88, vettore.front());
			Assert::AreEqual(-34, vettore.at(2));
			Assert::AreEqual(-34, vettore.back());
			vettore.pop_back();
			vettore.push_back(20);
			vettore.push_front(0);

			Assert::AreEqual(unsigned int(4), vettore.size());
			Assert::AreEqual(0, vettore.at(0));
			Assert::AreEqual(88, vettore.at(1));
			Assert::AreEqual(-22, vettore.at(2));
			Assert::AreEqual(20, vettore.at(3));
			Assert::AreEqual(0, vettore.front());
			Assert::AreEqual(20, vettore.back());

			vettore.clear();
			Assert::AreEqual(unsigned int(0), vettore.size());
			

			vettore.push_back(1);
			vettore.push_back(2);
			Assert::AreEqual(unsigned int(2), vettore.size());
			Assert::AreEqual(1, vettore.at(0));
			Assert::AreEqual(2, vettore.at(1));
			
			
			vettore.pop_front();
			Assert::AreEqual(vettore.size(), unsigned int(1));
			Assert::AreEqual(vettore.at(0),2);


			vettore.setAt(3,0);
			Assert::AreEqual(unsigned int(1), vettore.size());
			Assert::AreEqual(3, vettore.at(0));

			
			vettore.push_front(4);
			Assert::AreEqual(unsigned int(2), vettore.size());
			Assert::AreEqual(4, vettore.at(0));
			Assert::AreEqual(3, vettore.at(1));
			

			vettore.push_back(5);
			Assert::AreEqual(unsigned int(3), vettore.size());
			Assert::AreEqual(4, vettore.at(0));
			Assert::AreEqual(3, vettore.at(1));
			Assert::AreEqual(5, vettore.at(2));


			vettore.push_at(6,1);
			Assert::AreEqual(unsigned int(4), vettore.size());
			Assert::AreEqual(4, vettore.at(0));
			Assert::AreEqual(6, vettore.at(1));
			Assert::AreEqual(3, vettore.at(2));
			Assert::AreEqual(5, vettore.at(3));
	
			Assert::AreEqual(vettore.positionOf(5),3L);
			
			vettore.deleteAt(1);
			Assert::AreEqual(unsigned int(3), vettore.size());
			Assert::AreEqual(4, vettore.at(0));
			Assert::AreEqual(3, vettore.at(1));
			Assert::AreEqual(5, vettore.at(2));

			
			Assert::AreEqual(2L, vettore.positionOf(5));
			Assert::AreEqual(-1L, vettore.positionOf(9999));
			
			vettore.pop_front();
			Assert::AreEqual(unsigned int(2), vettore.size());
			Assert::AreEqual(3, vettore.at(0));
			Assert::AreEqual(5, vettore.at(1));

			
			vettore.pop_back();
			Assert::AreEqual(unsigned int(1), vettore.size());
			Assert::AreEqual(3, vettore.at(0));

			vettore.pop_back();
			Assert::AreEqual(unsigned int(0), vettore.size());

		}

	};
}