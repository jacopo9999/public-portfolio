#include "Brick.h"

void Brick::updateBrick()
{
	setBitmap(((BrickState*)stateMachine.currentState)->getImage());
}

void Brick::hit(){
	hit(1);
}

void Brick::hit(const int damage){
	if(type!=BTYPE_METAL){

		if(damage>0){
			hitPoints-=damage;
			for(int i=0; i<damage; i++){
				stateMachine.eventHappens("HIT");
			}
			destroyed=((BrickState*)stateMachine.currentState)->isDestroyed();
			updateBrick();
		}
	}
}

void Brick::destroy(){
	hitPoints=0;
	destroyed=true;
	stateMachine.eventHappens("DESTROY");
	updateBrick();
}