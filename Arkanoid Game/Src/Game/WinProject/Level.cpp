#include "Level.h"

//Constructor
Level::Level(String filename, X2DEventListener* eventListener) :
	ballIndex(0),
	paddleIndex(0),
	initBall(NULL),
	initPaddle(NULL)
{
	std::fstream inFile(String(String(PATH_DIR_LEVELS) + filename).get(), ios::in | ios::binary);
	inFile.seekg(0);

	if(inFile.fail())
	{
		char error[200];
		sprintf(error, "Error: cannot load level file with name \"%s\" because it does not exist!\n", filename.get());
		OutputDebugStringA(error);
		return;
	}

	unsigned int sceneWidth, sceneHeight, ballWidth, ballHeight, paddleWidth, paddleHeight;
	unsigned int ballX, ballY, paddleX, paddleY;

	//Read the number of bricks
	unsigned int bricksNum;
	inFile.read((char*)&bricksNum, sizeof(unsigned int));
	inFile.read((char*)&sceneWidth, sizeof(unsigned int));
	inFile.read((char*)&sceneHeight, sizeof(unsigned int));
	scene = new X2DScene(0, 0, sceneWidth, sceneHeight, eventListener, PATH_SCENE_BACKGROUND, 1.f);

	//Load the ball specs
	inFile.read((char*)&ballX, sizeof(unsigned int));
	inFile.read((char*)&ballY, sizeof(unsigned int));
	inFile.read((char*)&ballWidth, sizeof(unsigned int));
	inFile.read((char*)&ballHeight, sizeof(unsigned int));
	ballX = ballX + ballWidth/2;
	ballY = ballY + ballHeight/2;
	Ball *ball = new Ball(ballX, ballY, ballWidth, ballHeight, PATH_BALL, 1);

	ball->setSpeed(20.0f, -600.0f);
	scene->addObject(ball);
	ballIndex = scene->getSceneObjectsRef()->size() - 1;

	//Load the paddle specs
	inFile.read((char*)&paddleX, sizeof(unsigned int));
	inFile.read((char*)&paddleY, sizeof(unsigned int));
	inFile.read((char*)&paddleWidth, sizeof(unsigned int));
	inFile.read((char*)&paddleHeight, sizeof(unsigned int));
	Paddle *paddle = new Paddle(paddleX, paddleY, paddleWidth, paddleHeight, PATH_PADDLE);

	scene->addObject(paddle);
	paddleIndex = scene->getSceneObjectsRef()->size() - 1;

	initPaddle = paddle;
	initBall = ball;

	BrickData brickData;
	while(bricksNum > 0)
	{
		inFile.read((char*)&brickData, sizeof(BrickData));
		brickData.x = brickData.x + brickData.width/2;
		brickData.y = brickData.y + brickData.height/2;

		Brick *brick;
		switch(brickData.type)
		{
		case BTYPE_CLAY:
			{
				brick = new ClayBrick(brickData.x,
									  brickData.y,
									  brickData.width,
									  brickData.height,
									  brickData.type);
			} break;
		case BTYPE_CONCRETE:
			{
				brick = new ConcreteBrick(brickData.x,
										  brickData.y,
										  brickData.width,
										  brickData.height,
										  brickData.type);
			} break;
		case BTYPE_METAL:
			{
				brick = new MetalBrick(brickData.x,
									   brickData.y,
									   brickData.width,
									   brickData.height,
									   brickData.type);
			} break;
		case BTYPE_STONE:
			{
				brick = new StoneBrick(brickData.x,
									   brickData.y,
									   brickData.width,
									   brickData.height,
									   brickData.type);
			} break;
		default: //By default it is metal...
			{
				brick = new MetalBrick(brickData.x,
									   brickData.y,
									   brickData.width,
									   brickData.height,
									   brickData.type);
			}
		}

		scene->addObject(brick);
		bricksIndex.push_back(scene->getSceneObjectsRef()->size() - 1);
		initBricks.push_back(brick);

		bricksNum--;
	}

	inFile.close();
}

//Destructor
Level::~Level()
{
	bricksIndex.clear();

	for(unsigned int i = 0; i < initBricks.size(); ++i){
		delete initBricks.at(i);
	}
	initBricks.clear();

	delete scene;
	delete initBall;
	delete initPaddle;

	for(unsigned int i = 0; i < oldPointers.size(); ++i)
	{
		delete oldPointers.at(i);
	} 
	oldPointers.clear();

}

//Start moving the ball
void Level::startBall()
{
	Ball *ball = getBallRefAt(ballIndex);
	ball->detachFromPaddle();
}

//Move the paddle to the specified horizontal position
void Level::setPaddleX(const float x)
{
	Paddle *paddle = getPaddleRefAt(paddleIndex);
	if (x < scene->getLeftMargin() + paddle->getWidth() / 2)
	{
		paddle->setPositionByCenter(scene->getLeftMargin() + paddle->getWidth() / 2, paddle->getCenterY());
	}
	else if(x > scene->getRightMargin() - paddle->getWidth() / 2) 
	{
		paddle->setPositionByCenter(scene->getRightMargin() - paddle->getWidth() / 2, paddle->getCenterY());
	}
	else
	{
		paddle->setPositionByCenter(x, paddle->getCenterY());
	}
}

//Reset the paddle to the initial status
void Level::resetPaddle()
{
	scene->getSceneObjectsRef()->at(paddleIndex) = new Paddle(*initPaddle);
	oldPointers.push_back(scene->getSceneObjectsRef()->at(paddleIndex));
} 

//Reset the ball to the initial status
void Level::resetBall()
{

	scene->getSceneObjectsRef()->at(ballIndex) = new Ball(*initBall);
	oldPointers.push_back(scene->getSceneObjectsRef()->at(ballIndex));
	Ball *ball = getBallRefAt(ballIndex);
	ball->followPaddle(getPaddleRefAt(paddleIndex));
} 

//Reset the bricks to their initial position
void Level::resetBricks()
{
	for(unsigned int i = 0; i < bricksIndex.size(); i++)
	{
		scene->getSceneObjectsRef()->at(bricksIndex.at(i)) = new Brick(*initBricks.at(i));
		oldPointers.push_back(scene->getSceneObjectsRef()->at(bricksIndex.at(i)));
	} 
} 

//Reset the level scene to the initial status
void Level::restart()
{
	for(unsigned int i = 0; i < oldPointers.size(); ++i)
	{
		delete oldPointers.at(i);
	} 
	oldPointers.clear();
	
	resetBricks();
 	resetPaddle();
	resetBall();
} 

//Check whether there are still bricks into the scene or not
bool Level::checkForBricks()
{
	for(unsigned int i = 0; i < bricksIndex.size(); i++)
	{
		Brick *sceneBrick = getBrickRefAt(bricksIndex.at(i));
 		if(sceneBrick->isDestroyed() == false && sceneBrick->getBrickType() != BTYPE_METAL){
 			return true;
 		}
	} 
	return false;
}	 

//Get a reference to the ball at the specified index
Ball* const Level::getBallRefAt(const unsigned int index) const
{
	Ball *ball = dynamic_cast<Ball*>(scene->getSceneObjectsRef()->at(index));
	if(ball == NULL)
	{
		cout << "ERROR: invalid object type (expected: ball) at index: " << index << endl;
	}	
	return ball;
}	

//Get a reference to the paddle at the specified index
Paddle* const Level::getPaddleRefAt(const unsigned int index) const
{
	Paddle *paddle = dynamic_cast<Paddle*>(scene->getSceneObjectsRef()->at(index));
	if(paddle == NULL)
	{
		cout << "ERROR: invalid object type (expected: paddle) at index: " << index << endl;
	}	
	return paddle;
}	

//Get a reference to the brick at the specified index
Brick* const Level::getBrickRefAt(const unsigned int index) const 
{
	Brick *brick = dynamic_cast<Brick*>(scene->getSceneObjectsRef()->at(index));
	if(brick == NULL)
	{
		cout << "ERROR: invalid object type (expected: brick) at index: " << index << endl;
	}	
	return brick;
}	


