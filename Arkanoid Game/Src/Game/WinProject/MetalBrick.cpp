#include "MetalBrick.h"

MetalBrick::MetalBrick(
	const int x,
	const int y,
	const unsigned int width,
	const unsigned int height,
	BrickType type
	):Brick(x, y, width, height, mass, type, 1)
{
	intero = new BrickState("INTERO",PATH_METALBRICK);
	distrutto = new BrickState("DISTRUTTO",NULL,true);
	stateMachine.addState(intero);
	stateMachine.addState(distrutto);
	stateMachine.addEvent("DESTROY",intero,distrutto);
	stateMachine.addEvent("HIT",intero,intero);
	stateMachine.setState(intero);
	updateBrick();
}


MetalBrick::~MetalBrick(void)
{
	SAFE_DELETE(intero);
	SAFE_DELETE(distrutto);
}
