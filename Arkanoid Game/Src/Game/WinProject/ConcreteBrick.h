#pragma once

#include "Brick.h"
#include "BrickState.h"

class ConcreteBrick : public Brick
{
public:
	ConcreteBrick(	const int x,
		const int y,
		const unsigned int width,
		const unsigned int height,
		BrickType type
		);
	~ConcreteBrick();
private:
	BrickState * intero, * rotto, * distrutto;
};
