#pragma once

#include "Brick.h"
#include "BrickState.h"

class MetalBrick : public Brick
{
public:
	MetalBrick(	const int x,
		const int y,
		const unsigned int width,
		const unsigned int height,
		BrickType type
		);
	~MetalBrick();
private:
	BrickState * intero, * distrutto;
};
