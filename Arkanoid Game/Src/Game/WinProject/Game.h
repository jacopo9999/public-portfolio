#pragma once

#include "../WinGameEngine/WinGameEngine.h"
#include "../WinContainers/WinContainers.h"

#include "Level.h"
#include "Paddle.h"
#include "Ball.h"
#include "Brick.h"

#define MAX_LIVES 3
#define MIN_SCORE 50
#define INITIAL_TIME_SCORE 250

#define UPDATE_FREQUENCY 8

//The set of states for the Game State Machine
enum GameState
{
	GSTATE_RUNNING,
	GSTATE_PAUSED,
	GSTATE_VICTORY,
	GSTATE_GAMEOVER,
	GSTATE_ENDED
};

/*The Game class is a wrapper for all the game mechanics for the Arkanoid game, it contains all the Level objects and keeps track of the player's
progress through them. It loads the levels data and the saved progress from the specified folder, keeps the current level number, the number of lives
and the score and takes a reference to a frame which is the container of the game and where all the player's actions are redirected. It is implemented
as a finite states machine and its behavior changes depending on the current GameState*/
class Game : public X2DEventListener
{
private:
	Game(const Game& other) : X2DEventListener(other), gameFrame(other.gameFrame) {}
	Game& operator=(const Game& other) {}

	UIFrame& gameFrame; //A reference to the frame where the game scene will be displayed
	GameState gameState; //The current state of the game
	unsigned long score; //The current player's score
	int time_score; //The score modifier which depends on how fast the player hits the bricks
	unsigned int lives; //The number of remaining lives
	LinkedList<Level*> levels; //The levels container
	unsigned int currentLevel; //The index of the current level

protected:

	void loadLevels(); //Load all the levels from the Levels folder
	void loadData(); //Load the game data from the previous game session
	void deleteSavedData(); //Delete the saved game data
	void resetGameStats(); //Reset all the player's stats (lives and score)
	void decrementTimerValue(float secondsPerFrame); //Decrement the timer for the score bonus
	void resetDisplayedScene(); //Reset the displayed scene

	//Getters
	Level* getCurrentLevelRef() { return levels.at(currentLevel); }
	X2DScene* getCurrentScene() { return levels.at(currentLevel)->getSceneRef(); }
	//

public:

	//Constructor
	Game::Game(UIFrame& gameFrame) : 
		gameFrame(gameFrame),
		score(0), 
				   lives(MAX_LIVES),
				   time_score(INITIAL_TIME_SCORE),
				   currentLevel(0),
				   gameState(GSTATE_RUNNING)
	{
		loadLevels();
		resetDisplayedScene();
	}

	//Destructor
	~Game()
	{
		for(UINT i=0; i<levels.size(); ++i){
			SAFE_DELETE(levels.at(i));
		}
		levels.clear();
	}

	void collisionWithObject(X2DRectangle *movingObj, X2DRectangle *otherObj); //Called when two objects collide, overrides the X2DEventListener method
	void collisionWithMargin(X2DRectangle *movingObj, CollisionMargin sceneMargin); //Called when an object collides with the scene margin, overrides the X2DEventListener method
	void saveData(); //Save the game data
	bool checkForSavedData(); //Check whether there is saved data from the latest game session or not
	void increaseScore(unsigned int value); //Increase the player's score by the specified amount
	unsigned int getScore() const; //Get the player's score
	unsigned int getLives() const; //Get the player's lives number
	bool loseLife(); //Decrease the number of lives by one. Returns false if there are no more lives remaining
	void restartLevel(); //Restart the current level
	void start(); //Start the game when the user throws the ball
	bool nextLevel(); //Returns false if there are no more levels
	void restartGame(); //Restart the whole game
	void continueGame(); //Load the saved level and start it again
	void resumeGame(); //Resume the game (un-pause)
	void pauseGame(); //Pause the game
	void endGame(); //Stop the game
	void movePaddle(const unsigned int paddleX); //Move the paddle to the specified horizontal (X) position
	void update(const float secondsPerFrame); //Update the scene

	//Set a new mouse action on the game scene
	void setOnMouseAction(const MouseKey key,
		const KeyStatus status,
		void(*mouseAction)(const MouseKey, const KeyStatus, const int mouseX, const int mouseY));

	//Set a new keyboard action on the game scene
	void setOnKeyAction(const Key key,
		const KeyStatus status,	
		void(*keyAction)(const Key, const KeyStatus));

	//Getters:
	const GameState getState() const { return gameState; } //Get the current state of the game
	const unsigned int getCurrentLevel() const { return currentLevel; } //Get the current level number
	//

};

