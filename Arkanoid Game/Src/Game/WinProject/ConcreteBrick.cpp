#include "ConcreteBrick.h"

ConcreteBrick::ConcreteBrick(
	const int x,
	const int y,
	const unsigned int width,
	const unsigned int height,
	BrickType type
	):Brick(x, y, width, height, mass, type, 1)
{
	intero = new BrickState("INTERO",PATH_CONCRETEBRICK0);
	rotto = new BrickState("ROTTO",PATH_CONCRETEBRICK1);
	distrutto = new BrickState("DISTRUTTO",NULL,true);
	stateMachine.addState(intero);
	stateMachine.addState(rotto);
	stateMachine.addState(distrutto);
	stateMachine.addEvent("DESTROY",intero,distrutto);
	stateMachine.addEvent("DESTROY",rotto,distrutto);
	stateMachine.addEvent("HIT",intero,rotto);
	stateMachine.addEvent("HIT",rotto,distrutto);
	stateMachine.setState(intero);
	updateBrick();
}


ConcreteBrick::~ConcreteBrick(void)
{
	SAFE_DELETE(intero);
	SAFE_DELETE(rotto);
	SAFE_DELETE(distrutto);
}
