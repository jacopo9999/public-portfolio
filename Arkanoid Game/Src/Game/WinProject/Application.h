
#include "../WinUIWidgets/WinUIWidgets.h" 
#include "../WinContainers/WinContainers.h"
#include "../WinGameEngine/WinGameEngine.h"

//Local includes
#include "Game.h"
#include "Resources.h"

//Defines
#define GAMEMSG_PRESS_BUTTON_TO_START "PRESS MOUSE TO START..."
#define GAMEMSG_VICTORY "VICTORY!!\nPRESS SPACE FOR NEXT LEVEL!"
#define GAMEMSG_GAMEOVER "GAME OVER!!\nPRESS SPACE TO TRY AGAIN!"
#define GAMEMSG_ENDGAME "YOU WIN!!\nTHIS WAS THE LAST LEVEL!\nPRESS SPACE TO RETURN TO MAIN MENU"

/*This class contains all the GUI elements of the game as well as the data model*/
class Application
{
private:
	Application(const Application&);
	Application& operator=(const Application&);

public:
	//Window Resolution
	static const unsigned int windowWidth = 900;
	static const unsigned int windowHeight = 768;
	//

	static const unsigned int mainMenuButtonsWidth = 400;
	static const unsigned int mainMenuButtonsX = (windowWidth - mainMenuButtonsWidth) / 2;
	static const unsigned int mainMenuButtonsHeight = 80;
	static const unsigned int mainMenuButtonsInterMargin = 50;
	static const unsigned int mainMenuButtonsVerticalMargin = 200;

	static const unsigned int sceneWidth = 600;
	static const unsigned int sceneHeight = 700;
	static const unsigned int lateralMenuWidth = windowWidth - sceneWidth;
	static const unsigned int lateralMenuHeight = windowHeight;

	static const unsigned int buttonsHorMargin = 50;
	static const unsigned int buttonsPositionX = sceneWidth + buttonsHorMargin;
	static const unsigned int buttonsWidth = lateralMenuWidth - 2*buttonsHorMargin; 
	static const unsigned int buttonsHeight = 50;
	static const unsigned int buttonsVerticalMargin = 40;
	static const unsigned int buttonsInterMargin = 20;

	static const unsigned int infosPositionX = buttonsPositionX + 15;
	static const unsigned int infosPositionY = windowHeight - 400;
	static const unsigned int infosHeight = 300;
	static const unsigned int infosWidth = buttonsWidth;

	static const unsigned int livesPositionX = infosPositionX - 30;
	static const unsigned int livesPositionY = infosPositionY + 180;
	static const unsigned int livesHeight = 21;
	static const unsigned int livesWidth = 80;

	static const unsigned int checkBoxSize = 50;
	static const unsigned int pauseLCheckBoxX = buttonsPositionX + buttonsWidth/8;
	static const unsigned int pauseLCheckBoxY = buttonsVerticalMargin + buttonsHeight + buttonsInterMargin;

	static const unsigned int pauseLabelX = pauseLCheckBoxX + 20 + checkBoxSize;
	static const unsigned int pauseLabelY = pauseLCheckBoxY + checkBoxSize/3;

	static const unsigned int gameInfoLabelX = 70;
	static const unsigned int gameInfoLabelY = 30;

	bool exit;

	//Game Instance
	Game mGame;	

	//Fonts
	UIFont mFSmallerButtons, mFButtons, mFStatsText, mFPauseText, mFGameInfoText;

	//Window
	UIWindow mWindow;

	//UI Widgets
	UIFrame mFrameGame, mFrameScene, mFrameLateralMenu;
	UIFrame mFrameMainMenu, mFrameInfos;
	UICheckBox mCBPause;
	LinkedList<UIFrame*> mListLiveImg;
	UILabel mLblPause, mLblGameInfo, mLblStats;
	UILabeledButton mLBMenu, mLBNewGame, mLBContinueGame, mLBExit;

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//MEMBER FUNCTIONS:
	//Set the status of the buttons into the main menu
	void setMenuButtons();
	//Init the frame where the game scene is placed
	void initGameFrame();
	//Start the application main loop
	void startLoop();
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//ACTIONS (STATIC):
	
	//Starts the engine	(and the ball)
	static void startBall(const MouseKey = LMouse, const KeyStatus = KeyReleased, const int mouseX = NULL, const int mouseY = NULL);
	//Move the paddle depending on the position of the mouse
	static void movePaddle(const MouseKey = LMouse, const KeyStatus = KeyReleased, const int mouseX = NULL, const int mouseY = NULL);
	//Hide all the other menus and show only the main menu
	static void showMainMenu(const MouseKey = LMouse, const KeyStatus = KeyReleased, const int mouseX = NULL, const int mouseY = NULL);
	//Reset the game to the first level
	static void startNewGame(const MouseKey = LMouse, const KeyStatus = KeyReleased, const int mouseX = NULL, const int mouseY = NULL);
	//Continue the game starting from the beginning of the last level
	static void continueGame(const MouseKey = LMouse, const KeyStatus = KeyReleased, const int mouseX = NULL, const int mouseY = NULL);
	//Exit the game, mark the window as ready to be closed
	static void exitApplication(const MouseKey = LMouse, const KeyStatus = KeyReleased, const int mouseX = NULL, const int mouseY = NULL);
	//Un-pause the game
	static void resumeGame();
	//Pause the game
	static void pauseGame();
	//Handle what happens when pressing the "space" key
	static void spacePressed(const Key, const KeyStatus);
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	Application();
	~Application();

} application;