
/*
PROFILER PER MEMORY LEAK DI VISUAL STUDIO:
(documentazione in: http://msdn.microsoft.com/it-it/library/x98tx3cf.aspx)
*/

#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

#ifndef DBG_NEW
#define DBG_NEW new( _NORMAL_BLOCK, __FILE__, __LINE__)
#define new DBG_NEW
#endif
#endif

/*// dopo di che metti questa linea di codice:

_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

//all'interno del main
*/

#include "Application.h"

Application::Application() : 
	exit(false),
	mFSmallerButtons(8,8,6,false,false,false,UIColor(0,0,0)),
	mFButtons(13,13,8,false,false,false,UIColor(0,0,0)),
	mFStatsText(20,25,8,false,false,false,UIColor(150,150,200)),
	mFPauseText(12,17,8,false,false,false,UIColor(180,180,200)),
	mFGameInfoText(12,24,10,true,false,false,UIColor(240,0,0)),
	mLBNewGame(mainMenuButtonsX, mainMenuButtonsVerticalMargin, mainMenuButtonsWidth, mainMenuButtonsHeight, "START NEW GAME", &mFButtons),
	mLBContinueGame(mainMenuButtonsX, mainMenuButtonsVerticalMargin + 1*(mainMenuButtonsHeight+mainMenuButtonsInterMargin), mainMenuButtonsWidth, mainMenuButtonsHeight, "CONTINUE GAME", &mFButtons),
	mLBExit(mainMenuButtonsX, mainMenuButtonsVerticalMargin + 2*(mainMenuButtonsHeight+mainMenuButtonsInterMargin), mainMenuButtonsWidth, mainMenuButtonsHeight, "EXIT GAME", &mFButtons),
	mLBMenu(buttonsPositionX, buttonsVerticalMargin, buttonsWidth, buttonsHeight, "MAIN MENU", &mFSmallerButtons),
	mCBPause(pauseLCheckBoxX, pauseLCheckBoxY, checkBoxSize, checkBoxSize),
	mLblPause(pauseLabelX, pauseLabelY, &mFPauseText, "PAUSE"),
	mLblGameInfo(gameInfoLabelX, gameInfoLabelY, &mFGameInfoText, GAMEMSG_PRESS_BUTTON_TO_START),
	mFrameInfos(infosPositionX, infosPositionY, infosWidth, infosHeight),
	mFrameScene(0, 0, windowWidth, windowHeight),
	mFrameGame(0, 0, windowWidth, windowHeight, 1.f),
	mFrameMainMenu(0, 0, windowWidth, windowHeight, 1.f, PATH_FRAME_BACKGROUND),
	mFrameLateralMenu(sceneWidth, 0, lateralMenuWidth, lateralMenuHeight, 1.f, PATH_FRAME_BACKGROUND),
	mLblStats(infosPositionX, infosPositionY + 90, &mFStatsText, "<GAME_STATS>"),
	mGame(mFrameScene)
{
	mWindow.Create(windowWidth, windowHeight, "Arkanoid");

	setMenuButtons();

	mFrameMainMenu.addWidget(&mLBNewGame);
	mFrameMainMenu.addWidget(&mLBContinueGame);
	mFrameMainMenu.addWidget(&mLBExit);
	//////////////////////////////////////////////////////////////

	//Add everything to the window
	mWindow.addFrame(&mFrameMainMenu);
	//////////////////////////////////////////////////////////////

	//Initialize all the frames
	initGameFrame();
	showMainMenu();
	//////////////////////////////////////////////////////////////
}

Application::~Application()
{	
	//Match Informations Frame
	for(unsigned i = 0; i < mListLiveImg.size(); i++){
		SAFE_DELETE(mListLiveImg.at(i));
	}
}

void Application::initGameFrame()
{
	//Lateral Menu Frame
	mFrameLateralMenu.setVisible(true);

	mLBMenu.setOnMouseAction(LMouse, KeyPressed, &showMainMenu);
	mCBPause.setOnCheckedAction(&pauseGame);
	mCBPause.setOnUncheckedAction(&resumeGame);

	mFrameLateralMenu.addWidget(&mLBMenu);
	mFrameLateralMenu.addWidget(&mCBPause);
	mFrameLateralMenu.addWidget(&mLblPause);

	mFrameInfos.addWidget(&mLblStats);

	mFrameLateralMenu.addWidget(&mFrameInfos);
	//////////////////////////////////////////////////////////////

	//Game Scene Frame
	mFrameScene.setVisible(true);

	mGame.setOnMouseAction(No_Key, MouseMove, &movePaddle);
	mGame.setOnMouseAction(LMouse, KeyPressed, &startBall);
	mGame.setOnKeyAction(vk_Space, KeyPressed, &spacePressed);
	mFrameScene.setVisible(false);
	//////////////////////////////////////////////////////////////

	//Assemble everything together
	mFrameGame.addWidget(&mFrameScene);
	mFrameGame.addWidget(&mFrameLateralMenu);
	mFrameGame.addWidget(&mLblGameInfo);
	mWindow.addFrame(&mFrameGame);
	//////////////////////////////////////////////////////////////

	//Setting up the lives
	for(unsigned int i = 0; i < mGame.getLives(); i++){
		UIFrame* temp = new UIFrame(
			livesPositionX + ( (i % 3) * livesWidth),
			livesPositionY + ( (i / 3) * livesHeight),
			livesWidth-2,
			livesHeight-2,
			1.f,
			PATH_PADDLE
		);
		mListLiveImg.push_back(temp);
		mFrameInfos.addWidget(temp);
	}
	//////////////////////////////////////////////////////////////
}

void Application::setMenuButtons()
{
	mLBNewGame.setOnMouseAction(LMouse, KeyPressed, &startNewGame);
	if(mGame.checkForSavedData())
	{
		mLBContinueGame.setOnMouseAction(LMouse, KeyPressed, &continueGame);
		mLBContinueGame.setEnabled(true);
	}
	else
	{
		mLBContinueGame.setEnabled(false);
	}
	mLBExit.setOnMouseAction(LMouse, KeyPressed, &exitApplication);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//USER ACTIONS
void Application::showMainMenu(const MouseKey /*= LMouse*/, const KeyStatus /*= KeyReleased*/, const int mouseX /*= NULL*/, const int mouseY /*= NULL*/)
{
	application.mGame.pauseGame();
	application.mFrameGame.setVisible(false);
	application.mFrameMainMenu.setVisible(true);
	application.setMenuButtons();
}

void Application::startNewGame(const MouseKey /*= LMouse*/, const KeyStatus /*= KeyReleased*/, const int mouseX /*= NULL*/, const int mouseY /*= NULL*/)
{
	application.mGame.restartGame(); //Restart the game and reset all the levels;
	application.mFrameGame.setVisible(true);
	application.mFrameMainMenu.setVisible(false);
	application.mLblGameInfo.setText(GAMEMSG_PRESS_BUTTON_TO_START);
	application.mLblGameInfo.setVisible(true);
}

void Application::continueGame(const MouseKey /*= LMouse*/, const KeyStatus /*= KeyReleased*/, const int mouseX /*= NULL*/, const int mouseY /*= NULL*/)
{
	application.mGame.continueGame();
	application.mFrameGame.setVisible(true);
	application.mFrameMainMenu.setVisible(false);
	application.mLblGameInfo.setText(GAMEMSG_PRESS_BUTTON_TO_START);
	application.mLblGameInfo.setVisible(true);
}

void Application::exitApplication(const MouseKey /*= LMouse*/, const KeyStatus /*= KeyReleased*/, const int mouseX /*= NULL*/, const int mouseY /*= NULL*/)
{
	application.exit = true;
}

void Application::resumeGame()
{
	application.mGame.resumeGame();
}

void Application::pauseGame()
{
	application.mGame.pauseGame();
}

void Application::startBall(const MouseKey /*= LMouse*/, const KeyStatus /*= KeyReleased*/, const int mouseX /*= NULL*/, const int mouseY /*= NULL*/) 
{
	application.mGame.start();
	application.mLblGameInfo.setVisible(false);
}

void Application::movePaddle(const MouseKey /*= LMouse*/, const KeyStatus /*= KeyReleased*/, const int mouseX /*= NULL*/, const int mouseY /*= NULL*/)
{
	if(application.mGame.getState() == GameState::GSTATE_RUNNING)
	{
		application.mGame.movePaddle(mouseX);
	}
}

void Application::spacePressed(const Key, const KeyStatus)
{
	if(application.mGame.getState() == GameState::GSTATE_VICTORY)
	{
		//Go to the next level
		application.mGame.nextLevel();
		application.mLblGameInfo.setText(GAMEMSG_PRESS_BUTTON_TO_START);
		application.mLblGameInfo.setVisible(true);
	}
	else if(application.mGame.getState() == GameState::GSTATE_GAMEOVER)
	{
		//Restart the current level
		application.mGame.restartLevel();
		application.mLblGameInfo.setText(GAMEMSG_PRESS_BUTTON_TO_START);
		application.mLblGameInfo.setVisible(true);
	}
	else if(application.mGame.getState() == GameState::GSTATE_PAUSED)
	{
		//Resume the game
		application.mGame.resumeGame();
		application.mCBPause.setChecked(false);
	}
	else if(application.mGame.getState() == GameState::GSTATE_ENDED)
	{
		//Return to main menu
		application.mGame.endGame();
		application.mFrameGame.setVisible(false);
		application.mFrameMainMenu.setVisible(true);
		application.setMenuButtons();
	}
	else // (mGame->gameState() == GameState::RUNNING)
	{
		//Pause the game
		application.mGame.pauseGame();
		application.mCBPause.setChecked(true);
	}
}

void Application::startLoop()
{
	const float secondsPerFrame = (1.f / 30.f); //We're aiming at 30 fps (1 second / 30 frames per second) = frame length
	Timer t;				//create a timer for the main loop
	while(mWindow.IsAlive())		//closing a window will cause it to not be "alive" anymore. We'll loop until the window closes.  
	{
		//If the gameFrame is visible update it
		if(mFrameGame.isVisible())
		{
			//Check score/lives
			char infosBuffer[100];
			sprintf(infosBuffer, "SCORE:\n%d\nLIVES:\n \n \nLEVEL:\n%d",
				mGame.getScore(),
				mGame.getCurrentLevel());
			mLblStats.setText(String(infosBuffer));

			//With this cicle we can show or hide the images of lives in the game
			for(unsigned int i = 0; i < mListLiveImg.size(); i++){
				mListLiveImg.at(mListLiveImg.size()-(i+1))->setVisible( (mListLiveImg.size()-i)<=(mGame.getLives()) );
			}

			//Check the game status
			GameState gameState = mGame.getState();
			if (gameState == GameState::GSTATE_VICTORY)
			{
				mLblGameInfo.setText(GAMEMSG_VICTORY);
				mLblGameInfo.setVisible(true);
			}
			else if(gameState == GameState::GSTATE_ENDED)
			{
				char finalMsg[200];
				sprintf(finalMsg, "%s\n\nYOUR FINAL SCORE: %d", GAMEMSG_ENDGAME, mGame.getScore());
				mLblGameInfo.setText(finalMsg);
				mLblGameInfo.setVisible(true);
			}
			else if (gameState == GameState::GSTATE_GAMEOVER)
			{
				mLblGameInfo.setText(GAMEMSG_GAMEOVER);			
				mLblGameInfo.setVisible(true);			
			}
			else if (gameState == GameState::GSTATE_RUNNING)
			{
				mCBPause.setChecked(false);
			}
			else if (gameState == GameState::GSTATE_PAUSED)
			{
				mCBPause.setChecked(true);
			}

			//Update the game status
			mGame.update(secondsPerFrame); 
		}

		t.TimeFromLast();
		mWindow.DispatchInput(); //Invokes "MyEventHandler::onKeyInput" as required.
		mWindow.Redraw();	//Invokes "MyEventHandler::onRender" as required.
		wait(secondsPerFrame - t.TimeFromLast()); //Wait (frame seconds - seconds since last frame) to run at the desired fps and leave some processor time for everything else. 

		if(exit)
		{
			break; //The user has requested the application to terminate, exit the loop
		}
	}

	mWindow.ClearEventHandler(); //Detach the event handler, the window is closed anyway
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//MAIN
INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, INT nCmdShow)
{
	//MEMORY LEAK PROFILER
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

	setupWorkDirectory(); 
	application.startLoop();
	BitmapCache::getInstance()->cleanResources();
	return 0;
}
/////////////////////////////////////////////////////////////////////////////////////////////////