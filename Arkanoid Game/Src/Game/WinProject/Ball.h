#pragma once

class Ball : public X2DRectangle
{
private:
	Paddle *followedObject;

public:
	Ball(const int x,
		 const int y,
		 const unsigned int width,
		 const unsigned int height,
		 const char* filename,
		 const unsigned int mass) :
		X2DRectangle(x, y, width, height, DYNAMICRECT, filename, 1.f, mass),
		followedObject(NULL)
	{}

	~Ball(void)
	{}

	Ball(const Ball& other) : X2DRectangle(other)
	{
		followedObject = other.followedObject;
	}

	Ball& operator=(const Ball& other)
	{
		if (this == &other)
			return *this;

		X2DRectangle::operator=(other);
		followedObject = other.followedObject;
		return *this;
	}

	void followPaddle(Paddle *obj) 
	{
		followedObject = obj;
	}

	void detachFromPaddle()
	{
		followedObject = NULL;
	}

	void updatePhysics(const float dt) 
	{
		if (followedObject != NULL) 
		{
			position.set(float(followedObject->getX()) + float(followedObject->getWidth()/2) - float(this->getWidth()/2),
						 float(followedObject->getY()) - float(this->getHeight()));
		} 
		else
		{
			position.set(position.x + speed.x*dt, position.y + speed.y*dt);
		}
	}
};

