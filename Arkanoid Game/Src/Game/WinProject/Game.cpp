#include "Game.h"

//Called when two objects collide, overrides the X2DEventListener method
void Game::collisionWithObject(X2DRectangle *movingObj, X2DRectangle *otherObj)
{
	//get otherObject's behaviour to determine it's type
	collisionBehaviour behaviour = otherObj->getCollBehaviour();

 	if (behaviour == FIXEDRECT) //it's a brick
	{
		if(((Brick*) otherObj)->getBrickType() != BTYPE_METAL)
		{
			//time_score decrease with the time until it's 0
			this->score += (MIN_SCORE + time_score);
			time_score = INITIAL_TIME_SCORE;
			((Brick*) otherObj)->hit();
			if(((Brick*) otherObj)->isDestroyed())
			{
				otherObj->setEnabled(false);
				otherObj->setVisible(false);
			}
		}
		PlaySoundFromFile(PATH_AUDIO_COLLISION);
 	}
	else if (behaviour == SPECIALRADIAL) //it's a paddle
	{
		PlaySoundFromFile(PATH_AUDIO_COLLISION_PADDLE);
	}

	if(!getCurrentLevelRef()->checkForBricks())
	{
		if (currentLevel != levels.size()-1) {
			gameState = GameState::GSTATE_VICTORY;
			getCurrentScene()->stop();
		}
		else {
			gameState = GameState::GSTATE_ENDED;
			getCurrentScene()->stop();
		}
	}
}

//Called when an object collides with the scene margin, overrides the X2DEventListener method
void Game::collisionWithMargin(X2DRectangle *movingObj, CollisionMargin sceneMargin)
{
	if (sceneMargin == DOWN) 
	{
		if (!loseLife())
		{
			gameState = GameState::GSTATE_GAMEOVER;
			getCurrentScene()->stop();
		}
		getCurrentLevelRef()->resetBall();
	}else{
		PlaySoundFromFile(PATH_AUDIO_COLLISION);
	}
}

//Load all the levels from the Levels folder
void Game::loadLevels()
{
	//Clear all the levels data container
	for(UINT i=0; i<levels.size(); ++i){
		SAFE_DELETE(levels.at(i));
	}
	levels.clear();
	currentLevel = 0;

	//Search for all the files with the specified extension (the levels are loaded in order, depending on their name)
	WIN32_FIND_DATA searchData;
	memset(&searchData, 0, sizeof(WIN32_FIND_DATA));
	HANDLE handle = FindFirstFile(PATH_SEARCH_LEVELS, &searchData);
	while(handle != INVALID_HANDLE_VALUE)
	{
		char * levelFileName = searchData.cFileName;
			levels.push_back(new Level(levelFileName, this));
		if(FindNextFile(handle, &searchData) == FALSE)
			break;
	}
}

//Load the game data from the previous game session
void Game::loadData()
{
	fstream inFile(String(PATH_GAMEDATA).get(), ios::in | ios::binary);
	inFile.seekg(0);

	//READ CURRENT LEVEL
	inFile.read((char*)&currentLevel, sizeof(unsigned int));
}

//Save the game data
void Game::saveData()
{
	ofstream outFile(String(PATH_GAMEDATA).get(), ios::out | ios::binary);
	outFile.seekp(0);

	//WRITE CURRENT LEVEL
	outFile.write((char*)&currentLevel, sizeof(unsigned int));
}

//Delete the saved game data
void Game::deleteSavedData()
{
	remove(PATH_GAMEDATA);
}

//Check whether there is saved data from the latest game session or not
bool Game::checkForSavedData()
{
	ifstream infile(PATH_GAMEDATA);
	return infile.good();
}

//Increase the player's score by the specified amount
void Game::increaseScore(unsigned int value)
{
	score += value;
}

//Decrement the timer for the score bonus
void Game::decrementTimerValue(float secondsPerFrame){
	if(time_score>0)
	{
		time_score-=int(100.f*secondsPerFrame);
		if(time_score<0)
		{
			time_score=0;
		}
	}
}

//Reset all the player's stats (lives and score)
void Game::resetGameStats()
{
	lives = MAX_LIVES;
	score = 0;
}

//Get the player's score
unsigned int Game::getScore() const
{
	return score;
}

//Get the player's lives number
unsigned int Game::getLives() const
{
	return lives;
}

//Decrease the number of lives by one. Returns false if there are no more lives remaining
bool Game::loseLife()
{
	if(lives <= 0)
	{
		return false;
	}
	else {
		lives--;
		getCurrentLevelRef()->resetBall();
	}

	return true;
}

//Returns false if there are no more levels
bool Game::nextLevel()
{
	if(currentLevel >= levels.size() - 1)
	{
		return false;
	}
	currentLevel++;
	lives = MAX_LIVES;
	saveData(); //Save the current level
	resetDisplayedScene();
	resumeGame();
 	return true;
}

//Load the saved level and start it again
void Game::continueGame()
{
	loadData(); //Continue previous game...load data
	lives = MAX_LIVES; 
	score = 0;
	resetDisplayedScene();
	resumeGame();
}

//Restart the whole game
void Game::restartGame()
{
	deleteSavedData(); //New game...delete old data
	currentLevel = 0; //Reset the level to 0
	lives = MAX_LIVES;
	score = 0;
	resetDisplayedScene();
 	resumeGame();
}

//Restart the current level
void Game::restartLevel()
{
	lives = MAX_LIVES;
	score = 0;
	resetDisplayedScene();
	resumeGame();
}

//Start the game when the user throws the ball
void Game::start()
{
	getCurrentLevelRef()->startBall();
}

//Move the paddle to the specified horizontal (X) position
void Game::movePaddle(const unsigned int paddleX)
{
	getCurrentLevelRef()->setPaddleX(float(paddleX));
}

//Resume the game (un-pause)
void Game::resumeGame()
{
	getCurrentScene()->resume();
	gameState = GameState::GSTATE_RUNNING;
}

//Pause the game
void Game::pauseGame()
{
	getCurrentScene()->pause();
	gameState = GameState::GSTATE_PAUSED;
}

//Stop the game
void Game::endGame()
{
	getCurrentScene()->stop();
	gameState = GameState::GSTATE_ENDED;
}

//Update the scene
void Game::update(const float secondsPerFrame)
{
	for (int i = 0; i < UPDATE_FREQUENCY; i++) 
	{
		getCurrentScene()->updatePhysics(secondsPerFrame/UPDATE_FREQUENCY);		
	}
	decrementTimerValue(secondsPerFrame);
	getCurrentScene()->updateScreen();
}

//Set a new mouse action on the game scene
void Game::setOnMouseAction(const MouseKey key,
							const KeyStatus status,
							void(*mouseAction)(const MouseKey, const KeyStatus, const int mouseX, const int mouseY))
{
	for(unsigned int i = 0; i < levels.size(); i++)
	{
		levels.at(i)->getSceneRef()->setOnMouseAction(key, status, mouseAction);
	}
}

//Set a new keyboard action on the game scene
void Game::setOnKeyAction(const Key key,
					const KeyStatus status,	
					void(*keyAction)(const Key, const KeyStatus))
{
	for(unsigned int i = 0; i < levels.size(); i++)
	{
		levels.at(i)->getSceneRef()->setOnKeyAction(key, status, keyAction);
	}
}

//Reset the displayed scene
void Game::resetDisplayedScene()
{
	gameFrame.removeWidgets(); //Remove the old widgets
	gameFrame.addWidget(getCurrentScene()); //Add the current scene as displayed widget
	getCurrentScene()->registerEventListener(this); //Register this game object as an X2DEventListener to the scene
	getCurrentLevelRef()->restart();
}

