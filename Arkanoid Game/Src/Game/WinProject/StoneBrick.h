#pragma once

#include "Brick.h"
#include "BrickState.h"

class StoneBrick : public Brick
{
public:
	StoneBrick(	const int x,
		const int y,
		const unsigned int width,
		const unsigned int height,
		BrickType type
		);
	~StoneBrick();

private:
	BrickState * intero, * scheggiato, * rotto, * distrutto;
};
