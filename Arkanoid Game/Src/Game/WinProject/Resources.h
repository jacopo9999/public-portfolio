#pragma once

#define PATH_FRAME_BACKGROUND "..\\Resources\\UIFrame\\framebackground"
#define PATH_SCENE_BACKGROUND "..\\Resources\\Scene\\scenebackground"
#define PATH_PADDLE "..\\Resources\\GameObjects\\paddle"
#define PATH_BALL "..\\Resources\\GameObjects\\ball"

#define PATH_BRICK "..\\Resources\\GameObjects\\brick"
#define PATH_CLAYBRICK "..\\Resources\\GameObjects\\ClayBrick"				//un colpo
#define PATH_CONCRETEBRICK0 "..\\Resources\\GameObjects\\ConcreteBrick0"	//due colpi
#define PATH_CONCRETEBRICK1 "..\\Resources\\GameObjects\\ConcreteBrick1"	//due colpi
#define PATH_STONEBRICK0 "..\\Resources\\GameObjects\\StoneBrick0"			//tre colpi
#define PATH_STONEBRICK1 "..\\Resources\\GameObjects\\StoneBrick1"			//tre colpi
#define PATH_STONEBRICK2 "..\\Resources\\GameObjects\\StoneBrick2"			//tre colpi
#define PATH_METALBRICK "..\\Resources\\GameObjects\\MetalBrick"			//indistruttibile ( \m/- YOU CAN'T KILL THE METAL -\m/ )

#define PATH_AUDIO_COLLISION_PADDLE "..\\Resources\\Audio\\Collision_Paddle.wav"
#define PATH_AUDIO_COLLISION "..\\Resources\\Audio\\Collision_Other.wav"

#define PATH_GAMEDATA "..\\Data\\gamedata"

#define PATH_DIR_LEVELS "..\\Data\\Levels\\"
#define PATH_SEARCH_LEVELS "..\\Data\\Levels\\*.alevel"
