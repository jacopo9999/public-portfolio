#pragma once

#include "../WinStateMachine/WinStateMachine.h"
#include "../WinGameEngine/WinGameEngine.h"
#include "Resources.h"
#include "BrickState.h"

enum BrickType
{
	BTYPE_CLAY,
	BTYPE_CONCRETE,
	BTYPE_STONE,
	BTYPE_METAL
};

class Brick : public X2DRectangle
{
protected:
	BrickType type;
	bool destroyed;
	int hitPoints;
	String filename;
	LinkedStateMachine stateMachine;

public:
	Brick(const int x,
		  const int y,
		  const unsigned int width,
		  const unsigned int height,
		  const char* filename,
		  const unsigned int mass,
		  BrickType type,
		  const unsigned int hitPoints) :
		X2DRectangle(x, y, width, height, FIXEDRECT, filename, 1.f, mass),
		type(type),
		hitPoints(hitPoints),
		destroyed(false)
	{
		this->filename=filename;
	}

	Brick(const int x,
		const int y,
		const unsigned int width,
		const unsigned int height,
		const unsigned int mass,
		BrickType type,
		const unsigned int hitPoints) :
		X2DRectangle(x, y, width, height, FIXEDRECT, NULL, 1.f, mass),
			type(type),
			hitPoints(hitPoints),
			destroyed(false)
		{
			this->filename="";
		}

	virtual ~Brick(void)
	{
	}

	Brick(const Brick& other) :
		X2DRectangle(other),
		type(other.type),
		hitPoints(other.hitPoints),
		destroyed(other.destroyed),
		filename(other.filename),
		stateMachine(other.stateMachine)
	{}

	Brick& operator=(const Brick& other)
	{
		if (this == &other)
			return *this;

		X2DRectangle::operator=(other);
		type = other.type;
		hitPoints = other.hitPoints;
		destroyed = other.destroyed;
		filename = other.filename;
		stateMachine = other.stateMachine;

		return *this;
	}

	bool isDestroyed(){ 
		return destroyed;
	}

	virtual void updateBrick();

	//it performs the case that the brick is hit with 1 damage 
	virtual void hit();
	//it performs the case that the brick is hit with 1 or more damages 
	virtual void hit(const int damage);
	//it performs the case that the brick is hit with the maximum damage, and it's destroyed
	virtual void destroy();

	BrickType getBrickType() { return type; }
};

