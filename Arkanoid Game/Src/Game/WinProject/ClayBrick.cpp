#include "ClayBrick.h"

ClayBrick::ClayBrick(
	const int x,
	const int y,
	const unsigned int width,
	const unsigned int height,
	BrickType type
	):Brick(x, y, width, height, mass, type, 1)
{
	intero = new BrickState("INTERO",PATH_CLAYBRICK);
	distrutto = new BrickState("DISTRUTTO",NULL,true);
	stateMachine.addState(intero);
	stateMachine.addState(distrutto);
	stateMachine.addEvent("DESTROY",intero,distrutto);
	stateMachine.addEvent("HIT",intero,distrutto);
	stateMachine.setState(intero);
	updateBrick();
}


ClayBrick::~ClayBrick(void)
{
	SAFE_DELETE(intero);
	SAFE_DELETE(distrutto);
}
