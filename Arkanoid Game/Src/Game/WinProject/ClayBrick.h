#pragma once

#include "Brick.h"
#include "BrickState.h"

class ClayBrick : public Brick
{
public:
	ClayBrick(	const int x,
				const int y,
				const unsigned int width,
				const unsigned int height,
				BrickType type
				);
	~ClayBrick();

private:
	BrickState * intero, * distrutto;

};
