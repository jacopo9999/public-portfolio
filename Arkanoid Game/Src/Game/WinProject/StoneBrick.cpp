#include "StoneBrick.h"

StoneBrick::StoneBrick(
	const int x,
	const int y,
	const unsigned int width,
	const unsigned int height,
	BrickType type
	):Brick(x, y, width, height, mass, type, 1)
{
	intero = new BrickState("INTERO",PATH_STONEBRICK0);
	scheggiato = new BrickState("ROTTO",PATH_STONEBRICK1);
	rotto = new BrickState("ROTTO",PATH_STONEBRICK2);
	distrutto = new BrickState("DISTRUTTO",NULL,true);
	stateMachine.addState(intero);
	stateMachine.addState(rotto);
	stateMachine.addState(distrutto);
	stateMachine.addEvent("DESTROY",intero,distrutto);
	stateMachine.addEvent("DESTROY",scheggiato,distrutto);
	stateMachine.addEvent("DESTROY",rotto,distrutto);
	stateMachine.addEvent("HIT",intero,scheggiato);
	stateMachine.addEvent("HIT",scheggiato,rotto);
	stateMachine.addEvent("HIT",rotto,distrutto);
	stateMachine.setState(intero);
	updateBrick();
}


StoneBrick::~StoneBrick(void)
{
	SAFE_DELETE(intero);
	SAFE_DELETE(scheggiato);
	SAFE_DELETE(rotto);
	SAFE_DELETE(distrutto);
}
