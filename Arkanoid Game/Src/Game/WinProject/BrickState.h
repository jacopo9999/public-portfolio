#pragma once
#include "../WinUIWidgets/Defines.h"
#include "../WinStateMachine/State.h"
#include "../WinDraw/String.h"
#include "../WinContainers/WinContainers.h"

class BrickState: public State
{
protected:
	const bool destroyed;
	Bitmap* image;
	String path;
public:
	BrickState(const BrickState& other):State(other),
										destroyed(other.destroyed),
										path(other.path),
										image( BitmapCache::getInstance()->getBitmap(path) )
	{}

	BrickState(char* nomeStato, char* imagePath, const bool destroyed=false):	State(nomeStato),
																				destroyed(destroyed),
																				image( BitmapCache::getInstance()->getBitmap(imagePath) )
	{}

	~BrickState(){
	}

 	Bitmap* getImage(){
 		return image;
 	}

	 bool isDestroyed(){
		 return destroyed;
	 }
};
