#pragma once

#include "../WinGameEngine/WinGameEngine.h"

class Paddle : public X2DRectangle
{
private:

public:

	//constructor
	Paddle(const int x,
		   const int y,
		   const unsigned int width,
		   const unsigned int height,
		   const char* filename) :
		X2DRectangle(x, y, width, height, SPECIALRADIAL, filename)
	{}

	//destructor
	~Paddle() {}

	Paddle(const Paddle& other) : X2DRectangle(other)
	{}

	Paddle& operator=(const Paddle& other)
	{
		if (this == &other)
			return *this;

		X2DRectangle::operator=(other);
		return *this;
	}

	//void incWidth() { this->width *= 1.2f; }		powerup??

};
