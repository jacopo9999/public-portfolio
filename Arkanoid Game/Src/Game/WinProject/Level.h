#pragma once

#include "StoneBrick.h"
#include "MetalBrick.h"
#include "MetalBrick.h"
#include "ClayBrick.h"
#include "ConcreteBrick.h"
#include "Paddle.h"
#include "Ball.h"

#include <fstream>

using namespace std;

/*A level is an object that represent a particular configuration of objects into a scene. It has not the direct ownership of the scene objects
(like X2DScene) but can modify all the game widgets inside of the current scene, for example when resetting their initial position. This class 
is used when loading a level from file, for keeping track of the initial status of the scene objects and for checking whether all the bricks into
the current level have been destroyed or not*/
class Level
{
friend class Game;

private:
	X2DScene *scene; //The reference to the game scene, where all the objects are placed

	LinkedList<int> bricksIndex; //The index of the elements into the scene objects list
	unsigned int ballIndex; //The index of the ball into the scene objects list
	unsigned int paddleIndex; //The index of the paddle into the scene objects list

	//These define the initial status of the scene
 	LinkedList<Brick*> initBricks; //A list of references to the initial brick objects
 	Ball *initBall; //A reference to the initial ball object
 	Paddle *initPaddle; //A reference to the initial paddle object

	VectorList<X2DRectangle*> oldPointers; //A list to the pointers which will be deleted when a new occurs and in the destructor

	X2DScene* Level::getSceneRef() { return scene; } 

	Ball* const getBallRefAt(const unsigned int index) const;
	Paddle* const getPaddleRefAt(const unsigned int index) const;
	Brick* const getBrickRefAt(const unsigned int index) const;

public:
	//Constructor
	Level(String basePath, X2DEventListener* eventListener);

	//Destructor
	~Level();

	void startBall(); //Start the ball movement
	void setPaddleX(const float x); //Move the paddle to the specified horizontal position
	void resetBall(); //Reset the ball to the initial status
	void resetPaddle(); //Reset the paddle to the initial status
	void resetBricks(); //Reset the bricks to their initial position
	void restart(); //Reset the level scene to the initial status
	bool checkForBricks(); //Check whether there are still bricks into the scene or not

private:
	Level(const Level& other) {}
	Level& operator=(const Level& other) {}

};

//This represent the data to be written into the level file representing a single brick object
struct BrickData
{
	unsigned int x;
	unsigned int y;
	unsigned int width;
	unsigned int height;
	BrickType type;
};
