
#pragma once

#include "Container.h"
#include <assert.h>
#include <iostream>

template <typename type>
class LinkedList : public Container<type>
{
private:
	
	//The node class, it contains the data, the pointers to the next element and the previous element in the list
	template <class type>
	class ListElement
	{
	public:
		type dato;
		ListElement<type>* succ;
		ListElement<type>* prec;

		ListElement():succ(nullptr), prec(nullptr) {}
		ListElement(type dato,	ListElement<type>* prec, ListElement<type>* succ):dato(dato), prec(prec), succ(succ) {}
	private:
		ListElement& operator=(const ListElement& other) {}
		ListElement(const ListElement& other){}
	};

	//The length of the list, how many elements the list contains
	unsigned int length;
	//Ponter to the first node of the list
	ListElement<type>* firstElement;
	//Pointer to the last node of the list
	ListElement<type>* lastElement;
	
	//It returns the pointer to the object in the wanted position
	 ListElement<type>* getElement(const unsigned int posizione) const 
	{
		if(firstElement==nullptr || posizione >= length || posizione < 0){
			return nullptr;
		}
		ListElement<type>* tempElement;
		//dimezzo i tempi di ricerca usando due puntatori basandomi sulla posizione
		if(posizione<(length/2)){
			tempElement = firstElement;
			for(unsigned int i=0; i<posizione;i++){
				if( tempElement->succ == nullptr ){
					return nullptr;
				}
				tempElement=tempElement->succ;
			}
		}else{
			tempElement = lastElement;
			for(unsigned int i=length-1; i>posizione;i--){
				if( tempElement->prec == nullptr ){
					return nullptr;
				}
				tempElement=tempElement->prec;
			}
		}
		return tempElement;
	}


public:
	LinkedList() : firstElement(nullptr), lastElement(nullptr), length(0) {
	}

	LinkedList(const LinkedList<type>& other)
	{
		firstElement = nullptr;
		lastElement = nullptr;
		length = 0;
		ListElement<type>* tempElement = other.firstElement;
		for(unsigned int i = 0; (i < other.length) && (tempElement != nullptr) ; i++)
		{
			push_back(tempElement->dato);
			tempElement=tempElement->succ;
		}
	}

	LinkedList& operator=(const LinkedList& other)
	{
		if(this == &other)
		{
			return *this;
		}
		firstElement = nullptr;
		lastElement = nullptr;
		length = 0;
		ListElement<type>* tempElement = other.firstElement;
		for(unsigned int i = 0; (i < other.length) && (tempElement!=nullptr) ; i++)
		{
			push_back(tempElement->dato);
			tempElement=tempElement->succ;
		}
		return *this;
	}

	~LinkedList()
	{
		ListElement<type>* tempElement = firstElement;
		while(tempElement!=nullptr)
		{
			firstElement = tempElement->succ;
			delete tempElement;
			tempElement = firstElement;
		}
	}

	//clear the list removing it's content
	void clear()
	{
		ListElement<type>* tempElement = firstElement;
		while(tempElement != nullptr)
		{
			firstElement = tempElement->succ;
			delete tempElement;
			tempElement = firstElement;
		}
		lastElement = firstElement;
		length = 0;
	}

	//return the data at the wanted position
	type& at(const unsigned int posizione) const
	{ 
		 ListElement<type>* tempElement = getElement(posizione);
		 assert(tempElement != nullptr);
		 return tempElement->dato;
	}

	//insert the value in the front of the list
	void  push_front (const type& val)
	{
		ListElement<type>* tempElement = new ListElement<type>(val, nullptr, firstElement);
		if(firstElement != nullptr)
		{
			firstElement->prec = tempElement;
		}
		firstElement = tempElement;

		if(lastElement == nullptr)
		{
			lastElement = firstElement;
		}
		length++;
	}

	//insert the value in the tail of the list
	void push_back (const type& val)
	{
		if(firstElement == nullptr)
		{
			push_front(val);
			return;
		}
		
		ListElement<type>* tempElement = new ListElement<type>(val, lastElement, nullptr);
		assert(tempElement != nullptr);
		lastElement->succ = tempElement;
		lastElement = tempElement;
		length++;
	}

	//add all the elements of the parameter list in the tail of this list
	void appendList(const LinkedList<type>& other)
	{
		if(other.firstElement!=nullptr)
		{
			if(firstElement==nullptr)
			{
				firstElement=other.firstElement;
			}

			for(unsigned int i = 0; i < other.size(); ++i)
			{
				push_back(other.at(i));
			}
		}
	}


	//add all the elements of the parameter list in the tail of this list and clear the parameter list
	void merge (LinkedList<type>& ow_other)
	{
		if(ow_other.firstElement != nullptr)
		{
			if(firstElement == nullptr)
			{
				firstElement = ow_other.firstElement;
			}
			//tengo nota della sua lunghezza
			length += ow_other.size();
			//l'ultimo elemento punter� come successivo al primo della nuova lista			
			lastElement->succ = ow_other.firstElement;
			//il primo della nuova lista imposter� come nodo precedente all'ultimo della vecchia lista
			ow_other.firstElement->prec=lastElement;
			//infine, avendo gi� collegato le liste, pongo come nuovo ultimo elemento l'ultimo elemento effettivo della somma
			lastElement = ow_other.lastElement;

			//quindi tolgo tutti gli elementi dalla seconda, ma solo in maniera logica perch� fisicamente appartengono a this adesso
			ow_other.firstElement = nullptr;
			ow_other.lastElement = nullptr;
			ow_other.length = 0;
		}
	}

	//set the data in the wanted position
	void setAt(const type nuovoDato, const unsigned int posizione)
	{
		ListElement<type>* tempElement=getElement(posizione);
		assert (tempElement!=nullptr);
		tempElement->dato = nuovoDato;
	}

	//return the position in the list of the parameter element, if it isn't in the list it will return -1
	long positionOf(const type& dato) const
	{
		if(firstElement == nullptr || length < 1)
		{
			return -1;
		}
		ListElement<type>* tempElement = firstElement;
		int i;	
		for(i=0; (tempElement->dato) != dato; i++)
		{
			if (tempElement->succ==nullptr)
			{
				return -1;
			}
			tempElement = tempElement->succ;
		}
		return i;
	}

	//insert the element in the parameter position
	void push_at(const type nuovoDato, const unsigned int posizione)
	{
		 if( (posizione <= 0) || (firstElement == nullptr) )
		 {
			push_front(nuovoDato);
			return;
		 }

		 ListElement<type>* tempElement = firstElement;
		 unsigned int i=0;
		 //dura finch� arriva alla posizione -1, cos� posso eliminare il successivo che si trover� nella posizione
		 while( (i < posizione-1 ) && (tempElement->succ != nullptr) )
		 {
			tempElement = tempElement->succ;
			i++;
		 }

		 ListElement<type>*  llDaAggiungere=nullptr;
		 do
		 {
			llDaAggiungere = new(ListElement<type>);
		 } while(llDaAggiungere==nullptr);

		 llDaAggiungere->succ = tempElement->succ;
		 tempElement->succ->prec = llDaAggiungere;

		 tempElement->succ = llDaAggiungere;
		 llDaAggiungere->prec = tempElement;

		 llDaAggiungere->dato = nuovoDato;

		 length++;
	}

	// remove the element in the parameter position from the list and return its data
	type pop_at(const unsigned int posizione)
	{
		assert(firstElement != nullptr && posizione >= 0 && posizione < length);
		if(posizione == 0)
		{			
			return pop_front();
		}

		ListElement<type>* tempElement = firstElement;
		unsigned int i = 0;
		//dura finch� arriva alla posizione -1, cos� posso eliminare il successivo che si trover� nella posizione
		while(i<posizione-1)
		{
			assert(tempElement->succ!=nullptr);
			tempElement=tempElement->succ;

			i++;
		}

		assert(tempElement->succ != nullptr);

		ListElement<type>*  llDaRimuovere = tempElement->succ;
		type ritorno = llDaRimuovere->dato; 
		tempElement->succ = tempElement->succ->succ;
		tempElement->succ->prec = tempElement;
		SAFE_DELETE(llDaRimuovere);
		length--;
		return ritorno;
	}

	// remove the element in the parameter position from the list
	void deleteAt(const unsigned int posizione)
	{
		assert(firstElement != nullptr && posizione >= 0 && posizione < length);
		if(posizione==0)
		{
			pop_front();
			return;
		}

		ListElement<type>* tempElement = firstElement;
		unsigned int i = 0;
		//dura finch� arriva alla posizione -1, cos� posso eliminare il successivo che si trover� nella posizione
		while(i < posizione-1)
		{
			assert(tempElement->succ != nullptr);
			tempElement=tempElement->succ;
			i++;
		}

		assert(tempElement->succ != nullptr);

		ListElement<type>*  llDaRimuovere = tempElement->succ;
		tempElement->succ = tempElement->succ->succ;
		if(tempElement->succ != nullptr)
		{ //If we are not deleting the last element in the list...
			tempElement->succ->prec = tempElement;
		}
		SAFE_DELETE(llDaRimuovere);
		length--;
	}

	//remove the front element and returns the data contained
	type pop_front()
	{
		assert(firstElement!=nullptr && lastElement!=nullptr);
		ListElement<type>* primo=firstElement;
		if(firstElement==lastElement){
			firstElement=lastElement=nullptr;
		}
		else
		{
			firstElement = firstElement->succ;
			firstElement->prec=nullptr;
		}
		
		type temp = primo->dato;
		SAFE_DELETE(primo);
		length--;
		return temp;
	}

	//return the first element's data
	type front() const
	{
		return firstElement->dato;
	}

	//remove the last element and returns the data contained
	type pop_back()
	{
		assert(firstElement!=NULL && lastElement!=NULL);
		ListElement<type>* ultimo=lastElement;
		if(firstElement==lastElement){
			firstElement=lastElement=nullptr;
		}else{
			lastElement=lastElement->prec;
			lastElement->succ=nullptr;
		}
		type temp = ultimo->dato;
		SAFE_DELETE(ultimo);
		length--;
		return temp;
	}

	//return the last element's data
	type back() const
	{
		return lastElement->dato;
	}

	//return the length of the list
	unsigned int size() const
	{
		return length;
	}

	//return true if the list is empty, false if it's not
	bool isEmpty() const
	{
		return (length==0);
	}

	//print in the standard output the data in the list
	void stampaLista()
	{
		std::cout<<"\n";
		for(unsigned int i = 0; i<length; i++){
			std::cout<<"pos "<<i<<": "<<at(i)<<" ";
		}
	}

};

