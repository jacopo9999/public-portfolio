
#pragma once

#include "Container.h"
#include <assert.h>
#include <iostream>

template <typename T>
class VectorList : public Container<T>
{
private:
	T* data;
	unsigned int length;
	unsigned int capacity;

	//enlarge the capacity of the list, it's used when the length is the same as the capacity
	void enlargeCapacity()
	{
		capacity = (capacity * 2) + 10;
		T* newData = new T[capacity];
		for(unsigned int i = 0; i < length; ++i){
			newData[i] = data[i];
		}		
		delete[] data;
		data = newData;
	}

	//It returns the pointer to the object in the wanted position
	T& getElement(const unsigned int i_posizione) const 
	{
		assert(i_posizione < length);
		return data[i_posizione];
	}

public:
	VectorList(unsigned int i_capacity = 5) : length(0), capacity(i_capacity) {
		data = new T[capacity];
	}

	VectorList( const VectorList<T>& other )
	{
		length = other.length;
		capacity = other.capacity;
		data = new T[capacity];
		for(unsigned int i = 0; i < length; ++i)
		{
			data[i] = other.data[i];
		}
	}

	~VectorList()
	{
		delete[] data;
	}

	//shrink the list capacity to the length
	void shrinkToFit()
	{
		capacity = length;
		T* newData = new T[capacity];

		for(unsigned int i = 0; i < length; ++i){
			newData[i] = data[i];
		}
		
		delete[] data;
		data = newData;
	}

	//it returns the data at the wanted position
	T& at(const unsigned int i_posizione) const
	{ 
		assert(i_posizione < length);
		return data[i_posizione];
	}

	//it erase the list
	void clear()
	{
		delete[] data;
		data = nullptr;
		capacity = 0;
		length = 0;
	}

	//Insert the value in the front of the list
	void  push_front (const T& i_val)
	{
		if(length >= capacity){
			enlargeCapacity();
		}
		for(unsigned int i = length; i > 0; --i){
			data[i] = data[i-1];
		}
		data[0] = i_val;
		++length;
	}

	//Insert the value in the tail of the list
	void push_back (const T& i_val)
	{
		if(length >= capacity){
			enlargeCapacity();
		}
		data[length] = i_val;
		++length;
	}

	//add all the elements of the parameter list in the tail of this list
	void appendList(const VectorList<T>& i_appendingVL)
	{
		while( (length + i_appendingVL->length) > capacity)
		{
			enlargeCapacity();
		}

		for(unsigned int i = 0; i < i_appendingVL.length; ++i, ++length)
		{
			data[length] = i_appendingVL[i];
		}
	}


	//add all the elements of the parameter list in the tail of this list and clear the parameter list
	void merge (VectorList<T>& ow_appendingVL)
	{
		while( (length + i_appendingVL->length) > capacity)
		{
			enlargeCapacity();
		}

		for(unsigned int i = 0; i < i_appendingVL.length; ++i, ++length)
		{
			data[length] = i_appendingVL[i];
		}

		ow_appendingVL.cancellaLista();
	}

	//set the data in the wanted position
	void setAt(const T& i_nuovoDato, const unsigned int i_posizione)
	{
		assert (i_posizione < length);
		data[i_posizione] = i_nuovoDato;
	}

	//return the position in the list of the parameter element, if it isn't in the list it will return -1
	long positionOf(const T& i_dato) const
	{
		for(unsigned int i = 0; i < length; ++i)
		{
			if (data[i] == i_dato)
			{
				return i;
			}
		}
		return -1;
	}


	//insert the element in the parameter position
	bool push_at(const T& i_nuovoDato, const unsigned int i_posizione)
	{
		assert(i_posizione < length);

		if(length >= capacity)
		{
			enlargeCapacity();
		}

		for(unsigned int i = length; i > i_posizione; --i)
		{
			data[i] = data[i-1];
		}
		data[i_posizione] = i_nuovoDato;
		++length;

		return true;
	}

	// remove the element in the parameter position from the list and return its data
	T pop_at(const unsigned int i_posizione)
	{
		assert(i_posizione < length);
		T temp;
		temp = data[i_posizione];
		--length;
		for (unsigned int i = i_posizione; i < length; ++i)
		{
			data[i] = data[i+1];
		}
		return temp;
	}

	// remove the element in the parameter position from the list
	void deleteAt(const unsigned int i_posizione)
	{
		assert(i_posizione < length);
		--length;
		for (unsigned int i = i_posizione; i < length; ++i)
		{
			data[i] = data[i+1];
		}

	}

	//remove the front element and returns the data contained
	T pop_front()
	{
		T temp = data[0];
		--length;
		for (unsigned int i = 0; i < length; ++i)
		{
			data[i] = data[i+1];
		}
		return temp;
	}


	//return the first element's data
	T front() const
	{
		return data[0];
	}

	//remove the last element and returns the data contained
	T pop_back()
	{
		--length;
		return data[length];
	}

	//return the last element's data
	T back() const
	{
		return data[length-1];
	}

	//return the length of the list
	unsigned int size() const
	{
		return length;
	}

	//return true if the list is empty, false if it's not
	bool isEmpty() const
	{
		return (length==0);
	}

	//print in the standard output the data in the list
	void stampaLista()
	{
		std::cout<<"\n";
		for(unsigned int i = 0; i < length; ++i){
			std::cout<< "pos "<< i <<": "<< data[i] << " ";
		}
	}

};

