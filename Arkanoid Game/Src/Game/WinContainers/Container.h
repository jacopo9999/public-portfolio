#pragma once

#define SAFE_DELETE(a) { delete (a); (a) = NULL; }
#define SAFE_DELETE_ARRAY(a) { delete[] (a); (a) = NULL; }

template <typename type>
class Container {
	protected:
		//Add interface methods here (pure virtuals)
};