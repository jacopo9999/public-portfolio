This is the Level Editor for the Arkanoid Game. It has been developed in C++/Qt using Visual Studio.

This tool has been created as a project for the course of Tools Programming (Master in Computer Games Development).

With the tools is possible to:

- Create new levels for the game;
- Create new bricks by selecting their type from a table and then double-clicking into the scene at the position where the item should be placed;
- Delete bricks by double-clicking on them;
- Move bricks by dragging them;
- Save the created scene in a *.alevel file to be used into the game;
- Load an existing scene from a *.alevel file;
- Check collisions between the objects and with the scene (this is done automatically by the software).

Some sample levels are available into the Data/Levels folder in the Arkanoid main directory.
