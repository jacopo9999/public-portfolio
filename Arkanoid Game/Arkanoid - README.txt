Set of Libraries for 2D Games creation and Demo Project (Arkanoid)

This is a set of libraries for creating 2D games, this project has been developed for the Master in Computer Games Development
 and it has been developed in C++ using Visual Studio.

The solution is composed of 6 projects:

- WinDraw is a library for creating application windows using the Windows APIs and drawing bitmaps, animations and text into them.
It allows to catch user input from mouse and keyboard as well;

- WinUIWidgets is a library for drawing UI objects into the created window, it uses WinDraw as a basis. It defines
several GUI objects (widgets) such as labels, checkboxes, buttons, etc. and containers for them (frames). It also handles the delivery
of the user actions to the right GUI element and allows the user to specify custom functions to be executed in response to specific keyboard/mouse
combinations;

- WinGameEngine is a library which contains the functions for handling the 2D physics of the game objects, it is based on the WinUIWidgets library
but extends it by defining widgets and frames specializations which are able to move and detect collisions;

- WinContainers is a library for custom template containers, it defines vectors and linked lists and it is used as a replacement for the STL containers;

- WinStateMachine is a library which can be used for creating generic finite state machines (FSMs) by defining machine states and connections
between them.

- WinArkanoid (WinProject) is the demo game project, which is a demonstration of how the set of libraries can be used.


