#pragma once

#include <string>
#include <list>

namespace Utils
{
	template <typename T>
	void getListElementsAsString(const std::list<T>& path, std::string& outString)
	{
		outString = "";
		for (auto node : path)
		{
			outString += node;
			outString += " ";
		}
	}

	template <>
	void getListElementsAsString<int>(const std::list<int>& path, std::string& outString)
	{
		outString = "";
		for (auto node : path)
		{
			outString += std::to_string(node);
			outString += " ";
		}
	}

}
