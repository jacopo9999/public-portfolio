
#include <iostream>
#include <cassert>

#include "PathfindingSolver.h"
#include "Utils.h"

using namespace std;

template <typename T>
void RunTest(const std::string& testDescriptionStr, const Graph<T>& graph, const Path<T>& inputPath, const std::string& expectedResultDescriptionStr)
{
	cout << "Running Test: " << testDescriptionStr << endl;

	Subgraph<T> subgraph;
	PathfindingSolver::findSubgraph<T>(graph, inputPath, subgraph);

	string inputPathStr = "";
	Utils::getListElementsAsString<T>(inputPath, inputPathStr);
	cout << "Input Path: " << inputPathStr << endl;

	string resultSubgraphStr = "";
	Utils::getListElementsAsString(subgraph, resultSubgraphStr);
	cout << "Subgraph Found: " << resultSubgraphStr << endl;

	cout << "Expected Result: " << expectedResultDescriptionStr << endl << endl;
}

int main()
{
	// UNIT TESTS //
	cout << "Running Test Cases:" << endl;

	//1. Graph: A, X, Y, B, C, W, V (CYCLICAL GRAPH)
	Graph<char> graph1;

	graph1.addConnection('A', 'X');
	graph1.addConnection('X', 'B');
	graph1.addConnection('B', 'C');
	graph1.addConnection('X', 'Y');
	graph1.addConnection('Y', 'C');
	graph1.addConnection('Y', 'W');
	graph1.addConnection('W', 'C');
	graph1.addConnection('V', 'C');
	graph1.addConnection('W', 'V');

	Path<char> inputPath1 = { 'A', 'B', 'C' };
	RunTest("TEST 1", graph1, inputPath1, "A X B C");

	Path<char> inputPath2 = { 'B', 'C', 'A' };
	RunTest("TEST 2", graph1, inputPath2, "B C Y X A W V (or any other combination with B C A in this order)");

	//2. Graph: 1, 2, 3, 4, 5, 6, 7 (ACYCLICAL GRAPH)
	Graph<int> graph2;

	graph2.addConnection(1, 2);
	graph2.addConnection(1, 3);

	graph2.addConnection(2, 4);
	graph2.addConnection(2, 5);

	graph2.addConnection(3, 6);
	graph2.addConnection(3, 7);

	Path<int> inputPath3 = { 1, 3, 6 };
	RunTest("TEST 3", graph2, inputPath3, "1 3 6");

	Path<int> inputPath4 = { 6, 3, 1 };
	RunTest("TEST 4", graph2, inputPath4, "6 3 1");

	Path<int> inputPath5 = { 1, 5, 2 };
	RunTest("TEST 5", graph2, inputPath5, "NONE");

	Path<int> inputPath6 = { 4, 5, 6, 7 };
	RunTest("TEST 6", graph2, inputPath6, "NONE");

	Path<int> inputPath7 = { 1, 2, 5, 6, 3 };
	RunTest("TEST 7", graph2, inputPath7, "NONE");

	// Let's connect the E and F nodes to create a cycle, then execute again with the input path used previously
	Path<int> inputPath8 = inputPath7;
	graph2.addConnection(5, 6);

	RunTest("TEST 8", graph2, inputPath8, "1 2 5 6 3");

	//3. CROSS GRAPH - Need to pass multiple times from the same node to reach the other ones
	Graph<std::string> graph3;

	graph3.addConnection("X", "A");
	graph3.addConnection("X", "B");
	graph3.addConnection("X", "C");
	graph3.addConnection("X", "D");

	// Only one crossing of X
	Path<std::string> inputPath9 = { "A", "B" };
	RunTest("TEST 9", graph3, inputPath9, "A X B");

	// Multiple crossings of X
	Path<std::string> inputPath10 = { "A", "B", "C", "D" };
	RunTest("TEST 10", graph3, inputPath10, "NONE");

	//4. EMPTY INPUT PATH - EDGE CASE
	Path<std::string> inputPath11 = {};
	RunTest("TEST 11", graph3, inputPath11, "NONE (INCORRECT INPUT)");

	//5. NO NODE FROM THE INPUT PATH IS IN THE GRAPH - EDGE CASE
	Path<std::string> inputPath12 = { "Y", "Z" };
	RunTest("TEST 12", graph3, inputPath12, "NONE (INCORRECT INPUT)");

	//6. THE INITIAL NODE FROM THE INPUT PATH IS NOT IN THE GRAPH - EDGE CASE
	Path<std::string> inputPath13 = { "Y", "X" };
	RunTest("TEST 13", graph3, inputPath13, "NONE (INCORRECT INPUT)");

	//7. EMPTY GRAPH - EDGE CASE
	Graph<std::string> graphEmpty;

	Path<std::string> inputPath14 = { "A", "B", "C" };
	RunTest("TEST 14", graphEmpty, inputPath14, "NONE (INCORRECT INPUT)");

	return 0;
}


