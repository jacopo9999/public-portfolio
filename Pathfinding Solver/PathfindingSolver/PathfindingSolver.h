/*

Author: Jacopo Vezzosi

PathfindingSolver

*/

#pragma once

#include <string>
#include <map>
#include <list>
#include <vector>
#include <iostream>

template <typename T>
using Path = std::list<T>;
template <typename T>
using Subgraph = std::list<T>;
template <typename T>
using Connections = std::list<T>;
template <typename T>
using ValidPaths = std::list<Path<T>>;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

enum class PathState
{
	INCOMPLETE,
	COMPLETE,
	INVALID
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
Can represent any graph, cyclical or acyclical
*/
template <typename T>
class Graph
{
private:
	template <typename T>
	using NodesMap = std::map<const T, Connections<T>>;

	NodesMap<T> mNodes;

public:

	/*
	Adds a connection between nodes 1 and 2, paths are double connections (1->2 and 2->1)
	*/
	inline void addConnection(const T& node1, const T& node2)
	{
		mNodes[node1].push_back(node2);
		mNodes[node2].push_back(node1);
	}

	/*
	Returns all the connections to the specified node, if the specified node is not found then returns nullptr
	*/
	inline const Connections<T>* const getConnections(const T& node) const
	{
		typename NodesMap<T>::const_iterator foundNode = mNodes.find(node);
		return foundNode != mNodes.cend() ? &foundNode->second : nullptr;
	}
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*
PathfindingSolver

This class provides functionality to return the subgraph of a graph, given a path as an input (a set of nodes of the graph that need
to be visited in order).
The algorithm uses DFS (Depth Frist Search) to explore the graph provided as input. Each path of graph is explored until the algorithm either:
1- Finds all the nodes of the input path, in that case it adds the nodes of the found path into the output subgraph, or
2- Finds invalid nodes (input path nodes which are not in the correct order) in that case it discards the path found, or
3- Finds already visited nodes (being them nodes from the input path or not) which means that it has found a cycle and
then it discards the path found, or
4- It cannot find any further connection to explore but it didn't find all the input path nodes, in that case it discards the path found.
*/
namespace PathfindingSolver
{
	/*
	Explore the subgraph of the graph that starts from the last node of currentPath that is used as root, the result is then compared with timetable and returned on the output variable if valid
	*/
	template <typename T>
	void exploreSubgraph(const Graph<T>& graph, const Path<T>& inputPath, const Path<T>& currentPath, /*OUT*/ Subgraph<T>& outSubgraph);

	// PRIVATE METHODS
	namespace {
		/*
		Check that the path is valid if compared with the used input, the function returns COMPLETE if the path contains all the input path nodes in the same order, INCOMPLETE if it
		contains a subset of them, INVALID if contains duplicate nodes (cyclical paths) and/or if the input path nodes do not appear in the same order
		*/
		template <typename T>
		PathState checkPath(const Path<T>& inputPath, const Path<T>& currentPath);
		/*
		Returns the subgraph that includes all the possible paths between the input path nodes, in the same order as they appear on it
		*/
		template <typename T>
		void findSubgraph(const Graph<T>& graph, const Path<T>& inputPath, /*OUT*/ Subgraph<T>& outSubgraph);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TEMPLATES IMPLEMENTATION
#include "PathfindingSolver.tpp"
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

