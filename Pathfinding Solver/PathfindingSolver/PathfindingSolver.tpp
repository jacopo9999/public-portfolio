
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <typename T>
void PathfindingSolver::findSubgraph(const Graph<T>& graph, const Path<T>& inputPath, /*OUT*/ Subgraph<T>& outSubgraph)
{
	outSubgraph.clear();

	if (inputPath.size() == 0)
	{
		std::cout << "WARNING: The input path is empty!" << std::endl;
		return; //Empty input path
	}

	typename Path<T>::const_iterator iter = inputPath.begin();
	if (graph.getConnections(*iter) == nullptr) //Begin the exploration from the first input path node (if it exists)
	{
		std::cout << "WARNING: Cannot find this node into the input path: <" << *iter << "> into the graph!" << std::endl;
		return; //The first node was not found in the graph
	}

	const Path<T> startingPath = { *iter };
	exploreSubgraph<T>(graph, inputPath, startingPath, outSubgraph);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <typename T>
PathState PathfindingSolver::checkPath(const Path<T>& inputPath, const Path<T>& currentPath)
{
	// Check that the path does not contain any duplicate of a node (cycles) - including transit ones
	for (typename Path<T>::const_iterator firstIter = currentPath.cbegin(); firstIter != currentPath.cend(); ++firstIter)
	{
		typename Path<T>::const_iterator secondIter = firstIter;
		++secondIter;
		for (; secondIter != currentPath.cend(); ++secondIter)
		{
			if (*firstIter == *secondIter)
			{
				return PathState::INVALID;
			}
		}
	}

	// Check that the path contains all the nodes from the input path in order, otherwise return INVALID (if they are in the wrong order) or INCOMPLETE (if some are missing)
	// We assume that the check for duplicate has already been successfully executed
	Path<T> tmpPath;
	for (const T& currentPathNode : currentPath)
	{
		for (const T& inputPathNode : inputPath)
		{
			if (currentPathNode == inputPathNode)
			{
				tmpPath.push_back(currentPathNode);
			}
		}
	}

	typename Path<T>::const_iterator tmpIter = tmpPath.cbegin();
	for (typename Path<T>::const_iterator ttIter = inputPath.cbegin(); ttIter != inputPath.cend(); ++ttIter)
	{
		if (*tmpIter != *ttIter)
		{
			return PathState::INVALID; // Wrong order!
		}
		++tmpIter;
		if (tmpIter == tmpPath.cend())
		{
			break;
		}
	}

	assert(tmpPath.size() <= inputPath.size());

	if (tmpPath.size() < inputPath.size())
	{
		return PathState::INCOMPLETE; // Not all the input path nodes have been found inside the path!
	}

	return PathState::COMPLETE;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template <typename T>
void PathfindingSolver::exploreSubgraph(const Graph<T>& graph, const Path<T>& inputPath, const Path<T>& currentPath, /*OUT*/ Subgraph<T>& outSubgraph)
{
	const T& root = currentPath.back();

	const Connections<T>* const rootConnections = graph.getConnections(root);
	if (rootConnections == nullptr)
	{
		return; // No matching root node found in the graph. Terminate early.
	}

	// Keep exploring the sub-trees
	for (const T& connectedNode : *rootConnections)
	{
		Path<T> newPath = currentPath;
		newPath.push_back(connectedNode);

		const PathState pathState = checkPath(inputPath, newPath);
		if (pathState == PathState::COMPLETE) // If the path is valid it is not necessary to explore further and the solution found is added to the valid paths output
		{
			for (const T& newPathNode : newPath)
			{
				bool found = false;
				for (const T& outSubgraphNode : outSubgraph)
				{
					if (newPathNode == outSubgraphNode)
					{
						found = true;
						break;
					}
				}
				if (found == false)
				{
					outSubgraph.push_back(newPathNode);
				}
			}
			continue;
		}
		else if (pathState == PathState::INVALID) // If the path is invalid, no need to explore further
		{
			continue;
		}
		else if (pathState == PathState::INCOMPLETE) // If the path does not include all the nodes from the input path then continue to look inside the sub-graphs
		{
			// TODO: make this iterative using a stack to make it more performant and less likely to stack overflow
			PathfindingSolver::exploreSubgraph(graph, inputPath, newPath, outSubgraph);
		}
		else // We should never end up here
		{
			assert(false);
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////