
This projects demonstrates the use of some common AI algorithms for games.

The project is based on the SFML library, which is used only for display purposes, the AI logic has been written using custom C++ classes.

There are three demos in the project, each of them can be selected in the starting menu (I would recommend having a look at the last demo as it is the most interesting one).

- A-Star Example : an example of how the A-Star algorithm can be used to find the path from a source cell to a destination cell in a custom grid (the grid definition is hard-coded).

- Puzzle of 15 Solver : an example of how the puzzle of 15 can be solved, it uses a variation of the A-Star algorithm;

- Miner : an example of NPC behaviours implementation using different kinds of Steering Behaviours (Wandering, Flee, Avoidance, Pursuit, etc.) implemented by using a Finite States Machine. 

How the Miner demo works:
The yellow dwarves move between the home (cyan rectangle) the mine (grey rectangle) and the pub (pink rectangle) depending on their needs (when they work they gain money, when they need to sleep they go home and when they are thirsty they go to the 
pub and pay money for drinks). The dog (red) pursues the wandering sheeps (grey) and when barking (it becomes green) makes them escape. The sheeps and the dog always stay inside the fence (white rectangle).

-- NOTE: in the first two examples the algorithm calculations are made in the loading phase, the second phases just shows the steps of the algorithm. The reason because the calculation is not done simultaneously to 
the display is to be able to approximate the time efficiency of the algorithm without having to take into account the rendering time.





Created by: Jacopo Vezzosi
Please do not redistribute this demo code.