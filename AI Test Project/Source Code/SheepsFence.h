#pragma once

#include "Location.h"

class SheepsFence : public Location
{
public:
	SheepsFence() {}
	SheepsFence(float positionX, float positionY, float width, float height, sf::String text = "") : Location(positionX, positionY, width, height, 20, text) 
	{
		setFillColor(sf::Color::White);
	}

	~SheepsFence(void) {}
};

