#include "FifteenPuzzleScene.h"

FifteenPuzzleScene::FifteenPuzzleScene() : mState(LOADING)
{
	//GUI Init////////////////////////////////

	//Load the font
	mFont.loadFromFile("consola.ttf");

	//Create the message for the loading screen
	mLoadingMsg = sf::Text("Processing...please wait!", mFont, 26U);
	mLoadingMsg.setPosition(100.0f, 100.0f);
	mLoadingMsg.setColor(sf::Color::Red);
	mLoadingMsg.setCharacterSize(35);

	/////////////////////////////////////////

	//Create the screen for displaying the results
	for(int j = 0; j < 4; j++)
	{
		for(int k = 0; k < 4; k++)
		{
			const unsigned int index = j*4 + k;

			mCells[index] = sf::RectangleShape(sf::Vector2f(CELL_SIZE, CELL_SIZE));
			float cellX = CELL_SIZE * k + TABLE_OFFSET_X;
			float cellY = CELL_SIZE * j + TABLE_OFFSET_Y;

			mCells[index].setPosition(cellX, cellY);
			mCells[index].setOutlineColor(sf::Color::Blue);
			mCells[index].setOutlineThickness(TABLE_BORDER_THICKNESS);

			mCellsText[index] = sf::Text("", mFont, 28U);
			mCellsText[index].setPosition(cellX + TEXT_OFFSET_IN_CELL_X, cellY + TEXT_OFFSET_IN_CELL_Y);
			mCellsText[index].setColor(sf::Color::Black);
		}
	}

	mThread = new sf::Thread(&FifteenPuzzleScene::Exec, this);
	mThread->launch();
}

FifteenPuzzleScene::~FifteenPuzzleScene()
{
    delete mThread;
}

void FifteenPuzzleScene::OnIdle()
{
}

void FifteenPuzzleScene::Exec()
{
	AStarPuzzle AStarSearch;
	std::array<int,16> numbers = {12,	10,		5,	8,
		4,	EMPTY,	2,	11,
		14,	7,		9,	1,
		3,	15,		13,	6};

	if(AStarSearch.start(Board(numbers), mBoardsPath))
	{
		std::cout << "Path found!!" << std::endl;
		mState = ApplicationState::DISPLAYING_RESULT;
	}
	else 
	{
		std::cout << "Failed to find a solution!" << std::endl;
		return;
	}

	for(int i = mBoardsPath.size() - 1; i >= 0; --i)
	{
		std::string boardStr;
		Board board = mBoardsPath.at(i);
		for(int j = 0; j < 4; j++)
		{
			for(int k = 0; k < 4; k++)
			{
				const unsigned int index = j*4 + k;

				if(board.getValue(j,k) == EMPTY)
					mCells[index].setFillColor(sf::Color::Black);
				else if(isValueInFinalPosition(board.getValue(j, k), j, k))
					mCells[index].setFillColor(sf::Color::Green);
				else mCells[index].setFillColor(sf::Color::Red);

				std::string value = board.getValueAsString(j,k);
				mCellsText[index].setString(value);
			}
		}
		sf::sleep(sf::seconds(0.3f));
	}
}

bool FifteenPuzzleScene::isValueInFinalPosition(const unsigned int value, const unsigned int row, const unsigned int col) const
{
	return (value == row*4+col+1);
}

void FifteenPuzzleScene::OnDraw(sf::RenderWindow& renderWindow)
{
	renderWindow.clear();
	if(mState == ApplicationState::LOADING)
		renderWindow.draw(mLoadingMsg);
	else if(mState == ApplicationState::DISPLAYING_RESULT)
	{
		for(unsigned int i = 0; i < 16; i++)
		{
			renderWindow.draw(mCells[i]);
			renderWindow.draw(mCellsText[i]);
		}
	}
	else 
	{
		std::cout << "ERROR: invalid application state!" << std::endl;
		exit(1);
	}
}