#pragma once

#include <math.h>

#include <assert.h>

#include "SFML/Graphics.hpp"

namespace Vector2fMath 
{
	inline sf::Vector2f operator*(const sf::Vector2f& lhs, float rhs);
	inline sf::Vector2f operator*(float lhs, const sf::Vector2f& rhs);
	inline sf::Vector2f operator-(const sf::Vector2f& lhs, const sf::Vector2f& rhs);
	inline sf::Vector2f operator+(const sf::Vector2f& lhs, const sf::Vector2f& rhs);
	inline sf::Vector2f operator/(const sf::Vector2f&lhs , float val);
	inline float length(const sf::Vector2f& vector);
	inline float squaredLength(const sf::Vector2f& vector);
	inline float distance(const sf::Vector2f& v1, const sf::Vector2f& v2);
	inline bool isZero(const sf::Vector2f& vector);
	inline sf::Vector2f normalize(const sf::Vector2f& vector);
	inline float dotProduct(const sf::Vector2f& v1, const sf::Vector2f& v2);
	inline sf::Vector2f average(const std::vector<sf::Vector2f> vectorsList);

	inline sf::Vector2f operator*(const sf::Vector2f& lhs, float rhs)
	{
		sf::Vector2f result(lhs);
		result *= rhs;
		return result;
	}

	inline sf::Vector2f operator*(float lhs, const sf::Vector2f& rhs)
	{
		sf::Vector2f result(rhs);
		result *= lhs;
		return result;
	}

	inline sf::Vector2f operator-(const sf::Vector2f& lhs, const sf::Vector2f& rhs)
	{
		sf::Vector2f result(lhs);
		result.x -= rhs.x;
		result.y -= rhs.y;

		return result;
	}

	inline sf::Vector2f operator+(const sf::Vector2f& lhs, const sf::Vector2f& rhs)
	{
		sf::Vector2f result(lhs);
		result.x += rhs.x;
		result.y += rhs.y;

		return result;
	}

	inline sf::Vector2f operator/(const sf::Vector2f&lhs , float val)
	{
		sf::Vector2f result(lhs);
		result.x /= val;
		result.y /= val;

		return result;
	}

	inline float length(const sf::Vector2f& vector)
	{
		return sqrt(squaredLength(vector));
	}

	inline float squaredLength(const sf::Vector2f& vector)
	{
		return vector.x * vector.x + vector.y * vector.y;
	}

	inline float distance(const sf::Vector2f& v1, const sf::Vector2f& v2)
	{
		float ySeparation = v2.y - v1.y;
		float xSeparation = v2.x - v1.x;

		return sqrt(ySeparation * ySeparation + xSeparation * xSeparation);
	}

	inline bool isZero(const sf::Vector2f& vector) 
	{
		return (vector.x == 0.f && vector.y == 0.f);
	}

	inline sf::Vector2f normalize(const sf::Vector2f& vector)
	{
		if(length(vector) == 0.0f) return sf::Vector2f(0.0f,0.0f);
		float vLength = length(vector);
		sf::Vector2f retv(vector.x / vLength, vector.y / vLength);
		return retv;
	}

	inline float dotProduct(const sf::Vector2f& v1, const sf::Vector2f& v2)
	{
		return v1.x * v2.x + v1.y * v2.y;
	}

	inline sf::Vector2f average(const std::vector<sf::Vector2f> vectorsList)
	{
		if(vectorsList.size() == 0)
			return sf::Vector2f(0.0f, 0.0f);
		float sumX = 0, sumY = 0;
		for(std::vector<sf::Vector2f>::const_iterator it = vectorsList.begin(); it != vectorsList.end(); ++it)
		{
			sumX += it->x;
			sumY += it->y;
		}
		return sf::Vector2f(sumX / vectorsList.size(), sumY / vectorsList.size());
	}

}
