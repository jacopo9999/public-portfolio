#include "DogWanderingState.h"

void DogWanderingState::enter(DogNPC& agent) const
{
	agent.startWanderBehaviour();
}

void DogWanderingState::update(DogNPC& agent) const
{
	sf::Vector2f newTargetPosition;
	DogDetectionResult result = agent.targetSheeps(newTargetPosition); 
	if(result == ISOLATED_SHEEP_AROUND)
	{
		//If the dog identifies an isolated sheep, it goes towards it
		agent.getFSM()->changeState(DogPursuingState::getInstance());

	}
	else if(result == GROUPED_SHEEP_AROUND)
	{
		//If the dog identifies a group of sheeps, it goes to a central position in the middle of them thus making them escape
		agent.setTargetPosition(newTargetPosition);
		agent.getFSM()->changeState(DogBarkingState::getInstance());
	}
	else if(result == NO_SHEEPS_AROUND)
	{
		//Mantain the same behaviour!
	}
	else
	{
		assert(false); //Error! State not recognised!
	}
	return; 
}

void DogWanderingState::exit(DogNPC& agent) const
{
}
