#pragma once

#include <assert.h>
#include <cmath>
#include <iostream>
#include <string>
#include <array>

#define EMPTY -1

enum SwapDirection
{
	SWAPDIR_LEFT,
	SWAPDIR_RIGHT,
	SWAPDIR_UP,
	SWAPDIR_DOWN
};

class Board
{
public:
	Board() {}
	Board(std::array<int,16> valuesList);
	~Board(void) {}

	Board(const Board& other);
	Board& operator=(const Board& other);

	bool operator==(const Board& other);
	bool operator!=(const Board& other);

	bool swapDown();
	bool swapUp();
	bool swapLeft();
	bool swapRight();

	const float getCost();

	void print() const;

	const unsigned int getValue(const unsigned int i, const unsigned int j) const 
	{
		return mCells[i][j];
	}

	const std::string getValueAsString(const unsigned int i, const unsigned int j) const
	{
		if(mCells[i][j] == EMPTY)
			return "-";
		else return std::to_string(mCells[i][j]);
	}
private:
	int mCells[4][4];

	struct Coords
	{
		unsigned int x, y;
	};

	Coords getEmptyPosition() const;

	void swapTiles(Coords tile1, Coords tile2);

	const float getTileCost(const unsigned int posX, const unsigned int posY) const;
};
