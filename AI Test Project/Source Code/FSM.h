#pragma once

#include <assert.h>

#include "State.h"
#include "GlobalState.h"

template<class Agent>
class FSM
{
private:
	const State<Agent> * mCurrentState;
	const GlobalState<Agent> * mGlobalState;
	
	Agent& mOwner;

public:
	FSM(Agent& agent,
		const State<Agent> * startState,
		const GlobalState<Agent>* globalState = nullptr) : 
			mCurrentState(startState), 
			mGlobalState(globalState),
			mOwner(agent) 
	{
		assert(startState != nullptr);
		startState->enter(agent);
	}
	~FSM(void)
	{
		delete mCurrentState;
		delete mGlobalState;
	}

	void changeState(State<Agent>* state) 
	{
		assert(state != nullptr);
		mCurrentState->exit(mOwner);
		mCurrentState = state;
		mCurrentState->enter(mOwner);
	}

	void update(Agent& agent)
	{
		mCurrentState->update(agent);
		if(mGlobalState != nullptr) //It is not mandatory to have a global state
			mGlobalState->update(agent);
	}
};

