#pragma once

#include "Location.h"

class Home : public Location
{
public:
	Home() {}
	Home(float positionX, float positionY, float width, float height, sf::String text = "") : Location(positionX, positionY, width, height, 20, text) 
	{
		setFillColor(sf::Color::Cyan);
	}

	~Home(void) {}
};

