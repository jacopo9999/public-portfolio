#pragma once

#include "IScene.h"
#include "FontManager.h"
#include <assert.h>
#include <vector>
#include <list>
#include <time.h>
#include <sstream>

#include "AStar.h"

typedef std::vector<std::vector<sf::RectangleShape>> RectangleShapesMatrix;

class AStarScene : public IScene
{
private:
	enum ApplicationState{
		LOADING = 0,
		DISPLAYING_RESULT
	} mState;

	//CONSTANTS
	const unsigned int mMapRows, mMapColumns, mStartX, mStartY, mGoalX, mGoalY;

	const float mCellSize;
	const float mTableOffsetX, mTableOffsetY;
	const float mTableBorderThickness;
	//

	sf::Thread * mThread;

	sf::Text mLoadingMsg;
	sf::Font mFont;

	MatrixStates mCurrentMatrixStates;
	RectangleShapesMatrix mCells;

	std::vector<int> mNotUsableCells;

	void PrintMatrixStates(const MatrixStates& inMatrixStates, RectangleShapesMatrix& oMatrix) const;

public:
    
	AStarScene();
    
	virtual void OnIdle(void);
	virtual void OnDraw(sf::RenderWindow&);
    
	void Exec();

	virtual ~AStarScene(void);
};


