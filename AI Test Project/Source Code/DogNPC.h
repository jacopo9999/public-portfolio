#pragma once

#include <sstream>

#include "IScene.h"

#include "FSM.h"
#include "State.h"
#include "NPC.h"

#include "SheepNPC.h"

enum DogDetectionResult
{
	NO_SHEEPS_AROUND = 0,
	ISOLATED_SHEEP_AROUND,
	GROUPED_SHEEP_AROUND
};

class DogNPC : public NPC
{
private:
	void exec();

	//External pointers:
	NPC * ePursuedSheep;
	//

	FSM<DogNPC> * mFSM;

	const SteeringBehaviour * mPursuitBehaviour;

	float mSheepsGroupRange; //The range under which the dog considers the sheeps as "grouped"
	bool mIsBarking;

public:
	DogNPC(const SteeringBehaviour * standardBehaviour, 
		   const SteeringBehaviour * pursuitBehaviour,
		   Scene& scene, 
		   const sf::FloatRect boundingBox, 
		   const State<DogNPC> * startState, 
		   const GlobalState<DogNPC> * globalState = nullptr) : 
		NPC(10.0f,
			15.0f,
			80.0f,
			2.0f,
			0.3f,
			sf::Color::Red,
			standardBehaviour,
			boundingBox,
			scene),
		mSheepsGroupRange(30.0f),
		mIsBarking(false),
		mPursuitBehaviour(pursuitBehaviour)
	{
		mFSM = new FSM<DogNPC>(*this, startState, globalState);
	}
	~DogNPC(void) {}

	void startFSM();
	void stopFSM();

	/*Check the status of the sheeps inside the fence, if it find a group of sheeps it returns the average position among these ones
	if it doesn't find a group, it returns the position of the sheep closest to the dog NPC, if there are no sheeps inside the fence
	returns nothing*/
	DogDetectionResult targetSheeps(sf::Vector2f& o_targetPosition) const;

	void startPursuitBehaviourOnClosestSheep();
	void stopPursuingSheep();
	void startWanderBehaviour();
	void updatePositionOfPursuedSheep();

	//GETTERS:
	FSM<DogNPC> * getFSM() const { return mFSM; }
	bool isBarking() const { return mIsBarking; }

	//SETTERS:
	void setBarking(const bool isBarking) { mIsBarking = isBarking; isBarking? setFillColor(sf::Color::Green) : setFillColor(sf::Color::Red); }

};
