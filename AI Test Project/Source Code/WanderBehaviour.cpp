#include "WanderBehaviour.h"

//NOTE: the WanderBehaviour ignores the target position, it uses only the bounding box
sf::Vector2f WanderBehaviour::getNewSpeed(const NPC * npc) const
{
	static sf::Vector2f randomTargetPos;
	if(std::rand() % 20 == 1) 
	{
		sf::FloatRect boundingBox = npc->getBoundingBox();
		float randX = (rand() % int(boundingBox.width)) + boundingBox.left;
		float randY = (rand() % int(boundingBox.height)) + boundingBox.top;
		randomTargetPos = sf::Vector2f(randX, randY);
	}

	//Here it is the same as the seek behaviour but with the new target position:
	sf::Vector2f distance = randomTargetPos - npc->getPosition();
	sf::Vector2f desiredSpeed = normalize(distance) * getMaxSpeedModule();
	sf::Vector2f steeringForce = (desiredSpeed - npc->getSpeed()) * npc->getMaxSteeringForce();
	sf::Vector2f newSpeed = npc->getSpeed() + steeringForce;

	return newSpeed;
}