
#include "PubState.h"

void PubState::enter(DwarfNPC& agent) const
{
	agent.goToPub();
}

void PubState::update(DwarfNPC& agent) const
{
	if(agent.getThirst() <= 0)
	{
		agent.getFSM()->changeState(MineState::getInstance());	
		return;
	}

	while(!agent.hasArrivedAtTarget())
		return; //Do not do anything if the agent has not arrived

	while(!agent.isBusy() && !agent.enterPub())
		return; //Do not do anything if the location is full

	agent.setBusy(true);
	agent.increaseThirst(-1.0f);
}

void PubState::exit(DwarfNPC& agent) const
{	
	if(agent.isBusy())
	{
		agent.exitPub();
	}
	agent.setBusy(false);
}
