
#include "MineState.h"

void MineState::enter(DwarfNPC& agent) const
{
	agent.goToMine();
}

void MineState::update(DwarfNPC& agent) const
{
	if(agent.getGold() > 50)
	{
		agent.getFSM()->changeState(HomeState::getInstance());	
		return;
	}
	if(agent.getThirst() > 30.0f)
	{
		agent.getFSM()->changeState(PubState::getInstance());	
		return;
	}

	while(!agent.hasArrivedAtTarget())
		return; //Do not do anything if the agent has not arrived

	while(!agent.isBusy() && !agent.enterMine())
		return; //Do not do anything if the location is full

	agent.setBusy(true);
	agent.increaseGold(1);
}

void MineState::exit(DwarfNPC& agent) const
{
	if(agent.isBusy())
	{
		agent.exitMine();
	}
	agent.setBusy(false);
}