#pragma once

#include <SFML/Graphics.hpp>

//Locations:
#include "Mine.h"
#include "Home.h"
#include "Pub.h"
#include "SheepsFence.h"

#include "Vector2fMath.h"

class DwarfNPC;
class SheepNPC;
class DogNPC;
class NPC;

class Scene : public sf::RectangleShape
{
private:

	//Locations
	SheepsFence mFence;
	Home mHome;
	Pub mPub;
	Mine mMine;

	//NPCs
	std::vector<DwarfNPC*> mDwarves;
	std::vector<SheepNPC*> mSheeps;
	std::vector<DogNPC*> mDogs;

public:
	Scene() {}
	Scene(float width, float height) : sf::RectangleShape(sf::Vector2f(width, height))
	{
		setFillColor(sf::Color::Green);
	}
	~Scene(void);

	void drawIn(sf::RenderWindow * renderWindow);
	
	NPC* getClosestNPC(const std::vector<NPC*> npcs, const NPC* npc) const;

	const std::vector<NPC*> getNPCsInRangeTowardsDirection(const NPC * npc, float range) const;
	const std::vector<NPC*> getDogsInRange(const NPC * npc, float range) const;
	const std::vector<NPC*> getDwarvesInRange(const NPC * npc, float range) const;
	const std::vector<NPC*> getSheepsInRange(const NPC * npc, float range) const;
	
	bool isNPCOutsideLocationBounds(const NPC * npc, const Location * boundingLocation);

	//Setters:
	void setSheepsFence(SheepsFence fence) {mFence = fence;}
	void setHome(Home home) {mHome = home;}
	void setPub(Pub pub) {mPub = pub;}
	void setMine(Mine mine) {mMine = mine;}

	//Getters:
	SheepsFence& getSheepsFenceRef() {return mFence;}
	Home& getHomeRef() {return mHome;}
	Pub& getPubRef() {return mPub;}
	Mine& getMineRef() {return mMine;}

	//NPCs
	std::vector<DwarfNPC*> getDwarvesRefs() const {return mDwarves;}
	std::vector<SheepNPC*> getSheepsRefs() const {return mSheeps;}
	std::vector<DogNPC*> getDogsRefs() const {return mDogs;}
	void addDwarf(DwarfNPC* dwarf) {mDwarves.push_back(dwarf);}
	void addSheep(SheepNPC* sheep) {mSheeps.push_back(sheep);}
	void addDog(DogNPC* dog) {mDogs.push_back(dog);}
};

