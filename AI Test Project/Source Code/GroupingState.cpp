
#include "GroupingState.h"

void GroupingState::enter(SheepNPC& agent) const
{
	agent.startGroupingBehaviour();
}

void GroupingState::update(SheepNPC& agent) const
{
	if(agent.isBarkingDogInSightRange()) //If there is a dog in sight, escape from it
		agent.getFSM()->changeState(EscapingState::getInstance());	
}

void GroupingState::exit(SheepNPC& agent) const
{
	agent.stopMovement();
}

