#include "PursuedState.h"

void PursuedState::enter(SheepNPC& agent) const
{
	std::cout << "pursued! going to: " << agent.getTargetPosition().x << "," << agent.getTargetPosition().y << std::endl;

	agent.startPursuedBehaviour();
}

void PursuedState::update(SheepNPC& agent) const
{
	if(agent.isBarkingDogInSightRange()) //If there is a dog in sight, escape from it
		agent.getFSM()->changeState(EscapingState::getInstance());	
	else if(!agent.isPursued())
		agent.getFSM()->changeState(GroupingState::getInstance());
	else ; //Do not change the state!
}

void PursuedState::exit(SheepNPC& agent) const
{
	agent.stopMovement();
}