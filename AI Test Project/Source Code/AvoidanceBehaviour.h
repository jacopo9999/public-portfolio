#pragma once

#include "SteeringBehaviour.h"

/* the avoidance behaviour allows an NPC to avoid collision with another NPC, it doesn't use a target positon*/
class AvoidanceBehaviour
{
public:
	AvoidanceBehaviour() {}
	~AvoidanceBehaviour() {}

	sf::Vector2f getNewSpeed(const NPC * npc, const NPC * collidingNPC) const;
};

