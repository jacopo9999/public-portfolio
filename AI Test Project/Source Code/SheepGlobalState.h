#pragma once

#include "GlobalState.h"

#include "SheepNPC.h"

class SheepGlobalState : public GlobalState<SheepNPC>
{
private:

	SheepGlobalState(void) : GlobalState<SheepNPC>() {}
	~SheepGlobalState(void) {}

public:
	static SheepGlobalState * getInstance()
	{
		static SheepGlobalState * mSingleton;
		if(mSingleton == NULL)
		{
			mSingleton = new SheepGlobalState();
		}
		return mSingleton;
	}

	void enter(SheepNPC& agent) const;
	void update(SheepNPC& agent) const;
	void exit(SheepNPC& agent) const;
};
