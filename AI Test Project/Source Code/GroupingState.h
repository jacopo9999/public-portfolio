#pragma once

#include <iostream>

#include "State.h"

#include "SheepNPC.h"

#include "EscapingState.h"

class GroupingState : public State<SheepNPC>
{
private:

	//PUT HERE THE VARIABLES!

	GroupingState(void) : State<SheepNPC>() {}
	~GroupingState(void) {}

public:
	static GroupingState * getInstance()
	{
		static GroupingState * mSingleton;
		if(mSingleton == NULL)
		{
			mSingleton = new GroupingState();
		}
		return mSingleton;
	}

	void enter(SheepNPC& agent) const;
	void update(SheepNPC& agent) const;
	void exit(SheepNPC& agent) const;
};

