#pragma once

#include <iostream>
#include <assert.h>

#include "State.h"

#include "DogNPC.h"

#include "DogPursuingState.h"
#include "DogBarkingState.h"

class DogWanderingState : public State<DogNPC>
{
private:

	//PUT HERE THE VARIABLES!

	DogWanderingState(void) : State<DogNPC>() {}
	~DogWanderingState(void) {}

public:
	static DogWanderingState * getInstance()
	{
		static DogWanderingState * mSingleton;
		if(mSingleton == NULL)
		{
			mSingleton = new DogWanderingState();
		}
		return mSingleton;
	}

	void enter(DogNPC& agent) const;
	void update(DogNPC& agent) const;
	void exit(DogNPC& agent) const;
};