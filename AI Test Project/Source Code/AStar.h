//-------------------------------------------------------------------------------------------------
// A-STAR ALGORITHM IMPLEMENTATION
// AUTHOR: Jacopo Vezzosi - Master in Computer Games Development - University of Verona
//-------------------------------------------------------------------------------------------------

#pragma once

#include "NodeState.h"

#include <iostream>
#include <vector>

typedef std::vector<std::vector<NodeState>> MatrixStates;

class AStar
{
private:
	class Node;

public:
	struct Map
	{
		std::vector<Node> nodes;
		const unsigned int rows,
			columns,
			startx,
			starty,
			goalx,
			goaly;

		Map(const unsigned int rows,
			const unsigned int columns,
			const unsigned int startx,
			const unsigned int starty,
			const unsigned int goalx,
			const unsigned int goaly);

		std::vector<Node*> getAdjNodes(Node* node);
		Node* getNode(const unsigned int x, const unsigned int y);
	};

private:
	class Node
	{
	private:
		std::vector<Node*> adj;

		const unsigned int x, y;

		NodeState state;
		Node* previous;
		float score;

	public:
		Node(const unsigned int x, const unsigned int y) : 
			x(x), y(y), state(NodeState::Undefined), score(0), previous(nullptr) {}
		~Node(void) {}

		inline bool operator==(const Node& other)
		{ 
			return (x == other.x && y == other.y) ? true : false ;
		}

		void addAdjs(std::vector<Node*> nodes) 
		{ 
			adj.insert(adj.end(), nodes.begin(), nodes.end());
		}

		void setState(NodeState state) {this->state = state;}
		NodeState getState(void) const {return this->state;}

		void setPrevious(AStar::Node* previous) {this->previous = previous;}
		AStar::Node* getPrevious(void) const {return this->previous;}

		void setScore(float score) {this->score = score;}
		float getScore(void) const {return this->score;}

		const unsigned int X() const {return this->x;}
		const unsigned int Y() const {return this->y;}
	};

	AStar::Node *currentNode;
	Map *map;

	AStar::Node* getLowerScoreNode();
	void setCurrentNode(AStar::Node* node) {currentNode = node;}
	bool hasHigherGScore(AStar::Node* node) const;
	bool isGoalNode(const AStar::Node* node) const;
	bool isStartNode(const AStar::Node* node) const;
	bool openNodesExist() const;
	const float manhattanDistance(AStar::Node* startNode, AStar::Node* endNode) const;
	const float manhattanHeuristic(AStar::Node* node) const;
	std::vector<const AStar::Node*> getPath();

public:
	AStar(Map* map) : map(map), currentNode(nullptr) {}
	~AStar(void) {}

	std::vector<MatrixStates> startSearch();

	MatrixStates getMapState() const;
};


