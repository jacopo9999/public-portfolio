#include "EscapingState.h"

void EscapingState::enter(SheepNPC& agent) const
{
	agent.setOutlineColor(sf::Color::Black);
	agent.setOutlineThickness(3);
	agent.startEscapeBehaviour();
}
 
void EscapingState::update(SheepNPC& agent) const
{ 
	if(!agent.isBarkingDogInSightRange()) //If there is not a dog in sight, try to join other sheeps
		agent.getFSM()->changeState(GroupingState::getInstance());	
}

void EscapingState::exit(SheepNPC& agent) const
{
	agent.setOutlineThickness(0);
	agent.stopMovement();
}


