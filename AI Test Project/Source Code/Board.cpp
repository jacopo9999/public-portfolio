#include "Board.h"

Board::Board(std::array<int,16> valuesList)
{
	for(unsigned int i = 0; i < 4; i++)
		for(unsigned int j = 0; j < 4; j++)
			mCells[i][j] = valuesList.at(i*4+j);
	mCells[4][4] = EMPTY;
}

Board::Board(const Board& other)
{
	for(unsigned int i = 0; i < 4; i++)
		for(unsigned int j = 0; j < 4; j++)
			mCells[i][j] = other.mCells[i][j];
}

Board& Board::operator=(const Board& other)
{
	if(this != &other)
	{
		for(unsigned int i = 0; i < 4; i++)
			for(unsigned int j = 0; j < 4; j++)
				mCells[i][j] = other.mCells[i][j];
	}
	return *this;
}

bool Board::operator==(const Board& other)
{
	for(unsigned int i = 0; i < 4; i++)
	{
		for(unsigned int j = 0; j < 4; j++)
		{
			if(mCells[i][j] != other.mCells[i][j])
				return false;
		}
	}
	return true;
}

bool Board::operator!=(const Board& other)
{
	return !operator==(other);
}

const float Board::getCost()
{
	float cost = 0.f;
	for(unsigned int i = 0; i < 4; i++)
	{
		for(unsigned int j = 0; j < 4; j++)
		{
			cost += getTileCost(i, j);
		}
	}
	return cost;
}

void Board::print() const
{
	for(unsigned int i = 0; i < 4; i++)
	{
		std::cout << std::endl;
		for(unsigned int j = 0; j < 4; j++)
		{
			if(mCells[i][j] != EMPTY)
				std::cout << mCells[i][j] << "\t";
			else std::cout << "-\t";
		}
	}
	std::cout << std::endl;
}

Board::Coords Board::getEmptyPosition() const
{
	for(unsigned int i = 0; i < 4; i++)
		for(unsigned int j = 0; j < 4; j++)
			if(mCells[i][j] == EMPTY)
			{
				Coords coords;
				coords.x = i;
				coords.y = j;
				return coords;
			}
			assert("FATAL ERROR: Cannot find empty position into the selected board!!");
			return Coords();
}

bool Board::swapDown() //fill the empty position moving a tile downward
{
	Coords emptyPos = getEmptyPosition();
	if(emptyPos.y == 0)
		return false;
	else
	{
		Coords swapTilePos;
		swapTilePos.x = emptyPos.x;
		swapTilePos.y = emptyPos.y - 1;
		swapTiles(emptyPos, swapTilePos);
		return true;
	}
}

bool Board::swapUp() //fill the empty position moving a tile upward
{
	Coords emptyPos = getEmptyPosition();
	if(emptyPos.y == 3)
		return false;
	else
	{
		Coords swapTilePos;
		swapTilePos.x = emptyPos.x;
		swapTilePos.y = emptyPos.y + 1;
		swapTiles(emptyPos, swapTilePos);
		return true;
	}
}

bool Board::swapLeft() //fill the empty position moving a tile on the left
{
	Coords emptyPos = getEmptyPosition();
	if(emptyPos.x == 0)
		return false;
	else
	{
		Coords swapTilePos;
		swapTilePos.x = emptyPos.x - 1;
		swapTilePos.y = emptyPos.y;
		swapTiles(emptyPos, swapTilePos);
		return true;
	}
}

bool Board::swapRight() //fill the empty position moving a tile on the right
{
	Coords emptyPos = getEmptyPosition();
	if(emptyPos.x == 3)
		return false;
	else
	{
		Coords swapTilePos;
		swapTilePos.x = emptyPos.x + 1;
		swapTilePos.y = emptyPos.y;
		swapTiles(emptyPos, swapTilePos);
		return true;
	}
}

void Board::swapTiles(Coords tile1, Coords tile2)
{
	const unsigned int tmpTile = mCells[tile1.x][tile1.y];
	mCells[tile1.x][tile1.y] = mCells[tile2.x][tile2.y];
	mCells[tile2.x][tile2.y] = tmpTile;
}

const float Board::getTileCost(const unsigned int posX, const unsigned int posY) const
{
	const unsigned int tileValue = mCells[posX][posY];
	if(tileValue == EMPTY)
		return 0.f; //If the tile is empty it has no cost
	Coords targetPos;
	targetPos.x = (tileValue-1)/4;
	targetPos.y = (tileValue-1)%4;

	//Heuristic calculation:
	const unsigned int manhattanDHorizontal = std::abs(int(posX - targetPos.x));
	const unsigned int manhattanDVertical = std::abs(int(posY - targetPos.y));
	return float(manhattanDHorizontal) + float(manhattanDVertical);

	//return (targetPos.x == posX && targetPos.y == posY) ? 0.0f : 1.0f;
}