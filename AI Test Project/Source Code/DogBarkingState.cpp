#include "DogBarkingState.h"

void DogBarkingState::enter(DogNPC& agent) const
{
	agent.setBarking(true);
}

void DogBarkingState::update(DogNPC& agent) const
{
	if(agent.hasArrivedAtTarget())
	{
		sf::Vector2f newTargetPosition;
		DogDetectionResult result = agent.targetSheeps(newTargetPosition); 
		if(result == NO_SHEEPS_AROUND)
		{
			agent.getFSM()->changeState(DogWanderingState::getInstance());
		}
		else if(result == ISOLATED_SHEEP_AROUND)
		{
			//If the dog identifies an isolated sheep, it goes towards it
			agent.getFSM()->changeState(DogPursuingState::getInstance());
		}
		else if(result == GROUPED_SHEEP_AROUND)
		{
			//Mantain the same behaviour but update the target position with the current position of the pursued group of sheeps
			agent.setTargetPosition(newTargetPosition);
		}
		else
		{
			assert(false); //Error! State not recognised!
		}
	}
	return; 
}

void DogBarkingState::exit(DogNPC& agent) const
{
	agent.setBarking(false);
}
