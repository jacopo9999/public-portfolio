//-------------------------------------------------------------------------------------------------
// A-STAR ALGORITHM IMPLEMENTATION
// AUTHOR: Jacopo Vezzosi - Master in Computer Games Development - University of Verona
//-------------------------------------------------------------------------------------------------

#include "AStar.h"

AStar::Map::Map(const unsigned int rows,
		 const unsigned int columns,
		 const unsigned int startx,
		 const unsigned int starty,
		 const unsigned int goalx,
		 const unsigned int goaly) :
	rows(rows), columns(columns), startx(startx), starty(starty), goalx(goalx), goaly(goaly)
{
	nodes.clear();
	for(unsigned int i = 0; i < rows; i++)
	{
		for(unsigned int j = 0; j < columns; j++)
		{
			Node node(i,j);
			nodes.push_back(node);
		}
	}
	for(unsigned int i = 0; i < nodes.size(); i++)
	{
		nodes.at(i).addAdjs(getAdjNodes(&nodes.at(i)));
	}
}

std::vector<AStar::Node*> AStar::Map::getAdjNodes(AStar::Node* node)
{
	std::vector<Node*> output;
	Node* adj1 = getNode(node->X()-1,	node->Y()-1);
	Node* adj2 = getNode(node->X()-1,	node->Y());
	Node* adj3 = getNode(node->X()-1,	node->Y()+1);
	Node* adj4 = getNode(node->X(),		node->Y()-1);
	Node* adj5 = getNode(node->X(),		node->Y()+1);
	Node* adj6 = getNode(node->X()+1,	node->Y()-1);
	Node* adj7 = getNode(node->X()+1,	node->Y());
	Node* adj8 = getNode(node->X()+1,	node->Y()+1);
	if(adj1 != nullptr)
		output.push_back(adj1);
	if(adj2 != nullptr)
		output.push_back(adj2);
	if(adj3 != nullptr)
		output.push_back(adj3);
	if(adj4 != nullptr)
		output.push_back(adj4);
	if(adj5 != nullptr)
		output.push_back(adj5);
	if(adj6 != nullptr)
		output.push_back(adj6);
	if(adj7 != nullptr)
		output.push_back(adj7);
	if(adj8 != nullptr)
		output.push_back(adj8);
	return output;
}

AStar::Node* AStar::Map::getNode(const unsigned int x, const unsigned y)
{
	for(unsigned int i = 0; i < nodes.size(); i++)
	{
		if(nodes.at(i).X() == x && nodes.at(i).Y() == y)
			return &nodes.at(i);
	}
	return nullptr;
}

std::vector<MatrixStates> AStar::startSearch()
{
	//Restore map state
	for(unsigned int i = 0; i < map->nodes.size(); i++)
	{
		if(map->nodes.at(i).getState() != NodeState::NotUsable)
			map->nodes.at(i).setState(NodeState::Undefined);
		map->nodes.at(i).setScore(0);
		map->nodes.at(i).setPrevious(nullptr);
	}
	Node *startNode = map->getNode(map->startx, map->starty);
	startNode->setState(NodeState::Open);

	//Init the list of output matrices
	std::vector<MatrixStates> outMatrixStates;

	//Start search algorithm
	Node * currentNode;
	while(openNodesExist())
	{
		///single algorithm step//////////////////////////////
		currentNode = getLowerScoreNode();
		if(isGoalNode(currentNode))
		{
			break; //Goal node found, terminate the search!
		}
		currentNode->setState(NodeState::Closed);
		std::vector<Node*> adjNodes = map->getAdjNodes(currentNode);
		for(unsigned int i = 0; i < adjNodes.size(); i++)
		{
			Node * neighborNode = adjNodes.at(i);
			if(neighborNode->getState() == NodeState::Closed || neighborNode->getState() == NodeState::NotUsable)
				continue;
			const float tentativeScore = currentNode->getScore() + manhattanDistance(currentNode, neighborNode) + manhattanHeuristic(neighborNode);
			if(neighborNode->getState() == NodeState::Undefined || tentativeScore < neighborNode->getScore() + manhattanHeuristic(neighborNode))
			{
				neighborNode->setPrevious(currentNode);
				neighborNode->setScore(tentativeScore);
				if(neighborNode->getState() == NodeState::Undefined)
					neighborNode->setState(NodeState::Open);
			}
		}
		////////////////////////////////////////////////

		//Add the current map to the output vector
		outMatrixStates.push_back(getMapState());
		//
	}
	return outMatrixStates;
}

const float AStar::manhattanDistance(Node* startNode, Node* endNode) const
{
	float outDistance = 0.0f;
	outDistance += 1.0f * abs(int(endNode->X() - startNode->X()));
	outDistance += 1.0f * abs(int(endNode->Y() - startNode->Y()));
	return outDistance;
}

const float AStar::manhattanHeuristic(AStar::Node* node) const
{
	float outDistance = 0.0f;
	outDistance += 1.0f * abs(int(map->goalx - node->X()));
	outDistance += 1.0f * abs(int(map->goaly - node->Y()));
	return outDistance;
}

bool AStar::openNodesExist() const
{
	for(unsigned int i = 0; i < map->nodes.size(); i++)
	{
		if(map->nodes.at(i).getState() == NodeState::Open)
			return true;
	}
	return false;
}

bool AStar::isGoalNode(const Node* node) const
{
	if(map->goalx == node->X() && map->goaly == node->Y())
		return true;
	return false;
}

bool AStar::isStartNode(const Node* node) const
{
	if(map->startx == node->X() && map->starty == node->Y())
		return true;
	return false;
}

AStar::Node* AStar::getLowerScoreNode()
{
	Node* output = nullptr;
	for(unsigned int i = 0; i < map->nodes.size(); i++)
	{
		if(map->nodes.at(i).getState() == NodeState::Open)
			if(output == nullptr || map->nodes.at(i).getScore() <= output->getScore())
				output = &map->nodes.at(i);
	}
	return output;
}

bool AStar::hasHigherGScore(AStar::Node* node) const
{
	for(unsigned int i = 0; i < map->nodes.size(); i++)
	{
		if(map->nodes.at(i).getScore() > node->getScore())
			return false;
	}
	return true;
}

std::vector<const AStar::Node*> AStar::getPath()
{
	std::vector<const Node*> path;
	const Node* ptr = map->getNode(map->goalx, map->goaly);
	while(ptr != nullptr && ptr->getPrevious() != nullptr)
	{
		path.push_back(ptr);
		ptr = ptr->getPrevious();
	}
	return path;
}

MatrixStates AStar::getMapState() const
{
	unsigned int currentRow = 0;
	MatrixStates outMap;
	const Node* ptr = map->getNode(map->goalx, map->goaly);

	//Fill the rows with empty vectors
	for(unsigned int i = 0; i < map->rows; i++)
	{
		outMap.push_back(std::vector<NodeState>());
	}

	//Start filling the cells
	for(unsigned int i = 0; i < map->nodes.size(); i++)
	{
		if(map->nodes.at(i).X() > currentRow)
		{
			currentRow = map->nodes.at(i).X();
		}
		if(isStartNode(&map->nodes.at(i)))
		{
			outMap.at(currentRow).push_back(NodeState::Start);
			continue;
		}
		bool found = false;
		ptr = map->getNode(map->goalx, map->goaly);
		while(ptr != nullptr && ptr->getPrevious() != nullptr)
		{
			ptr = ptr->getPrevious();
			if(map->nodes.at(i) == *ptr)
			{
				outMap.at(currentRow).push_back(NodeState::Path);
				found = true;
			}
			if(found)
			{
				break;
			}
		}
		if(found)
			continue;
		if(isGoalNode(&map->nodes.at(i)))
		{
			outMap.at(currentRow).push_back(NodeState::Goal);
			continue;
		}
		else
		{
			outMap.at(currentRow).push_back(map->nodes.at(i).getState());
		}
	}
	return outMap;
}