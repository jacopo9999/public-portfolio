#include "Scene.h"

#include "DwarfNPC.h"
#include "DogNPC.h"
#include "SheepNPC.h"

Scene::~Scene(void)
{
	//Delete the NPCs
	for(unsigned int i = 0; i < mDwarves.size(); i++)
	{
		delete mDwarves.at(i);
	}
	for(unsigned int i = 0; i < mSheeps.size(); i++)
	{
		delete mSheeps.at(i);
	}
	for(unsigned int i = 0; i < mDogs.size(); i++)
	{
		delete mDogs.at(i);
	}
}

void Scene::drawIn(sf::RenderWindow * renderWindow)
{
	renderWindow->draw(*this);

	renderWindow->draw(mFence);
	renderWindow->draw(mHome);
	renderWindow->draw(mPub);
	renderWindow->draw(mMine);
}

NPC* Scene::getClosestNPC(const std::vector<NPC*> npcs, const NPC* npc) const
{
	if(npc->isBusy()) return nullptr; //Returns nothing if the NPC is idle...
	 
	NPC* retptr = nullptr;
	 
	float distance = 0.0f;
	
	 for(unsigned int i = 0; i < npcs.size(); i++)
	 {
	 	if(npc->getPosition() != npcs.at(i)->getPosition() &&
   		   !npcs.at(i)->isBusy())
	 	{
	 		//Collision!
	 		if(Vector2fMath::distance(npc->getPosition(), npcs.at(i)->getPosition()) < distance || distance == 0.0f)
	 		{
	 			retptr = npcs.at(i);
	 			distance = Vector2fMath::distance(npc->getPosition(), npcs.at(i)->getPosition());
	 		}			
	 	}
	 }

	 return retptr;
}

//Return the positions of the NPC inside the range, towards a specified direction, ignoring the busy ones
const std::vector<NPC*> Scene::getNPCsInRangeTowardsDirection(const NPC * npc, float range) const
{
	std::vector<NPC*> out;
	 
	for(unsigned int i = 0; i < mDwarves.size(); i++)
	{
		if(npc->getPosition() != mDwarves.at(i)->getPosition() &&
				!mDwarves.at(i)->isBusy() &&
				CollisionDetection::betweenAheadVectorAndCircle(*npc, npc->getSpeed(), range, *mDwarves.at(i)))
	 	{
	 		out.push_back(mDwarves.at(i));	
	 	}
	}
	for(unsigned int i = 0; i < mDogs.size(); i++)
	{
		if(npc->getPosition() != mDogs.at(i)->getPosition() &&
			!mDogs.at(i)->isBusy() &&
				CollisionDetection::betweenAheadVectorAndCircle(*npc, npc->getSpeed(), range, *mDogs.at(i)))
		{
			out.push_back(mDogs.at(i));	
		}
	}
	for(unsigned int i = 0; i < mSheeps.size(); i++)
	{
		if(npc->getPosition() != mSheeps.at(i)->getPosition() &&
			!mSheeps.at(i)->isBusy() &&
				CollisionDetection::betweenAheadVectorAndCircle(*npc, npc->getSpeed(), range, *mSheeps.at(i)))
		{
			out.push_back(mSheeps.at(i));	
		}
	}
	 
	return out;
}

//Return the number of dwarves in the specified range, ignore the busy ones, returns the positions of the NPC inside the range
const std::vector<NPC*> Scene::getDwarvesInRange(const NPC * npc, float range) const
{
	std::vector<NPC*> out;

	for(unsigned int i = 0; i < mDwarves.size(); i++)
	{
		if(npc->getPosition() != mDwarves.at(i)->getPosition() &&
			!mDwarves.at(i)->isBusy() &&
			CollisionDetection::shapeInsideRange(*mDwarves.at(i), npc->getPosition(), range))
		{
			out.push_back(mDwarves.at(i));	
		}
	}

	return out;
}

//Return the number of dogs in the specified range, ignore the busy ones, returns the positions of the NPC inside the range
const std::vector<NPC*> Scene::getDogsInRange(const NPC * npc, float range) const
{
	std::vector<NPC*> out;

	for(unsigned int i = 0; i < mDogs.size(); i++)
	{
		if(npc->getPosition() != mDogs.at(i)->getPosition() &&
			!mDogs.at(i)->isBusy() &&
			CollisionDetection::shapeInsideRange(*mDogs.at(i), npc->getPosition(), range))
		{
			out.push_back(mDogs.at(i));	
		}
	}

	return out;
}

//Return the number of sheeps in the specified range, ignore the busy ones, returns the positions of the NPC inside the range
const std::vector<NPC*> Scene::getSheepsInRange(const NPC * npc, float range) const
{
	std::vector<NPC*> out;

	for(unsigned int i = 0; i < mSheeps.size(); i++)
	{
		if(npc->getPosition() != mSheeps.at(i)->getPosition() &&
			!mSheeps.at(i)->isBusy() &&
			CollisionDetection::shapeInsideRange(*mSheeps.at(i), npc->getPosition(), range))
		{
			out.push_back(mSheeps.at(i));
		}
	}

	return out;
}

bool Scene::isNPCOutsideLocationBounds(const NPC * npc, const Location * boundingLocation)
{
	return !CollisionDetection::betweenTwoShapes(*npc, *boundingLocation);
}

