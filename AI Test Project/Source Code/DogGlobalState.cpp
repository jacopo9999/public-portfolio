#include "DogGlobalState.h"

void DogGlobalState::enter(DogNPC& agent) const
{
}

void DogGlobalState::update(DogNPC& agent) const
{
	agent.update();
}

void DogGlobalState::exit(DogNPC& agent) const
{
}