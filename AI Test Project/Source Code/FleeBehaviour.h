#pragma once

#include "SteeringBehaviour.h"

using namespace Vector2fMath;

/* With the flee behaviour the NPC will try to escape from the target position using the most direct path*/
class FleeBehaviour : public SteeringBehaviour
{
public:
	FleeBehaviour(const float maxSpeedModule) : SteeringBehaviour(maxSpeedModule) {}
	~FleeBehaviour() {}

	sf::Vector2f getNewSpeed(const NPC * npc) const;
};

