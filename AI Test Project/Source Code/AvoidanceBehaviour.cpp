#include "AvoidanceBehaviour.h"

sf::Vector2f AvoidanceBehaviour::getNewSpeed(const NPC * npc, const NPC * collidingNPC) const
{
	sf::Vector2f ahead = npc->getSpeed();
	sf::Vector2f projectedPosition = npc->getPosition() + npc->getSpeed();
	float dist = distance(collidingNPC->getPosition(), projectedPosition);
	if(dist > collidingNPC->getRadius())
	{
		return npc->getSpeed();
	}
	else
	{
		sf::Vector2f steeringVector = normalize(collidingNPC->getPosition() - projectedPosition);
		return -steeringVector * npc->getMaxSteeringForce();
	}
}