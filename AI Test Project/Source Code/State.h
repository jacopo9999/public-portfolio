#pragma once

#include <vector>

template<class Agent>
class State
{
protected:

	State(void) {}
	virtual ~State(void) {}

public:

	virtual void enter(Agent& agent) const = 0;
	virtual void update(Agent& agent) const= 0;
	virtual void exit(Agent& agent) const = 0;
};

