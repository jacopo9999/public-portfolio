#include "DogNPC.h"

void DogNPC::startFSM()
{
	mFSMThread = new sf::Thread(&DogNPC::exec, this);
	mFSMThread->launch();
}

void DogNPC::stopFSM()
{
	mFSMThread->terminate();
}

void DogNPC::exec()
{
	while(true)
	{
		mFSM->update(*this);
		sf::sleep(sf::seconds(UPDATE_FREQUENCY));
	}
}

DogDetectionResult DogNPC::targetSheeps(sf::Vector2f& o_targetPosition) const
{
	std::vector<SheepNPC*> vSheeps = eScene.getSheepsRefs();

	if(vSheeps.size() == 0) //No sheeps detected at all, do not modify the target position and return
		return NO_SHEEPS_AROUND;

	for(std::vector<SheepNPC*>::iterator sheepIter = vSheeps.begin();
		sheepIter != vSheeps.end();
		sheepIter++) //Iterate through all the sheeps
	{
		const SheepNPC* sheep = *sheepIter;
		const std::vector<NPC*> vSheepsInRange = eScene.getSheepsInRange(sheep, mSheepsGroupRange);
		if(vSheepsInRange.size() > 3) //One of the sheeps is in a group of more than 3 sheeps
		{
			std::vector<sf::Vector2f> vPositions;
			for(std::vector<NPC*>::const_iterator sheepInRangeIter = vSheepsInRange.begin();
				sheepInRangeIter != vSheepsInRange.end();
				sheepInRangeIter++) //Add all the positions of the sheeps in the group to a positions vector
			{
				sf::Vector2f newPosition = (*sheepInRangeIter)->getPosition();
				vPositions.push_back(newPosition);
			}			
			o_targetPosition = average(vPositions); //Add the average of the positions as the target
			return GROUPED_SHEEP_AROUND; //Returns the result as a group of sheep detected
		}
	}

	//If there are no groups of sheeps, try to find the closest isolated one
	const NPC * closestNPC = eScene.getClosestNPC(eScene.getSheepsInRange(this, getSightRange()), this);
	if(closestNPC != NULL && typeid(*closestNPC) == typeid(SheepNPC)) //If there is a close sheep, return an "isolated sheep" result and set its position as the target position 
	{
		o_targetPosition = closestNPC->getPosition();
		return ISOLATED_SHEEP_AROUND;
	}

	//There are sheeps but not in the range of sight of the dog, so return "No sheeps around" and do not change the target position
	return NO_SHEEPS_AROUND;
}

void DogNPC::startPursuitBehaviourOnClosestSheep()
{
	//Find the closest sheep 
	ePursuedSheep = eScene.getClosestNPC(eScene.getSheepsInRange(this, getSightRange()), this);

	NPC * closestToPursuedSheep = eScene.getClosestNPC(eScene.getSheepsInRange(this, 500.0f), ePursuedSheep); //Find the closest sheep to the selected one
	if(closestToPursuedSheep != nullptr) //The new target of the sheep is the position of that sheep
	{
		ePursuedSheep->setTargetPosition(closestToPursuedSheep->getPosition());
	}
	else //If there are no other sheeps stay idle
	{
		ePursuedSheep->stopMovement();
	}

	//Change the behaviour of the dog
	mCurrentBehaviour = &mPursuitBehaviour;
}

void DogNPC::updatePositionOfPursuedSheep()
{
	if(ePursuedSheep != nullptr)
		setTargetPosition(ePursuedSheep->getPosition());
}

void DogNPC::stopPursuingSheep()
{
	ePursuedSheep = nullptr;
}

void DogNPC::startWanderBehaviour()
{
	mCurrentBehaviour = &mStandardBehaviour;
}
