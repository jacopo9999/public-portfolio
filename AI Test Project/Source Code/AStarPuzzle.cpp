//-------------------------------------------------------------------------------------------------
// A-STAR ALGORITHM IMPLEMENTATION
// AUTHOR: Jacopo Vezzosi - Master in Computer Games Development - University of Verona
//-------------------------------------------------------------------------------------------------

#include "AStarPuzzle.h"

bool AStarPuzzle::start(Board& startBoard, std::vector<Board>& out_path)
{
	mVisitedNodes.push_back(Node(startBoard, true));

	const float tentativeCost = getCurrentNode().getBoard().getCost();	
	while(openNodesExist())
	{
		setLowerCostNode();
		if(goalReached())
		{
			setPath(out_path);
			return true;
		}
		getCurrentNode().setState(NodeState::Closed);
		visitAdjNodes();
		for(unsigned int i = 0; i < mVisitedNodes.size(); i++)
		{
			Node * neighborNode = &mVisitedNodes.at(i);
			if(neighborNode->getState() == NodeState::Closed)
				continue;		
			const float neighborCost = neighborNode->getBoard().getCost();
			if(neighborCost > tentativeCost)
				neighborNode->setState(NodeState::Closed);
		}
	}
	return false;
}

AStarPuzzle::Node::Node(Board& content, bool isRoot /*= false*/) : 
	mPreviousIndex(-1), 
	mContent(content), 
	mState(NodeState::Open),
	mIsRoot(isRoot)
{}

AStarPuzzle::Node::~Node()
{}

AStarPuzzle::Node::Node(const Node& other) :
	mContent(other.mContent),
	mState(other.mState),
	mPreviousIndex(other.mPreviousIndex),
	mIsRoot(other.mIsRoot)
{}

AStarPuzzle::Node& AStarPuzzle::Node::operator=(const Node& other) 
{
	if(this == &other)
		return *this;
	mContent = other.mContent;
	mState = other.mState;
	mPreviousIndex = other.mPreviousIndex;
	mIsRoot = other.mIsRoot;
	return *this;
}

std::vector<AStarPuzzle::Node> AStarPuzzle::Node::getAdjNodes() const
{
	std::vector<Node> adjNodes;

	Board swappedDownBoard(mContent);
	if(swappedDownBoard.swapDown())
		adjNodes.push_back(Node(swappedDownBoard));

	Board swappedUpBoard(mContent);
	if(swappedUpBoard.swapUp())
		adjNodes.push_back(Node(swappedUpBoard));

	Board swappedLeftBoard(mContent);
	if(swappedLeftBoard.swapLeft())
		adjNodes.push_back(Node(swappedLeftBoard));

	Board swappedRightBoard(mContent);
	if(swappedRightBoard.swapRight())
		adjNodes.push_back(Node(swappedRightBoard));

	return adjNodes;
}

void AStarPuzzle::Node::print() const
{
	mContent.print();
}

void AStarPuzzle::visitAdjNodes()
{
	std::vector<Node> adjNodes = getCurrentNode().getAdjNodes();
	for(unsigned int j = 0; j < adjNodes.size(); j++)
	{
		bool exists = false;
		for(unsigned int i = 0; i < mVisitedNodes.size(); i++)
		{
			if(adjNodes.at(j).getBoard() == mVisitedNodes.at(i).getBoard()) //check whether the node has been visited already or not...
			{
				exists = true;
			}
		}
		if(!exists)
		{
			adjNodes.at(j).setPreviousIndex(getCurrentNodeIndex());
			mVisitedNodes.push_back(adjNodes.at(j));
		}
	}
}

bool AStarPuzzle::goalReached()
{
	if(getCurrentNode().getBoard().getCost() == 0)
		return true;
	return false;
}

void AStarPuzzle::setPath(std::vector<Board>& out_path)
{
	out_path.clear();
	Node& ptr = getCurrentNode();
	while(!ptr.isRoot())
	{
		out_path.push_back(ptr.getBoard());
		ptr = mVisitedNodes.at(ptr.getPreviousIndex());
	}
}

bool AStarPuzzle::openNodesExist()
{
	for(unsigned int i = 0; i < mVisitedNodes.size(); i++)
	{
		if(mVisitedNodes.at(i).getState() == NodeState::Open)
			return true;
	}
	return false;
}

void AStarPuzzle::setLowerCostNode()
{
	int index = -1;
	for(unsigned int i = 0; i < mVisitedNodes.size(); i++)
	{
		if(mVisitedNodes.at(i).getState() == NodeState::Open)
			if(index == -1 || mVisitedNodes.at(i).getBoard().getCost() <= mVisitedNodes.at(index).getBoard().getCost())
				index = i;
	}
	mCurrentIndex = index;
	if(mCurrentIndex == -1)
		assert("FATAL ERROR: No node has been found while looking for lower cost node!!");
}
