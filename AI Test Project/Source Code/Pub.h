#pragma once

#include "Location.h"

class Pub : public Location
{
public:
	Pub() {}
	Pub(float positionX, float positionY, float width, float height, sf::String text = "") : Location(positionX, positionY, width, height, 20, text) 
	{
		setFillColor(sf::Color::Magenta);
	}

	~Pub(void) {}
};

