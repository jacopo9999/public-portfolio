#include "DogPursuingState.h"

void DogPursuingState::enter(DogNPC& agent) const
{
	agent.startPursuitBehaviourOnClosestSheep();
}

void DogPursuingState::update(DogNPC& agent) const
{
	sf::Vector2f newTargetPosition;
	DogDetectionResult result = agent.targetSheeps(newTargetPosition); 
	if(result == NO_SHEEPS_AROUND)
	{
		agent.getFSM()->changeState(DogWanderingState::getInstance());
	}
	else if(result == GROUPED_SHEEP_AROUND)
	{
		//If the dog identifies a group of sheeps, it goes to a central position in the middle of them thus making them escape
		agent.setTargetPosition(newTargetPosition);
		agent.getFSM()->changeState(DogBarkingState::getInstance());
	}
	else if(result == ISOLATED_SHEEP_AROUND)
	{
		//Mantain the same behaviour but update the target position with the current position of the pursued sheep
		agent.updatePositionOfPursuedSheep();
	}
	else
	{
		assert(false); //Error! State not recognised!
	}
	return; 
}

void DogPursuingState::exit(DogNPC& agent) const
{
	agent.stopPursuingSheep();
}
