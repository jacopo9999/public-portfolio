#pragma once

#include <SFML/Graphics.hpp>

#include <assert.h>
#include <iostream>

#include "mutex.h"

class Location : public sf::RectangleShape
{
protected:

	//Number of NPCs inside
	unsigned int mNPCsInside;

	//Number of NPC slots available
	unsigned int mMaxSlots;

public:
	Location() {}
	Location(const float positionX,
			 const float positionY,
			 const float width,
			 const float height,
			 const unsigned int maxSlots,
			 const sf::String text = "") : 
		sf::RectangleShape(sf::Vector2f(width, height)), 
		mNPCsInside(0),
		mMaxSlots(maxSlots)
	{
		setPosition(sf::Vector2f(positionX, positionY));
	}

	virtual ~Location(void) {}

	//GETTERS:
	//The position of a location is the center of the rectangle, not its upper-left vertex
	sf::Vector2f getPosition() const { return sf::Vector2f(sf::RectangleShape::getPosition().x + sf::RectangleShape::getSize().x/2,
														  sf::RectangleShape::getPosition().y + sf::RectangleShape::getSize().y/2); }
	unsigned int getNPCsInside() const { return mNPCsInside; }
	unsigned int getMaxSlots() const { return mMaxSlots; }

	bool NPCEnter() { 
		if(mNPCsInside >= mMaxSlots) 
			return false; 
		else
		{
			mutex.lock();
			mNPCsInside++; 
			mutex.unlock();
		}
		return true; }
	void NPCExit() { 
		//assert(mNPCsInside > 0); 
		mutex.lock();
		mNPCsInside--; 
		mutex.unlock();
	}
};
