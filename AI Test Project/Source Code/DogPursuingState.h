#pragma once

#include <iostream>
#include <assert.h>

#include "State.h"

#include "DogNPC.h"

#include "DogWanderingState.h"
#include "DogBarkingState.h"

class DogPursuingState : public State<DogNPC>
{
private:

	//PUT HERE THE VARIABLES!

	DogPursuingState(void) : State<DogNPC>() {}
	~DogPursuingState(void) {}

public:
	static DogPursuingState * getInstance()
	{
		static DogPursuingState * mSingleton;
		if(mSingleton == NULL)
		{
			mSingleton = new DogPursuingState();
		}
		return mSingleton;
	}

	void enter(DogNPC& agent) const;
	void update(DogNPC& agent) const;
	void exit(DogNPC& agent) const;
};