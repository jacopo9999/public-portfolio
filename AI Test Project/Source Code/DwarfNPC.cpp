#include "DwarfNPC.h"

void DwarfNPC::startFSM()
{
	mFSMThread = new sf::Thread(&DwarfNPC::exec, this);
	mFSMThread->launch();
}

void DwarfNPC::stopFSM()
{
	mFSMThread->terminate();
}

void DwarfNPC::goToMine()
{
	setTargetPosition(eScene.getMineRef().getPosition());
}

void DwarfNPC::goToHome()
{
	setTargetPosition(eScene.getHomeRef().getPosition());
}

void DwarfNPC::goToPub()
{
	setTargetPosition(eScene.getPubRef().getPosition());
}

void DwarfNPC::exec()
{
	while(true)
	{
		mFSM->update(*this);
		sf::sleep(sf::seconds(UPDATE_FREQUENCY));
	}
}
