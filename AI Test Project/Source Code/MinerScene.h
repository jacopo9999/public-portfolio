#pragma once

#include "IScene.h"

//NPCs:
#include "DwarfNPC.h"
#include "DogNPC.h"
#include "SheepNPC.h"

//Scene:
#include "Scene.h"

//Behaviours:
#include "SeekBehaviour.h"
#include "WanderBehaviour.h"

class MinerScene : public IScene
{
private:
	//Scene
	Scene mScene;

	//Threads
	sf::Thread * mThread;

	//Info label
	sf::Text mInfosLabel;
	sf::Font mFont;

public:

	MinerScene();
	virtual ~MinerScene();
    
	virtual void OnIdle();
	virtual void OnDraw(sf::RenderWindow& renderWindow);

	void RunGame();
};

