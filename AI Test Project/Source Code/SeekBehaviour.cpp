#include "SeekBehaviour.h"

sf::Vector2f SeekBehaviour::getNewSpeed(const NPC * npc) const
{
	int offset = 0;
	if(rand() % 10 == 1) 
	{
		if(mOffset > 0) 
			offset = (rand() % mOffset) - mOffset/2;
	}

	sf::Vector2f targetPositionWithOffset = sf::Vector2f(
		npc->getTargetPosition().x + offset,
		npc->getTargetPosition().y + offset);

	sf::Vector2f distance = targetPositionWithOffset - npc->getPosition();
	sf::Vector2f desiredVelocity = normalize(distance) * getMaxSpeedModule();
	sf::Vector2f steeringForce = (desiredVelocity - npc->getSpeed()) * npc->getMaxSteeringForce();
	sf::Vector2f newSpeed = npc->getSpeed() + steeringForce;
	if(length(newSpeed) > getMaxSpeedModule()) //The speed is over the limit...
		return npc->getSpeed(); //Leave the speed as it is!
	else if(length(newSpeed) > length(distance)) //The speed is over the distance...
		return distance; //The speed becomes equal to the distance!
	else return newSpeed;  //Set the new speed as the current one!
}