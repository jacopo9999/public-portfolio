#pragma once

#include "IScene.h"
#include <algorithm>
#include <sstream>
#include "FontManager.h"
#include "AStarPuzzle.h"

#define CELL_SIZE 100.0f
#define TABLE_OFFSET_X 100.0f
#define TABLE_OFFSET_Y 100.0f
#define TEXT_OFFSET_IN_CELL_X 40.0f
#define TEXT_OFFSET_IN_CELL_Y 25.0f
#define TABLE_BORDER_THICKNESS 1

class FifteenPuzzleScene : public IScene
{
private:
	enum ApplicationState{
		LOADING = 0,
		DISPLAYING_RESULT
	} mState;

	sf::Thread * mThread;

	sf::RectangleShape mCells[16];
	sf::Text mCellsText[16];
	sf::Text mLoadingMsg;
	sf::Font mFont;

	std::vector<Board> mBoardsPath;

public:
	FifteenPuzzleScene();

	void Exec();

    virtual void OnIdle();
    virtual void OnDraw(sf::RenderWindow&);

	virtual ~FifteenPuzzleScene();

	bool isValueInFinalPosition(const unsigned int value, const unsigned int x, const unsigned int y) const;

};