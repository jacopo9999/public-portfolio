#pragma once

#include <iostream>

#include "State.h"

#include "SheepNPC.h"

#include "GroupingState.h"

class EscapingState : public State<SheepNPC>
{
private:

	//PUT HERE THE VARIABLES!

	EscapingState(void) : State<SheepNPC>() {}
	~EscapingState(void) {}

public:
	static EscapingState * getInstance()
	{
		static EscapingState * mSingleton;
		if(mSingleton == NULL)
		{
			mSingleton = new EscapingState();
		}
		return mSingleton;
	}

	void enter(SheepNPC& agent) const;
	void update(SheepNPC& agent) const;
	void exit(SheepNPC& agent) const;
};