#pragma once

#include <SFML/Graphics.hpp>

#include <iostream>

#include "State.h"
#include "GlobalState.h"
#include "Scene.h"
#include "CollisionDetection.h"

#define UPDATE_FREQUENCY 0.02f

class SteeringBehaviour;
class AvoidanceBehaviour;

using namespace Vector2fMath;

class NPC : public sf::CircleShape
{
private:
	sf::FloatRect mBoundingBox; //Defines the shape (bounding box) which contains the NPC

	sf::Vector2f mTargetPosition;
	sf::Vector2f mSpeed;

	const float mMaxSteeringForce;
	const float mCollisionAvoidanceRange;
	const float mSightRange;

	sf::Color mFillColor;

	bool mIsBusy;

protected:
	const SteeringBehaviour ** mCurrentBehaviour; //The current NPC behavior (to be used when multiple behaviors are present)

	const AvoidanceBehaviour * mAvoidanceBehaviour; //The behavior triggered when the NPC must avoid an obstacle
	const SteeringBehaviour * mStandardBehaviour; //The standard NPC behavior

	sf::Thread * mFSMThread;

	//External Pointers
	Scene& eScene;

public:
	NPC(const float radius,
		const float collisionAvoidanceRange, //Should be more than the radius
		const float rangeOfSight, //Should be more than the collision avoidance range
		const float maxSpeedModule,
		const float maxSteeringForce,
		const sf::Color fillColor,
		const SteeringBehaviour * standardBehaviour,
		const sf::FloatRect boundingBox,
		Scene& scene);

	virtual ~NPC(void);

	virtual void startFSM() = 0;
	virtual void stopFSM() = 0;

	void update();

	void stopMovement() { setTargetPosition(getPosition()); }

	bool enterMine() { return eScene.getMineRef().NPCEnter(); }
	void exitMine() { eScene.getMineRef().NPCExit(); }

	bool enterPub() { return eScene.getPubRef().NPCEnter(); }
	void exitPub() { eScene.getPubRef().NPCExit(); }

	bool enterHome() { return eScene.getHomeRef().NPCEnter(); }
	void exitHome() { eScene.getHomeRef().NPCExit(); }

	//Setters:
	void setTargetPosition(sf::Vector2f targetPosition) {mSpeed = sf::Vector2f(0.f, 0.f); mTargetPosition = targetPosition;}
	void setBusy(bool busy) { busy? setFillColor(sf::Color::Black) : setFillColor(mFillColor); mIsBusy = busy; }

	//Getters:
	sf::Vector2f getSpeed() const { return mSpeed; }
	sf::Vector2f getTargetPosition() const { return mTargetPosition; }
	float getMaxSteeringForce() const { return mMaxSteeringForce; }
	float getCollisionRange() const { return mCollisionAvoidanceRange; }
	float getSightRange() const { return mSightRange; }
	sf::FloatRect getBoundingBox() const { return mBoundingBox; }
	bool isBusy() const { return mIsBusy; }
	bool hasSpeedZero() const { return mSpeed == sf::Vector2f(0.f, 0.f); }
	bool hasArrivedAtTarget() const { return getPosition() == mTargetPosition; }

};

