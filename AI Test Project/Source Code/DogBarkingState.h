#pragma once

#include <iostream>
#include <assert.h>

#include "State.h"

#include "DogNPC.h"

#include "DogPursuingState.h"
#include "DogWanderingState.h"

class DogBarkingState : public State<DogNPC>
{
private:

	//PUT HERE THE VARIABLES!

	DogBarkingState(void) : State<DogNPC>() {}
	~DogBarkingState(void) {}

public:
	static DogBarkingState * getInstance()
	{
		static DogBarkingState * mSingleton;
		if(mSingleton == NULL)
		{
			mSingleton = new DogBarkingState();
		}
		return mSingleton;
	}

	void enter(DogNPC& agent) const;
	void update(DogNPC& agent) const;
	void exit(DogNPC& agent) const;
};