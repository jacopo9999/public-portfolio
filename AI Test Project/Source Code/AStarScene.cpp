#include "AStarScene.h"
#include "AStarPuzzle.h"

AStarScene::AStarScene() : 
	mState(LOADING),
	mMapRows(20),
	mMapColumns(20),
	mStartX(3),	
	mStartY(3),
	mGoalX(18),
	mGoalY(19),
	mCellSize(20.0f),
	mTableOffsetX(0.0f),
	mTableOffsetY(0.0f),
	mTableBorderThickness(1.0f)
{
	//GUI Init///////////////////////////////////////////

	//Load the font
	mFont.loadFromFile("consola.ttf");

	//Create the message for the loading screen
	mLoadingMsg = sf::Text("Processing...please wait!", mFont, 26U);
	mLoadingMsg.setPosition(100.0f, 100.0f);
	mLoadingMsg.setColor(sf::Color::Red);
	mLoadingMsg.setCharacterSize(35);

	//Create the screen for displaying the results
	for(unsigned int i = 0; i < mMapRows; i++)
	{
		mCells.push_back(std::vector<sf::RectangleShape>());
		for(unsigned int j = 0; j < mMapColumns; j++)
		{
			float cellX = mCellSize * j + mTableOffsetX;
			float cellY = mCellSize * i + mTableOffsetY;

			sf::RectangleShape shape(sf::Vector2f(mCellSize, mCellSize));
			shape.setPosition(cellX, cellY);
			shape.setOutlineColor(sf::Color::Blue);
			shape.setOutlineThickness(mTableBorderThickness);

			mCells.at(i).push_back(shape);
		}
	}
	////////////////////////////////////////////////////////

	//INIT ALGORITHM INFO///////////////////////////////////
	int notUsableCells[] = {0,6,
							1,6,
							2,6,
							3,6,
							4,6,
							5,6,
							6,6,
							6,7,
							11,7,
							11,6,
							12,6,
							13,6,
							14,6,
							15,6,
							16,6,
							17,6,
							18,6,
							19,6};
	mNotUsableCells = std::vector<int>(notUsableCells, notUsableCells+36);
	////////////////////////////////////////////////////////

	//ALGORITHM THREAD EXECUTION////////////////////////////
	mThread = new sf::Thread(&AStarScene::Exec, this);
	mThread->launch();
	////////////////////////////////////////////////////////
}

AStarScene::~AStarScene()
{
    delete mThread;
}

void AStarScene::OnIdle()
{
}

void AStarScene::Exec()
{

	//Init the map
	AStar::Map map(mMapRows, mMapColumns, mStartX, mStartY, mGoalX, mGoalY);
	
	//Set the state of the non-usable nodes
	for(unsigned int i = 0; i < mNotUsableCells.size(); i+=2)
	{
		map.getNode(mNotUsableCells[i], mNotUsableCells[i+1])->setState(NodeState::NotUsable);
	}

	//Pass the map to the algorithm
	AStar algorithm(&map);

	//Start the search algorithm
	std::vector<MatrixStates> stepsMatrices = algorithm.startSearch();

	//Search finished, now display the result
	mState = DISPLAYING_RESULT;

	//Update the current step matrix at regular intervals
	for(unsigned int i = 0; i < stepsMatrices.size(); i++)
	{
		//Print the algorithm steps
		PrintMatrixStates(stepsMatrices.at(i), mCells); //Change the displayed matrix of states
		sf::sleep(sf::seconds(0.02f));
	}
}

void AStarScene::OnDraw(sf::RenderWindow& renderWindow)
{
	renderWindow.clear();
	if(mState == ApplicationState::LOADING)
		renderWindow.draw(mLoadingMsg);
	else if(mState == ApplicationState::DISPLAYING_RESULT)
	{
		for(unsigned int i = 0; i < mMapRows; i++)
		{
			for(unsigned int j = 0; j < mMapColumns; j++)
			{
				renderWindow.draw(mCells[i][j]);
			}
		}
	}
	else 
	{
		std::cout << "ERROR: invalid application state!" << std::endl;
		exit(1);
	}
}

void AStarScene::PrintMatrixStates(const MatrixStates& inMatrixStates, RectangleShapesMatrix& oMatrix) const
{
	const size_t rows = inMatrixStates.size();
	const size_t cols = inMatrixStates.at(0).size();
	RectangleShapesMatrix outMatrix;
	for(unsigned int i = 0; i < rows; i++)
	{
		for(unsigned int j = 0; j < cols; j++)
		{
			switch(inMatrixStates[i][j])
			{
				case NodeState::Open : {
					oMatrix[i][j].setFillColor(sf::Color::Green);
				} break;
				case NodeState::Closed : {
					oMatrix[i][j].setFillColor(sf::Color::Red);
				} break;
				case NodeState::NotUsable : {
					oMatrix[i][j].setFillColor(sf::Color::Black);
				} break;
				case NodeState::Goal : {
					oMatrix[i][j].setFillColor(sf::Color::Cyan);
				} break;
				case NodeState::Path : {
					oMatrix[i][j].setFillColor(sf::Color::Yellow);
				} break;
				case NodeState::Start : {
					oMatrix[i][j].setFillColor(sf::Color::Magenta);
				} break;
				default : {
					oMatrix[i][j].setFillColor(sf::Color(100, 100, 100));
				}
			}
		}
	}
}
