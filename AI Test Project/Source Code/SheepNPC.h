#pragma once

#include <sstream>

#include "IScene.h"

#include "FSM.h"
#include "State.h"
#include "NPC.h"
#include "DogNPC.h"

class SheepNPC : public NPC
{
private:
	void exec();

	FSM<SheepNPC> * mFSM;

	const SteeringBehaviour *  mEscapeBehaviour;

public:
	SheepNPC(const SteeringBehaviour * standardBehaviour,
		     const SteeringBehaviour * escapeBehaviour,
			 Scene& scene,
			 const sf::FloatRect boundingBox,
			 const State<SheepNPC> * startState,
			 const GlobalState<SheepNPC> * globalState = nullptr) : 
		NPC(10.0f,
			15.0f,
			80.0f,
			0.5f,
			1.0f,
			sf::Color(160,160,160,255),
			standardBehaviour,
			boundingBox,
			scene),
		mEscapeBehaviour(escapeBehaviour)
	{
		mFSM = new FSM<SheepNPC>(*this, startState, globalState);
	}
	~SheepNPC(void) {}

	void startFSM(); //Start the Finite States Machine
	void stopFSM(); //Stop the Finite States Machine

	bool isBarkingDogInSightRange(); //Check whether there is a barking dog on sight or not
	void startGroupingBehaviour(); //Change the target position to the position of the nearest sheep, does nothing if there are no sheeps in the sight range
	void startEscapeBehaviour(); //Change the target position to the position of the nearest dog, does nothing if there are no dogs in the sight range

	//GETTERS:
	FSM<SheepNPC> * getFSM() const { return mFSM; }
};
