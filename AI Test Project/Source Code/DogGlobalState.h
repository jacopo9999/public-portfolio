#pragma once

#include <iostream>

#include "GlobalState.h"

#include "DogNPC.h"

class DogGlobalState : virtual public GlobalState<DogNPC>
{
private:

	DogGlobalState(void) : GlobalState<DogNPC>() {}
	~DogGlobalState(void) {}

public:
	static DogGlobalState * getInstance()
	{
		static DogGlobalState * mSingleton;
		if(mSingleton == NULL)
		{
			mSingleton = new DogGlobalState();
		}
		return mSingleton;
	}

	void enter(DogNPC& agent) const;
	void update(DogNPC& agent) const;
	void exit(DogNPC& agent) const;
};
