#include "FleeBehaviour.h"

sf::Vector2f FleeBehaviour::getNewSpeed(const NPC * npc) const
{
	sf::Vector2f distance = npc->getTargetPosition() - npc->getPosition();
	sf::Vector2f desiredVelocity = - normalize(distance) * getMaxSpeedModule();
	return desiredVelocity;
}