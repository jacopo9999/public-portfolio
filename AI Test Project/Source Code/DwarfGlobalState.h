#pragma once

#include <iostream>

#include "GlobalState.h"

#include "DwarfNPC.h"

class DwarfGlobalState : virtual public GlobalState<DwarfNPC>
{
private:

	DwarfGlobalState(void) : GlobalState<DwarfNPC>() {}
	~DwarfGlobalState(void) {}

public:
	static DwarfGlobalState * getInstance()
	{
		static DwarfGlobalState * mSingleton;
		if(mSingleton == NULL)
		{
			mSingleton = new DwarfGlobalState();
		}
		return mSingleton;
	}

	void enter(DwarfNPC& agent) const;
	void update(DwarfNPC& agent) const;
	void exit(DwarfNPC& agent) const;
};
