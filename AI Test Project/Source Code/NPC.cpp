#include "NPC.h"

#include "SteeringBehaviour.h"
#include "AvoidanceBehaviour.h"

NPC::NPC(const float radius,
		 const float collisionAvoidanceRange, //Should be more than the radius
		 const float rangeOfSight, //Should be more than the collision avoidance range
		 const float maxSpeedModule,
		 const float maxSteeringForce,
		 const sf::Color fillColor,
		 const SteeringBehaviour * standardBehaviour,
		 const sf::FloatRect boundingBox,
		 Scene& scene) :
	sf::CircleShape(radius),
	mIsBusy(false),
	mMaxSteeringForce(maxSteeringForce), //Should be > 0 and < 1
	mFillColor(fillColor),
	mTargetPosition(getPosition()),
	mStandardBehaviour(standardBehaviour),
	mCurrentBehaviour(&mStandardBehaviour),
	mBoundingBox(boundingBox),
	mSpeed(0.0f, 0.0f),
	mCollisionAvoidanceRange(collisionAvoidanceRange),
	mSightRange(rangeOfSight),
	eScene(scene),
	mFSMThread(nullptr)
{
	setFillColor(mFillColor);
	mAvoidanceBehaviour = new AvoidanceBehaviour;
}

NPC::~NPC(void)
{
	delete mAvoidanceBehaviour;
	delete mFSMThread;
}

void NPC::update()
{
	const NPC * collidingNPC = eScene.getClosestNPC(eScene.getNPCsInRangeTowardsDirection(this, getCollisionRange()), this);
	if(collidingNPC != nullptr)
	{	
		mutex.lock();
		mSpeed = mAvoidanceBehaviour->getNewSpeed(this, collidingNPC); //There is a collision, add the avoidance steering only
		mutex.unlock();
	}
	else 
	{	
		mutex.lock();
		mSpeed = (*mCurrentBehaviour)->getNewSpeed(this); //Obtain the new speed depending on the behavior (the different steering factors are added one to each other)
		mutex.unlock();
	}

	//Change the desired speed according to collisions with the bounding box
	CollisionType collisionType = CollisionDetection::boundingBoxContainsShape(*this, mBoundingBox);
	if(collisionType != CollisionType::FULLY_INSIDE)
	{
		mutex.lock();
		if(collisionType == CollisionType::HORIZONTAL_LEFT)
			mSpeed.x = abs(mSpeed.x);
		else if(collisionType == CollisionType::HORIZONTAL_RIGHT)
			mSpeed.x = - abs(mSpeed.x); 
		else if(collisionType == CollisionType::VERTICAL_TOP)
			mSpeed.y = abs(mSpeed.y);
		else if(collisionType == CollisionType::VERTICAL_BOTTOM)
			mSpeed.y = - abs(mSpeed.y);
		else ; //Does nothing (if the NPC is outside the bounding box, no action on it is done.
		mutex.unlock();
	}

	//Set the new position of the NPC depending on its new speed
	setPosition(getPosition() + mSpeed);
}

