#pragma once

#include <sstream>

#include "IScene.h"

#include "FSM.h"
#include "State.h"
#include "NPC.h"

class DwarfNPC : public NPC
{
private:
	void exec();

	FSM<DwarfNPC> * mFSM;

	int mGold;
	float mThirst;

public:
	DwarfNPC(const SteeringBehaviour * standardBehaviour,
			 Scene& scene,
			 const sf::FloatRect boundingBox,
			 const State<DwarfNPC> * startState,
			 const GlobalState<DwarfNPC> * globalState = nullptr) : 
		NPC(10.0f,
			20.0f,
			20.0f,
			4.0f,
			0.3f,
			sf::Color::Yellow,
			standardBehaviour,
			boundingBox,
			scene),
		mGold(0), 
		mThirst(0.f)
	{
		mFSM = new FSM<DwarfNPC>(*this, startState, globalState);
	}
	~DwarfNPC(void) {}

	void startFSM();
	void stopFSM();

	void goToMine();
	void goToHome();
	void goToPub();

	//Setters:
	void increaseGold(int goldModifier) {mGold += goldModifier;}
	void increaseThirst(float thirstModifier) {mThirst += thirstModifier;}
	void emptyGold() {mGold = 0;}
	void startStandardBehaviour() {mCurrentBehaviour = &mStandardBehaviour;}

	//Getters:
	int getGold() const {return mGold;}
	float getThirst() const {return mThirst;}
	FSM<DwarfNPC> * getFSM() const {return mFSM;}
};

