
#include "HomeState.h"

void HomeState::enter(DwarfNPC& agent) const
{
	agent.goToHome();
}

void HomeState::update(DwarfNPC& agent) const
{
	while(!agent.hasArrivedAtTarget())
		return; //Do not do anything if the agent has not arrived

	while(!agent.isBusy() && !agent.enterHome())
		return; //Do not do anything if the location is full

	agent.setBusy(true);
	sf::sleep(sf::seconds(3.0f));
	agent.emptyGold();

	agent.getFSM()->changeState(MineState::getInstance());	
	return;
}
 
void HomeState::exit(DwarfNPC& agent) const
{
	if(agent.isBusy())
	{
		agent.exitHome();
	}
	agent.setBusy(false);
}