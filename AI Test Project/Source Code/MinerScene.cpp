#include "MinerScene.h"

#include "NPC.h"
#include "HomeState.h"
#include "GroupingState.h"
#include "EscapingState.h"
#include "DogWanderingState.h"
#include "DwarfGlobalState.h"
#include "SheepGlobalState.h"
#include "DogGlobalState.h"
#include "FSM.h"

#include "SeekBehaviour.h"
#include "FleeBehaviour.h"

#define NUMBER_DWARVES 7
#define NUMBER_SHEEPS 10

MinerScene::MinerScene()
{
	//Load the font
	mFont.loadFromFile("consola.ttf");

	//Create the message for the loading screen
	mInfosLabel = sf::Text("<INFO TEXT>", mFont, 21U);
	mInfosLabel.setPosition(10.0f, 10.0f);
	mInfosLabel.setColor(sf::Color::Black);

	//Init the scene objects
	mScene = Scene(1000.0f, 1000.0f);
	mScene.setPub(Pub(150.0f, 300.0f, 100.0f, 75.0f, "PUB"));
	mScene.setMine(Mine(500.0f, 100.0f, 150.0f, 250.0f, "MINE"));
	mScene.setHome(Home(100.0f, 50.0f, 100.0f, 75.0f, "HOME"));
	mScene.setSheepsFence(SheepsFence(300.0f, 375.0f, 400.0f, 200.0f, "FENCE"));

	//Init the NPCs:
	for(unsigned int i = 0; i < NUMBER_DWARVES; i++)
	{
		DwarfNPC * dwarf = new DwarfNPC(new SeekBehaviour(2.0f, 500),
										mScene,
										mScene.getGlobalBounds(),
										MineState::getInstance(),
										DwarfGlobalState::getInstance());
		dwarf->increaseThirst(i*2.0f);
		mScene.addDwarf(dwarf);
	}
	mScene.getDwarvesRefs().at(0)->setPosition(2.0f,		2.0f);
	mScene.getDwarvesRefs().at(1)->setPosition(2.0f,		70.0f);
	mScene.getDwarvesRefs().at(2)->setPosition(250.0f,	100.0f);
	mScene.getDwarvesRefs().at(3)->setPosition(350.0f,	200.0f);
	mScene.getDwarvesRefs().at(4)->setPosition(350.0f,	20.0f);
	mScene.getDwarvesRefs().at(5)->setPosition(100.0f,	100.0f);
	mScene.getDwarvesRefs().at(6)->setPosition(80.0f,	200.0f);
	for(unsigned int i = 0; i < NUMBER_SHEEPS; i++)
	{
		SheepNPC * sheep = new SheepNPC(new WanderBehaviour(0.5f),
										new FleeBehaviour(1.5f),
										mScene,
										mScene.getSheepsFenceRef().getGlobalBounds(),
										GroupingState::getInstance(),
										SheepGlobalState::getInstance());
		mScene.addSheep(sheep);
	}
	mScene.getSheepsRefs().at(0)->setPosition(350.0f,	400.0f);
	mScene.getSheepsRefs().at(1)->setPosition(380.0f,	400.0f);
	mScene.getSheepsRefs().at(2)->setPosition(380.0f,	440.0f);
	mScene.getSheepsRefs().at(3)->setPosition(320.0f,	380.0f);
	mScene.getSheepsRefs().at(4)->setPosition(320.0f,	440.0f);
	mScene.getSheepsRefs().at(5)->setPosition(310.0f,	510.0f);
	mScene.getSheepsRefs().at(6)->setPosition(440.0f,	520.0f);
	mScene.getSheepsRefs().at(7)->setPosition(510.0f,	550.0f);
	mScene.getSheepsRefs().at(8)->setPosition(540.0f,	550.0f);
	mScene.getSheepsRefs().at(9)->setPosition(500.0f,	380.0f);
	DogNPC * dog = new DogNPC(new WanderBehaviour(0.5f),
							  new SeekBehaviour(3.0f),		
							  mScene,
							  mScene.getSheepsFenceRef().getGlobalBounds(),
							  DogWanderingState::getInstance(),
							  DogGlobalState::getInstance());
	mScene.addDog(dog);
	dog->setPosition(500.0f, 450.0f);

	//Create and launch the worker thread
	mThread = new sf::Thread(&MinerScene::RunGame, this);
	mThread->launch();
}

MinerScene::~MinerScene()
{
    delete mThread;
}

void MinerScene::OnIdle()
{
}

void MinerScene::OnDraw(sf::RenderWindow& renderWindow)
{
    renderWindow.clear();

	//Draw the scene
	mScene.drawIn(&renderWindow);

	//Draw the text infos
	std::stringstream sstm;
	sstm << "HOME OCCUPANCY:[" << mScene.getHomeRef().getNPCsInside() << "/" << mScene.getHomeRef().getMaxSlots() << "] " <<
			"PUB OCCUPANCY:["  << mScene.getPubRef().getNPCsInside() << "/" << mScene.getPubRef().getMaxSlots() << "] " <<
			"MINE OCCUPANCY:[" << mScene.getMineRef().getNPCsInside() << "/" << mScene.getMineRef().getMaxSlots() << "] " ;
	mInfosLabel.setString(sf::String(sstm.str()));
	renderWindow.draw(mInfosLabel);

	//Draw the NPCs
	for(unsigned int i = 0; i < NUMBER_DWARVES; i++)
	{
		renderWindow.draw(*mScene.getDwarvesRefs().at(i));
	}
	for(unsigned int i = 0; i < NUMBER_SHEEPS; i++)
	{
		renderWindow.draw(*mScene.getSheepsRefs().at(i));
	}
	renderWindow.draw(*mScene.getDogsRefs().at(0));
}

void MinerScene::RunGame()
{
	for(unsigned int i = 0; i < mScene.getDwarvesRefs().size(); i++)
	{
		mScene.getDwarvesRefs().at(i)->startFSM();
	}
	for(unsigned int i = 0; i < mScene.getSheepsRefs().size(); i++)
	{
		mScene.getSheepsRefs().at(i)->startFSM();
	}
	for(unsigned int i = 0; i < mScene.getDogsRefs().size(); i++)
	{
		mScene.getDogsRefs().at(i)->startFSM();
	}
}



