#include "DwarfGlobalState.h"

void DwarfGlobalState::enter(DwarfNPC& agent) const
{
}

void DwarfGlobalState::update(DwarfNPC& agent) const
{
	agent.increaseThirst(0.1f);
	agent.update();
}

void DwarfGlobalState::exit(DwarfNPC& agent) const
{
}