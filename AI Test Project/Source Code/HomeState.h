#pragma once

#include <iostream>

#include "State.h"

#include "DwarfNPC.h"
#include "MineState.h"

class HomeState : public State<DwarfNPC>
{
private:

	HomeState(void) : State<DwarfNPC>() {}
	~HomeState(void) {}

public:
	static HomeState * getInstance()
	{
		static HomeState * mSingleton;
		if(mSingleton == NULL)
		{
			mSingleton = new HomeState();
		}
		return mSingleton;
	}

	void enter(DwarfNPC& agent) const;
	void update(DwarfNPC& agent) const;
	void exit(DwarfNPC& agent) const;
};

