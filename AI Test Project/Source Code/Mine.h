#pragma once

#include "Location.h"

class Mine : public Location
{
public:
	Mine() {}
	Mine(float positionX, float positionY, float width, float height, sf::String text = "") : Location(positionX, positionY, width, height, 2, text) 
	{
		setFillColor(sf::Color(50,50,50));
	}

	~Mine(void) {}
};

