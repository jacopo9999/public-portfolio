#include <iostream>
#include "Application.h"

#include <tchar.h>

int _tmain(int argc, _TCHAR* argv[])
{
	Application* game = Application::Istance();
	
	game->Init();
	
	game->Run();
	
	game->Destroy();
	
	exit(0);
}




