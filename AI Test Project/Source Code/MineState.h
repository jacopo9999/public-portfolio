#pragma once

#include "State.h"

#include <iostream>

#include "DwarfNPC.h"

#include "HomeState.h"
#include "PubState.h"

class MineState : public State<DwarfNPC>
{
private:

	MineState(void) : State<DwarfNPC>() {}
	~MineState(void) {}

public:
	static MineState * getInstance()
	{
		static MineState * mSingleton;
		if(mSingleton == NULL)
		{
			mSingleton = new MineState();
		}
		return mSingleton;
	}

	void enter(DwarfNPC& agent) const;
	void update(DwarfNPC& agent) const;
	void exit(DwarfNPC& agent) const;
};

