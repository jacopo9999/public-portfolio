//-------------------------------------------------------------------------------------------------
// A-STAR ALGORITHM IMPLEMENTATION
// AUTHOR: Jacopo Vezzosi - Master in Computer Games Development - University of Verona
//-------------------------------------------------------------------------------------------------

#pragma once

#include "Board.h"
#include "NodeState.h"
#include "AStar.h"

#include <vector>

class AStarPuzzle
{
private:

	class Node
	{
	public:
		Node(Board& content, bool isRoot = false);
		~Node();

		std::vector<Node> getAdjNodes() const;

		void print() const;

		Board& getBoard() {return mContent;}

		NodeState getState() const {return mState;}
		void setState(NodeState state) {mState = state;}

		const int getPreviousIndex() const {return mPreviousIndex;}
		void setPreviousIndex(const unsigned int previousIndex) {mPreviousIndex = previousIndex;}

		bool isRoot() const {return mIsRoot;}

		Node(const Node& other);
		Node& operator=(const Node& other);

	private:
		Board mContent;
		NodeState mState;
		int mPreviousIndex;

		bool mIsRoot;
	};

	std::vector<Node> mVisitedNodes;
	unsigned int mCurrentIndex;

	void visitAdjNodes();
	bool goalReached();
	void setPath(std::vector<Board>& out_path);
	bool openNodesExist();
	void setLowerCostNode();

	Node& getCurrentNode() {return mVisitedNodes.at(mCurrentIndex);}
	const int getCurrentNodeIndex() {return mCurrentIndex;}

public:
	AStarPuzzle(void) : mCurrentIndex(0) {}
	~AStarPuzzle(void) {}

	bool start(Board& startBoard, std::vector<Board>& out_path);
};


