#pragma once

#include <SFML/Graphics.hpp>

#include "Vector2fMath.h"

enum CollisionType
{
	FULLY_INSIDE,
	FULLY_OUTSIDE,
	HORIZONTAL_LEFT,
	HORIZONTAL_RIGHT,
	VERTICAL_TOP,
	VERTICAL_BOTTOM
};

class CollisionDetection
{
public:
	static bool betweenTwoShapes(const sf::Shape& shape1, const sf::Shape& shape2);
	static CollisionType boundingBoxContainsShape(const sf::Shape& shape, const sf::FloatRect& boundingBox);
	static bool CollisionDetection::shapeInsideRange(const sf::Shape& shape, const sf::Vector2f rangeCenter, const float range);
	static bool betweenAheadVectorAndCircle(const sf::Shape& shape,
											const sf::Vector2f shapeSpeed,
											const float rangeOfSight,
											const sf::CircleShape& circleShape);
};

