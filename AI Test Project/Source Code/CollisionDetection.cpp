#include "CollisionDetection.h"

bool CollisionDetection::betweenTwoShapes(const sf::Shape& shape1, const sf::Shape& shape2)
{
	sf::FloatRect boundsShape1 = shape1.getGlobalBounds();
	sf::FloatRect boundsShape2 = shape2.getGlobalBounds();
	return boundsShape1.intersects(boundsShape2);
}

CollisionType CollisionDetection::boundingBoxContainsShape(const sf::Shape& shape, const sf::FloatRect& boundingBox)
{
	sf::FloatRect bounds = shape.getGlobalBounds();
	
	if(!bounds.intersects(boundingBox)) 
		return CollisionType::FULLY_OUTSIDE; //No collision but the shape is fully outside the bounding box
	else if((bounds.left + bounds.width) > (boundingBox.left + boundingBox.width)) 
		return CollisionType::HORIZONTAL_RIGHT;
	else if(bounds.left < boundingBox.left) 
		return CollisionType::HORIZONTAL_LEFT;
	else if(bounds.top < boundingBox.top) 
		return CollisionType::VERTICAL_TOP;
	else if((bounds.top + bounds.height) > (boundingBox.top + boundingBox.height)) 
		return CollisionType::VERTICAL_BOTTOM;
	else
		return CollisionType::FULLY_INSIDE;
}

bool CollisionDetection::shapeInsideRange(const sf::Shape& shape, sf::Vector2f rangeCenter, const float range)
{
	if(Vector2fMath::length(shape.getPosition() - rangeCenter) <= range)
		return true;
	else return false;
}

bool CollisionDetection::betweenAheadVectorAndCircle(const sf::Shape& shape,
													 const sf::Vector2f shapeSpeed,
													 const float rangeOfSight,
													 const sf::CircleShape& circleShape)
{
	sf::Vector2f ahead = shape.getPosition() + Vector2fMath::normalize(shapeSpeed) * rangeOfSight;
	sf::Vector2f ahead2 = ahead * 0.5f;
	if(Vector2fMath::distance(ahead, circleShape.getPosition()) <= circleShape.getRadius() ||
	   Vector2fMath::distance(ahead2, circleShape.getPosition()) <= circleShape.getRadius() ||
	   Vector2fMath::distance(shape.getPosition(), circleShape.getPosition()) <= circleShape.getRadius())
		return true;
	else return false;
}