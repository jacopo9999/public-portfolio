#ifndef AStar_NodeState_h
#define AStar_NodeState_h

enum NodeState
{
    Open,
    Closed,
	Undefined,
	NotUsable,
	Goal,
	Path,
	Start
};
	
#endif
