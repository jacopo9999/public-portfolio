#pragma once

#include <iostream>

#include "State.h"

#include "SheepNPC.h"

#include "EscapingState.h"
#include "GroupingState.h"

class PursuedState : public State<SheepNPC>
{
private:

	//PUT HERE THE VARIABLES!

	PursuedState(void) : State<SheepNPC>() {}
	~PursuedState(void) {}

public:
	static PursuedState * getInstance()
	{
		static PursuedState * mSingleton;
		if(mSingleton == NULL)
		{
			mSingleton = new PursuedState();
		}
		return mSingleton;
	}

	void enter(SheepNPC& agent) const;
	void update(SheepNPC& agent) const;
	void exit(SheepNPC& agent) const;
};
