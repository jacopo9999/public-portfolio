#include "SheepNPC.h"

void SheepNPC::startFSM()
{
	mFSMThread = new sf::Thread(&SheepNPC::exec, this);
	mFSMThread->launch();
}

void SheepNPC::stopFSM()
{
	mFSMThread->terminate();
}

void SheepNPC::exec()
{
	while(true)
	{
		mFSM->update(*this);
		sf::sleep(sf::seconds(UPDATE_FREQUENCY));
	}
}

bool SheepNPC::isBarkingDogInSightRange()
{
	const DogNPC * closestDog = reinterpret_cast<const DogNPC*>(eScene.getClosestNPC(eScene.getDogsInRange(this, getSightRange()), this));
	if(closestDog != nullptr && closestDog->isBarking())
	{
		return true;
	}
	return false;
}

void SheepNPC::startEscapeBehaviour()
{
	mCurrentBehaviour = &mEscapeBehaviour;
	const NPC * closestDog = eScene.getClosestNPC(eScene.getDogsInRange(this, getSightRange()), this);
	if(closestDog != nullptr)
	{	
		setTargetPosition(closestDog->getPosition());
	}
}

void SheepNPC::startGroupingBehaviour()
{
	mCurrentBehaviour = &mStandardBehaviour;
	const NPC * closestSheep = eScene.getClosestNPC(eScene.getSheepsInRange(this, getSightRange()), this);
	if(closestSheep != nullptr)
	{	
		sf::Vector2f position = closestSheep->getPosition(); //for debug
		setTargetPosition(closestSheep->getPosition());
	}
	else stopMovement();
}