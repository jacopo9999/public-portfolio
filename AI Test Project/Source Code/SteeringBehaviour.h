#pragma once

#include <SFML/Graphics.hpp>

#include "Vector2fMath.h"
#include "NPC.h"

class SteeringBehaviour
{
private:
	const float mMaxSpeedModule;

protected:
	float getMaxSpeedModule() const { return mMaxSpeedModule; }

public:
	SteeringBehaviour(const float maxSpeedModule) : mMaxSpeedModule(maxSpeedModule) {}
	virtual ~SteeringBehaviour() {}

	virtual sf::Vector2f getNewSpeed(const NPC * npc) const = 0;  //Change the speed according to the behaviour
};

