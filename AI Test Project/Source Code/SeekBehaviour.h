#pragma once

#include "SteeringBehaviour.h"

using namespace Vector2fMath;

/* With the seek behaviour the NPC will move towards its target position using the most direct path, if an offset is specified
the NPC will wander to reach its final target (by applying an offset to the target position)*/
class SeekBehaviour : public SteeringBehaviour
{
private:
	const unsigned int mOffset;

public:
	SeekBehaviour(const float maxSpeedModule, const unsigned int offset = 0) : 
		SteeringBehaviour(maxSpeedModule),
		mOffset(offset) {}
	~SeekBehaviour() {}

	sf::Vector2f getNewSpeed(const NPC * npc) const;
};

