#pragma once

#include "SteeringBehaviour.h"

/* The wander behaviour is based on variations to the NPC's target position, a new target position is obtained
from the original one (without changing it) and the NPC will move toward the temporary position until a new one is created*/
class WanderBehaviour : public SteeringBehaviour
{
public:
	WanderBehaviour(const float maxSpeedModule) : SteeringBehaviour(maxSpeedModule) {}
	~WanderBehaviour() {}

	sf::Vector2f getNewSpeed(const NPC * npc) const;
};

