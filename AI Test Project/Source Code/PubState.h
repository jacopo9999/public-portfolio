#pragma once

#include <iostream>

#include "State.h"
#include "MineState.h"
#include "DwarfNPC.h"

class PubState : public State<DwarfNPC>
{
private:

	PubState(void) : State<DwarfNPC>() {}
	~PubState(void) {}

public:
	static PubState * getInstance()
	{
		static PubState * mSingleton;
		if(mSingleton == NULL)
		{
			mSingleton = new PubState();
		}
		return mSingleton;
	}

	void enter(DwarfNPC& agent) const;
	void update(DwarfNPC& agent) const;
	void exit(DwarfNPC& agent) const;
};

