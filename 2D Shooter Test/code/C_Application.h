#ifndef TEST_C_APPLICATION_H
#define TEST_C_APPLICATION_H

#include <Game\Game.h>

class C_Application
{
public:
	
	typedef unsigned int T_PressedKey;

	C_Application(int screenWidth, int screenHeight);
	~C_Application();

	/// Tick is called on fix framerate (50fps)
	void Tick(T_PressedKey pressedKeys);

	static const T_PressedKey s_KeyLeft  = 0x01;
	static const T_PressedKey s_KeyUp    = 0x02;
	static const T_PressedKey s_KeyRight = 0x04;
	static const T_PressedKey s_KeyDown  = 0x08;
	static const T_PressedKey s_KeySpace = 0x10;

private:

	void		ClearScreen();

	const int	m_ScreenWidth;
	const int	m_ScreenHeight;
	
	Game		m_game;
};

#endif // #ifndef TEST_C_APPLICATION_H
