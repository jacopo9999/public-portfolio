#pragma once

#include <memory>
#include <list>

#include <GameObjects\GameObject.h>
#include <GameObjects\Projectile.h>
#include <GameObjects\Cannon.h>
#include <GameObjects\Clock.h>

/*Contains all the logic and data of the game*/
class Game {
public:
	Game(unsigned int sceneWidth, unsigned int sceneHeight);

	void playerRotateClockwise(float rotationRadians);
	void playerRotateCounterClockwise(float rotationRadians);

	void playerSetPosition(const Vec2& position);

	void drawGameObjects();
	
	void update();

	void playerFire();

	void createClocks();

private:
	std::list<std::shared_ptr<GameObject>> m_gameObjects;
	std::weak_ptr<GameObject> m_player; //Reference to Cannon object for quick access

	const unsigned int m_sceneHeight;
	const unsigned int m_sceneWidth;

};
