#include "Game.h"

#include <assert.h>

#include <Framework\time.h>
#include <Math\Math.h>

Game::Game(unsigned int sceneWidth, unsigned int sceneHeight) : 
	m_sceneWidth(sceneWidth),
	m_sceneHeight(sceneHeight)
{
	m_gameObjects.push_back(std::make_shared<Cannon>(Vec2(0.f, 0.f), 0.f));
	m_player = m_gameObjects.back();

	createClocks();
}

void Game::drawGameObjects()
{
	for (auto gameObject : m_gameObjects)
	{
		assert(gameObject); // Trying to draw a null gameObject ptr
		if (gameObject)
		{
			gameObject.get()->draw();
		}	
	}
}

void Game::update()
{
	for (auto go : m_gameObjects)
	{
		GameObject* gameObject = go.get();
		assert(gameObject != nullptr); //Trying to update a null gameObject ptr

		if (gameObject == nullptr || gameObject->isMarkedForDeletion())
		{
			continue;
		}

		gameObject->updateMovement(); //Update physics
		gameObject->updateLogic(); //Update game logic
		
		//Delete projectiles that end up outside the scene (for simplicity they get deleted as soon as they collide, instead of disappearing over the edge)
		if (gameObject->getType() == GameObjectsTypes::PROJECTILE)
		{
			Projectile* projectile = static_cast<Projectile*>(gameObject);
			for (auto go2 = m_gameObjects.begin(); go2 != m_gameObjects.end(); ++go2)
			{
				GameObject* otherGameObject = (*go2).get();
				if (otherGameObject->getType() == GameObjectsTypes::CLOCK && !otherGameObject->isMarkedForDeletion())
				{
					//Break or destroy the clock
					Clock* clock = static_cast<Clock*>(otherGameObject);
					if (projectile->checkcollisionWithObject(*clock)) //Projectile hits Clock
					{
						if (clock->getSideLength() == 100.f) //If ithe clock is big, break it into two smaller ones
						{
							m_gameObjects.push_back(std::make_shared<Clock>(clock->getPosition(), 5.f, 0.f));
							m_gameObjects.back().get()->setVelocity(Vec2(0.5f, 0.5f));

							m_gameObjects.push_back(std::make_shared<Clock>(clock->getPosition(), 5.f, 0.f));
							m_gameObjects.back().get()->setVelocity(Vec2(-0.5f, -0.5f));
						}

						//Destroy the projectile
						projectile->setMarkedForDeletion();

						//Destroy the clock
						clock->setMarkedForDeletion();

						break;
					}
				}
			}

			if (!projectile->isMarkedForDeletion())
			{
				if (projectile->checkcollisionWithBounds(0.f, float(m_sceneWidth), 0.f, float(m_sceneHeight)))
				{
					//Destroy the projectile
					projectile->setMarkedForDeletion();
				}
			}
		}
		else if (gameObject->getType() == GameObjectsTypes::CLOCK) //Bounce clock off the bounds
		{
			Clock* clock = static_cast<Clock*>(gameObject);
			Vec2 collisionNormal;
			if (clock->checkcollisionWithBounds(0.f, float(m_sceneWidth), 0.f, float(m_sceneHeight), collisionNormal))
			{		
				clock->reflectVelocityOnCollision(collisionNormal);
				break;
			}

			for (auto go2 : m_gameObjects)
			{
				const GameObject* otherGameObject = go2.get();
				if (go != go2 && otherGameObject->getType() == GameObjectsTypes::CLOCK && !otherGameObject->isMarkedForDeletion()) //Bounce off another clock
				{
					const Clock* otherClock = static_cast<const Clock*>(otherGameObject);
					Vec2 collisionNormal;
					if (clock->checkcollisionWithObject(*otherClock, collisionNormal))
					{
						clock->reflectVelocityOnCollision(collisionNormal);
						break;
					}
				}
			}
		}
	}

	//Destroy object marked for deletion and count the number of clocks
	unsigned int numClocks = 0;
	auto go = m_gameObjects.begin();
	while (go != m_gameObjects.end())
	{
		GameObject* gameObject = (*go).get();
		if (gameObject->isMarkedForDeletion())
		{
			go = m_gameObjects.erase(go);
			continue;
		}
		else
		{
			if (gameObject->getType() == GameObjectsTypes::CLOCK)
			{
				numClocks++;
			}

			++go;
		}
	}

	//Check if there are no clocks left, then recreate two of them
	if (numClocks == 0)
	{
		createClocks();
	}

}

void Game::playerFire()
{
	auto player = m_player.lock();
	if (player)
	{
		const float playerRotationAngle = player.get()->getRotationAngle() - k_PI / 2;
		std::shared_ptr<Projectile> projectile = std::make_shared<Projectile>(player.get()->getPosition(), 20.0f, playerRotationAngle);
		Vec2 rotationVectorUnary;
		Math::getRotationVectorUnary(playerRotationAngle, rotationVectorUnary);
		const Vec2 velocity = Vec2(rotationVectorUnary.x * 2.0f, rotationVectorUnary.y * 2.f);
		projectile.get()->setVelocity(velocity);
		m_gameObjects.push_back(projectile);
	}
	else
	{
		return; //No player!
	}
}

void Game::playerRotateClockwise(float rotationRadians)
{
	auto player = m_player.lock();
	if (player)
	{
		if (player.get()->getRotationAngle() < k_PI/2)
		{
			player.get()->rotate(rotationRadians);
		}	
	}
}

void Game::playerRotateCounterClockwise(float rotationRadians)
{
	auto player = m_player.lock();
	if (player)
	{
		if (player.get()->getRotationAngle() > -k_PI/2)
		{
			player.get()->rotate(-rotationRadians);
		}
	}
}

void Game::playerSetPosition(const Vec2& position)
{
	auto player = m_player.lock();
	if(player)
	{
		player.get()->setPosition(position);
	}
}

void Game::createClocks()
{
	//Might need to use srand(seed) instead of rand() to make this truly random, otherwise it will be the same for each match
	float speedX1 = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
	float speedY1 = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
	float speedX2 = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
	float speedY2 = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);

	m_gameObjects.push_back(std::make_shared<Clock>(Vec2(300.f, 200.f), 100.f, 0.f));
	m_gameObjects.back().get()->setVelocity(Vec2(speedX1, speedY1));

	m_gameObjects.push_back(std::make_shared<Clock>(Vec2(500.f, 200.f), 100.f, 0.f));
	m_gameObjects.back().get()->setVelocity(Vec2(-speedX2, -speedY2));
}
