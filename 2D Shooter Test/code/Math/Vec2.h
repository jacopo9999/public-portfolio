#pragma once

/* Class that represents a simple 2D vector of floats, with some useful arithmetic operations */
struct Vec2
{

	Vec2() :
		x(0.f),
		y(0.f)
	{}

	Vec2(float _x, float _y) :
		x(_x),
		y(_y)
	{}

	Vec2& operator+=(const Vec2& rhs)
	{
		x += rhs.x;
		y += rhs.y;
		return *this;
	}

	friend Vec2 operator+(Vec2 lhs, const Vec2 rhs)
	{
		lhs.x += rhs.x;
		lhs.y += rhs.y;
		return lhs;
	}

	Vec2& operator-=(const Vec2& rhs)
	{
		x += rhs.x;
		y += rhs.y;
		return *this;
	}

	friend Vec2 operator-(Vec2 lhs, const Vec2 rhs)
	{
		lhs.x += rhs.x;
		lhs.y += rhs.y;
		return lhs;
	}

	Vec2& operator/=(float scalar)
	{
		x /= scalar;
		y /= scalar;
		return *this;
	}

	friend Vec2 operator/(Vec2 lhs, float scalar)
	{
		lhs.x /= scalar;
		lhs.y /= scalar;
		return lhs;
	}

	Vec2& operator*=(float scalar)
	{
		x *= scalar;
		y *= scalar;
		return *this;
	}

	friend Vec2 operator*(Vec2 lhs, float scalar)
	{
		lhs.x *= scalar;
		lhs.y *= scalar;
		return lhs;
	}

	float x;
	float y;
};