#pragma once

#include <math.h>

#include "Vec2.h"

static const float k_PI = 3.1415926536f;

/* Utility class for static mathematical functions. Result vectors are passed as arguments for efficiency reasons. */
namespace Math {

	static float dotProduct(const Vec2& v1, const Vec2& v2)
	{
		return v1.x * v2.x + v1.y * v2.y;
	}

	static void getRotationVectorUnary(float rotationAngleRadians, /*OUT*/ Vec2& resultVector)
	{
		resultVector = Vec2(cos(rotationAngleRadians), sin(rotationAngleRadians));
	}

	static void rotatePointAroundCenter(const Vec2& point, const Vec2& center, float angleRadians, /*OUT*/ Vec2& resultPoint)
	{
		const float cosAngle = cos(angleRadians);
		const float sinAngle = sin(angleRadians);

		const float rotatedX = cosAngle * (point.x - center.x) - sinAngle * (point.y - center.y) + center.x;
		const float rotatedY = sinAngle * (point.x - center.x) + cosAngle * (point.y - center.y) + center.y;

		resultPoint = Vec2(rotatedX, rotatedY);
	}

	static void reflectVectorOnNormal(const Vec2& vector, const Vec2& normalVector, /*OUT*/ Vec2& resultVector)
	{
		const float dp = dotProduct(normalVector, vector);
		resultVector = Vec2(vector.x - 2 * dp * normalVector.x, vector.y - 2 * dp * normalVector.y);
	}
};
