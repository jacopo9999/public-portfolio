#pragma once

#include "GameObject.h"

/* Cannon represents the player in the game */
class Cannon : public GameObject {
public:
	Cannon(const Vec2& position, float rotationRadians);

	GameObjectsTypes getType() const { return GameObjectsTypes::CANNON; }

private:

};