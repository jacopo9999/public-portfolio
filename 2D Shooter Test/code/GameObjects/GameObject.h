#pragma once

#include <vector>
#include <memory>

#include <Framework\graphics.h>

#include <Math\Vec2.h>
#include <GameObjects\CollisionComponent\IBoundingBox.h>
#include <GameObjects\GameObjectsTypes.h>

struct Line
{
	Line() :
		v1(0),
		v2(0),
		colorRGB(0)
	{}

	Line(unsigned int _v1, unsigned int _v2, unsigned int _colorR, unsigned int _colorG, unsigned int _colorB) :
		v1(_v1),
		v2(_v2),
		colorRGB(GetRGB(_colorR, _colorG, _colorB))
	{}

	//Indices of the two points of the line
	unsigned int	v1;
	unsigned int	v2;

	//Line color
	unsigned int	colorRGB;
};

/* This class defines a shape in the game, composed of vertices and lines of different colours - default position and rotation are 0 */
class GameObject {
public:
	GameObject();
	GameObject(const Vec2& position, const std::vector<Vec2>& vertices);

	virtual void updateMovement();
	virtual void updateLogic() {} //This function needs to be overridden to add object-specific behaviour
	virtual GameObjectsTypes getType() const = 0;

	void draw();

	void setPosition(const Vec2& position); //Sets a specific position for the object
	void translate(const Vec2& position); //Modifies the position of the specified amount
	const Vec2& getPosition() const			{ return m_position; }

	void setRotationAngle(float rotationAngle); //Sets a specific rotation angle for the object
	void rotate(float rotationAngle); //Modifies the rotation angle of the specified amount
	const float getRotationAngle() const	{ return m_rotationAngleRadians; }

	void setVelocity (const Vec2& velocity);
	const Vec2& getVelocity() const;

	void setBoundingBox(std::shared_ptr<IBoundingBox> boundingBox);
	const std::shared_ptr<IBoundingBox> getBoundingBox() const;

	bool checkcollisionWithObject(const GameObject& gameObject, /*OUT*/ Vec2& collisionNormal = Vec2(0.f, 0.f));
	bool checkcollisionWithBounds(float minWidth, float maxWidth, float minHeight, float maxHeight, /*OUT*/ Vec2& collisionNormal = Vec2(0.f, 0.f));
	
	void reflectVelocityOnCollision(const Vec2& collisionNormal);

	void setMarkedForDeletion() { m_markedForDeletion = true; }
	bool isMarkedForDeletion() const { return m_markedForDeletion; }

#ifdef _DEBUG
	virtual void debugDraw();
#endif

protected:
	std::vector<Vec2>					m_vertices;
	std::vector<Line>					m_lines; //Connections between vertices - based on each vertex index

	Vec2								m_position;
	Vec2								m_velocity;

	float								m_rotationAngleRadians;

	bool								m_markedForDeletion; // This object has been marked to be deleted

	std::shared_ptr<IBoundingBox>		m_boundingBox; //Object may not have different types of or no Bounding Box

	void addVertex(float x, float y);
	void addLine(unsigned int v1, unsigned int v2, unsigned int colorR, unsigned int colorG, unsigned int colorB);
};