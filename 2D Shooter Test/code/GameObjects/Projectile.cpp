#include "Projectile.h"

#include <Math\Math.h>
#include <GameObjects\CollisionComponent\AxisAlignedBoundingBox.h>

#include <memory>

Projectile::Projectile(const Vec2& position, float length, float rotationRadians)
{
	addVertex(length/2, 0);
	addVertex(-length/2, 0);	

	addLine(0, 1, 255, 0, 0);

	translate(position);
	rotate(rotationRadians);

	setBoundingBox(std::make_shared<AxisAlignedBoundingBox>(m_vertices));
}