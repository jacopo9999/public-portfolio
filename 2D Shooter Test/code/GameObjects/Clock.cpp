#include "Clock.h"

#include <Framework\time.h>

#include <Math\Math.h>
#include <GameObjects\CollisionComponent\AxisAlignedBoundingBox.h>

Clock::Clock(const Vec2& position, float sideLength, float rotationRadians) :
	m_secondsVertex(nullptr),
	m_minutesVertex(nullptr),
	m_hoursVertex(nullptr),
	m_startingSecondsVertex(nullptr),
	m_startingMinutesVertex(nullptr),
	m_startingHoursVertex(nullptr),
	m_sideLength(sideLength)
{
	const unsigned int R = 255;
	const unsigned int G = 211;
	const unsigned int B = 0;

	//Make Clock Frame
	addVertex(-sideLength/2,	-sideLength/2);
	addVertex(sideLength/2,		-sideLength/2);
	addVertex(sideLength/2,		sideLength/2);
	addVertex(-sideLength/2,	sideLength/2);

	addLine(0, 1, R, G, B);
	addLine(1, 2, R, G, B);
	addLine(2, 3, R, G, B);
	addLine(3, 0, R, G, B);
	//

	//Make the segments that represent hours
	const float segmentLength = sideLength / 10;
	unsigned int vertexIndex = 4;
	for (float angle = 0; angle < 2 * k_PI; angle += k_PI / 6)
	{
		Vec2 p1(sideLength / 2,					0);
		Vec2 p2(sideLength / 2 - segmentLength,	0);

		Math::rotatePointAroundCenter(p1, Vec2(0.f, 0.f), angle, p1);
		Math::rotatePointAroundCenter(p2, Vec2(0.f, 0.f), angle, p2);

		addVertex(p1.x, p1.y);
		addVertex(p2.x, p2.y);

		addLine(vertexIndex, vertexIndex + 1, R, G, B);
		vertexIndex += 2;
	}
	//

	//Make the arms
	const float secondsArmLength = (6.f / 16) * sideLength;
	const float minutesArmLength = (5.f / 16) * sideLength;
	const float hoursArmLength = (4.f / 16) * sideLength;

	addVertex(0, 0);

	addVertex(0, -secondsArmLength);
	
	addLine(vertexIndex, vertexIndex + 1, 0, 255, 0); //Hours arm is RED

	addVertex(0, -minutesArmLength);

	addLine(vertexIndex, vertexIndex + 2, 0, 0, 255); //Minutes arm is BLUE

	addVertex(0, -hoursArmLength);

	addLine(vertexIndex, vertexIndex + 3, 255, 0, 0); //Seconds arm is GREEN

	//Add these references for quick access
	m_secondsVertex = &m_vertices[m_vertices.size() - 3];
	m_minutesVertex = &m_vertices[m_vertices.size() - 2];
	m_hoursVertex = &m_vertices[m_vertices.size() - 1];


	//Add the reference points for 0h 0m 0s
	addVertex(0, -secondsArmLength);
	addVertex(0, -minutesArmLength);
	addVertex(0, -hoursArmLength);

	m_startingSecondsVertex = &m_vertices[m_vertices.size() - 3];
	m_startingMinutesVertex = &m_vertices[m_vertices.size() - 2];
	m_startingHoursVertex = &m_vertices[m_vertices.size() - 1];
	//

	translate(position);
	rotate(rotationRadians);

	setBoundingBox(std::make_shared<AxisAlignedBoundingBox>(m_vertices));
}

void Clock::updateLogic() 
{
	int hr;
	int min;
	int sec;

	GetTime(hr, min, sec);

	const float secondsAngle = sec * 6.f;							//(sec * 360) / 60
	const float minutesAngle = min * 6.f + sec/2.f;					//(min * 360) / 60 + ((sec / 60.f) * 30.f)
	const float hoursAngle = (hr % 12) * 30.f + min/2.f;			//(((hrs % 12) * 360) / 12) + ((min / 60.f) * 30.f)

	Math::rotatePointAroundCenter(*m_startingSecondsVertex, m_position, secondsAngle * (k_PI / 180), *m_secondsVertex);
	Math::rotatePointAroundCenter(*m_startingMinutesVertex, m_position, minutesAngle * (k_PI / 180), *m_minutesVertex);
	Math::rotatePointAroundCenter(*m_startingHoursVertex, m_position, hoursAngle * (k_PI / 180), *m_hoursVertex);
}