#include "AxisAlignedBoundingBox.h"

#include <assert.h>

#include <Framework\graphics.h>

AxisAlignedBoundingBox::AxisAlignedBoundingBox(const std::vector<Vec2>& vertices)
{
	update(vertices);
}

AxisAlignedBoundingBox::AxisAlignedBoundingBox(const Vec2& topLeftVertex, const Vec2& bottomRightVertex) :
	m_topLeftVertex(topLeftVertex),
	m_bottomRightVertex(bottomRightVertex)
{}

bool AxisAlignedBoundingBox::checkcollisionWithBounds(float minWidth, float maxWidth, float minHeight, float maxHeight, /*OUT*/ Vec2& collisionNormal) const
{
	collisionNormal = Vec2(0.f, 0.f);

	if (m_bottomRightVertex.x > maxWidth)
	{
		collisionNormal = Vec2(-1.f, 0.f);
		return true;
	}
	else if (m_topLeftVertex.x < minWidth)
	{
		collisionNormal = Vec2(1.f, 0.f);
		return true;
	}
	else if (m_bottomRightVertex.y > maxHeight)
	{
		collisionNormal = Vec2(0.f, -1.f);
		return true;
	}
	else if (m_topLeftVertex.y < minHeight)
	{
		collisionNormal = Vec2(0.f, 1.f);
		return true;
	}

	return false;
}

bool AxisAlignedBoundingBox::checkcollision(const std::shared_ptr<IBoundingBox> boundingBox, const Vec2& velocity, /*OUT*/ Vec2& collisionNormal) const
{
	collisionNormal = Vec2();

	if (boundingBox == nullptr)
	{
		return false;
	}

	if (boundingBox->getType() == BoundingBoxType::AABB)
	{
		const AxisAlignedBoundingBox* aabb = static_cast<const AxisAlignedBoundingBox*>(boundingBox.get());
		
		const bool isOutsideBottom = aabb->m_topLeftVertex.y > this->m_bottomRightVertex.y;
		const bool isOutsideRight = aabb->m_topLeftVertex.x > this->m_bottomRightVertex.x;
		const bool isOutsideLeft = aabb->m_bottomRightVertex.x < this->m_topLeftVertex.x;
		const bool isOutsideTop = aabb->m_bottomRightVertex.y < this->m_topLeftVertex.y;

		if (isOutsideBottom || isOutsideTop || isOutsideLeft || isOutsideRight) // No collision is going to happen
		{
			return false;
		}

		const Vec2 previousBottomRightVertex = m_bottomRightVertex - velocity;
		const Vec2 previousTopLeftVertex = m_topLeftVertex - velocity;
		const Vec2 otherPreviousBottomRightVertex = aabb->m_bottomRightVertex - velocity;
		const Vec2 otherPreviousTopLeftVertex = aabb->m_topLeftVertex - velocity;

		const bool wasOutsideBottom = otherPreviousTopLeftVertex.y > this->m_bottomRightVertex.y;
		const bool wasOutsideRight = otherPreviousTopLeftVertex.x > this->m_bottomRightVertex.x;
		const bool wasOutsideLeft = otherPreviousBottomRightVertex.x < this->m_topLeftVertex.x;
		const bool wasOutsideTop = otherPreviousBottomRightVertex.y < this->m_topLeftVertex.y;

		if (!isOutsideLeft && wasOutsideLeft)
		{
			collisionNormal = Vec2(-1.f, 0.f);
		}
		if (!isOutsideRight && wasOutsideRight)
		{
			collisionNormal = Vec2(1.f, 0.f);
		}
		if (!isOutsideTop && wasOutsideTop)
		{
			collisionNormal = Vec2(0.f, -1.f);
		}
		if (!isOutsideBottom && wasOutsideBottom)
		{
			collisionNormal = Vec2(0.f, 1.f);
		}

		return true;
	}

	assert(false); // Other Bounding Box type not recognised!
	return false;
}

void AxisAlignedBoundingBox::update(const std::vector<Vec2>& vertices)
{
	if (vertices.empty())
	{
		return;
	}

	m_topLeftVertex = vertices.back();
	m_bottomRightVertex = vertices.back();

	for (const Vec2& vertex : vertices)
	{
		if (vertex.x < m_topLeftVertex.x)
		{
			m_topLeftVertex.x = vertex.x;
		}
		if (vertex.y < m_topLeftVertex.y)
		{
			m_topLeftVertex.y = vertex.y;
		}
		if (vertex.x > m_bottomRightVertex.x)
		{
			m_bottomRightVertex.x = vertex.x;
		}
		if (vertex.y > m_bottomRightVertex.y)
		{
			m_bottomRightVertex.y = vertex.y;
		}
	}
}

#ifdef _DEBUG
void AxisAlignedBoundingBox::debugDraw()
{
	const Vec2 topRightVertex(m_bottomRightVertex.x, m_topLeftVertex.y);
	const Vec2 bottomLeftVertex(m_topLeftVertex.x, m_bottomRightVertex.y);

	DrawLine((int)m_topLeftVertex.x,		(int)m_topLeftVertex.y,			(int)topRightVertex.x,			(int)topRightVertex.y,			GetRGB(255, 0, 0));
	DrawLine((int)topRightVertex.x,			(int)topRightVertex.y,			(int)m_bottomRightVertex.x,		(int)m_bottomRightVertex.y,		GetRGB(255, 0, 0));
	DrawLine((int)m_bottomRightVertex.x,	(int)m_bottomRightVertex.y,		(int)bottomLeftVertex.x,		(int)bottomLeftVertex.y,		GetRGB(255, 0, 0));
	DrawLine((int)bottomLeftVertex.x,		(int)bottomLeftVertex.y,		(int)m_topLeftVertex.x,			(int)m_topLeftVertex.y,			GetRGB(255, 0, 0));

}
#endif