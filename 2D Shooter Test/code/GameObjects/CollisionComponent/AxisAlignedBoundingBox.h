#pragma once

#include "IBoundingBox.h"

#include <Math\Vec2.h>

class AxisAlignedBoundingBox : public IBoundingBox {

public:
	AxisAlignedBoundingBox(const std::vector<Vec2>& vertices);
	AxisAlignedBoundingBox(const Vec2& topLeftVertex, const Vec2& bottomRightVertex);

	/*
	Checks whether our object will be colliding with another object assuming an AABB. We may use the SAT algorithm to
	take into account rotated bounding boxes, I am not implementing SAT given the time-constraints of this test.
	We could also improve collision detection time by using a quad tree to check only the collisions between objects in
	the same quad, instead of comparing all the objects as I am doing now, this would definitely be more efficient to do.
	The collision algorithm uses the velocity to determine which side the collision comes from even though it doesn't sweep, which means
	that it may potentially miss collisions with smaller objects if its velocity is high.
	*/
	bool checkcollision(const std::shared_ptr<IBoundingBox> boundingBox, const Vec2& velocity, /*OUT*/ Vec2& collisionNormal) const override;

	bool checkcollisionWithBounds(float minWidth, float maxWidth, float minHeight, float maxHeight, /*OUT*/ Vec2& collisionNormal) const override;

	void update(const std::vector<Vec2>& vertices) override;

#ifdef _DEBUG
	void debugDraw() override;
#endif

	BoundingBoxType getType() const override { return BoundingBoxType::AABB; }

	const Vec2& getTopLeft() const		{ return m_topLeftVertex; }
	const Vec2& getBottomRight() const	{ return m_bottomRightVertex; }

	const float getWidth() const		{ return abs(m_topLeftVertex.x - m_bottomRightVertex.x); }
	const float getHeight() const		{ return abs(m_topLeftVertex.y - m_bottomRightVertex.y); }

private:

	Vec2 m_topLeftVertex;
	Vec2 m_bottomRightVertex;

	Vec2 m_previousVelocity;
};