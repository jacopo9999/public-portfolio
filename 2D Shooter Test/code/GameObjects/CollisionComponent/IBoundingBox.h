#pragma once

#include <vector>
#include <memory>

#include <Math\Vec2.h>

#include <GameObjects\CollisionComponent\BoundingBoxesTypes.h>

/* An interface which is common to bounding boxes of different types and shapes. Useful to quickly assign different shapes to different GameObjects */
class IBoundingBox {
public:
	/* 
	Checks if this collision box is colliding with another and uses the velocity vector to determine where the collision happened, 
	returns the normal vector of surface where the collision happened
	*/
	virtual bool checkcollision(const std::shared_ptr<IBoundingBox> boundingBox, const Vec2& velocity, /*OUT*/ Vec2& collisionNormal = Vec2()) const = 0;

	/* 
	Checks if this collision box is going to collide with x and y bounds, returns the normal vector to the surface where the collision happened 
	*/
	virtual bool checkcollisionWithBounds(float minWidth, float maxWidth, float minHeight, float maxHeight, /*OUT*/ Vec2& collisionNormal = Vec2()) const = 0;

	/* 
	Updates the collision box - needs to be called when translating or rotating an object 
	*/
	virtual void update(const std::vector<Vec2>& vertices) = 0;

	/* 
	Returns the type of the Bounding Box 
	*/
	virtual BoundingBoxType getType() const = 0;

#ifdef _DEBUG
	/* Draws the bounding box for debug */
	virtual void debugDraw() = 0;
#endif
};