#pragma once

#include "GameObject.h"

#include <Math\Vec2.h>

/* Represents a clock in the game that shows the current time, the red arm are the hours, the blue one the minutes and the green one the seconds. */
class Clock : public GameObject {
public:
	Clock(const Vec2& position, float sideLength, float rotationRadians);

	GameObjectsTypes getType() const { return GameObjectsTypes::CLOCK; }

	float getSideLength() const { return m_sideLength; }

	void updateLogic() override;

private:
	Vec2* m_secondsVertex;
	Vec2* m_minutesVertex;
	Vec2* m_hoursVertex;

	Vec2* m_startingSecondsVertex;
	Vec2* m_startingMinutesVertex;
	Vec2* m_startingHoursVertex;

	float m_sideLength;

};
