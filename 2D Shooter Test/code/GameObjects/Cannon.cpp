#include "Cannon.h"

#include <GameObjects\CollisionComponent\AxisAlignedBoundingBox.h>

#include <memory>

Cannon::Cannon(const Vec2& position, float rotationRadians)
{
	addVertex(0, -15);
	addVertex(-10, 15);
	addVertex(10, 15);

	addLine(0, 1, 0, 255, 0);
	addLine(1, 2, 255, 0, 0);
	addLine(2, 0, 0, 0, 255);

	translate(position);
	rotate(rotationRadians);

	setBoundingBox(std::make_shared<AxisAlignedBoundingBox>(m_vertices));
}