#pragma once

#include "GameObject.h"

#include <Math\Vec2.h>

/*Represents a projectile in the game, position is the center of the line representing the projectile*/
class Projectile : public GameObject {
public:
	Projectile(const Vec2& position, float length, float rotationRadians);

	GameObjectsTypes getType() const { return GameObjectsTypes::PROJECTILE; }

private:


};
