#include "GameObject.h"

#include <Framework\graphics.h>

#include <Math\Math.h>

GameObject::GameObject() :
	m_rotationAngleRadians(0.f),
	m_markedForDeletion(false)
{

}

GameObject::GameObject(const Vec2& position, const std::vector<Vec2>& vertices) :
	m_vertices(vertices),
	m_position(position),
	m_rotationAngleRadians(0.f),
	m_markedForDeletion(false)
{
	if (m_boundingBox)
	{
		m_boundingBox.get()->update(m_vertices);
	}
}

void GameObject::addVertex(float x, float y)
{
	m_vertices.push_back(Vec2(x, y));

	if (m_boundingBox)
	{
		m_boundingBox.get()->update(m_vertices);
	}
}

void GameObject::addLine(unsigned int v1, unsigned int v2, unsigned int colorR, unsigned int colorG, unsigned int colorB)
{
	m_lines.push_back(Line(v1, v2, colorR, colorG, colorB));
}

void GameObject::setPosition(const Vec2& position)
{
	Vec2 positionDifference;
	positionDifference = position - m_position;
	translate(positionDifference);
}

void GameObject::translate(const Vec2& position)
{
	m_position += position;

	for (Vec2& vertex : m_vertices)
	{
		vertex += position;
	}

	if (m_boundingBox)
	{
		m_boundingBox.get()->update(m_vertices);
	}
}

void GameObject::setRotationAngle(float rotationAngleRadians)
{
	float angleDifference = rotationAngleRadians - m_rotationAngleRadians;
	rotate(angleDifference);
}

void GameObject::rotate(float rotationAngleRadians)
{
	m_rotationAngleRadians += rotationAngleRadians;

	for (Vec2& vertex : m_vertices)
	{
		Math::rotatePointAroundCenter(vertex, m_position, rotationAngleRadians, vertex);
	}

	if (m_boundingBox)
	{
		m_boundingBox.get()->update(m_vertices);
	}
}

void GameObject::draw()
{
	for (std::vector<Line>::const_iterator lineIter = m_lines.cbegin(); lineIter != m_lines.cend(); ++lineIter)
	{
		const Line& line = *lineIter;
		DrawLine((int)m_vertices[line.v1].x, (int)m_vertices[line.v1].y, (int)m_vertices[line.v2].x, (int)m_vertices[line.v2].y, line.colorRGB);
	}

#ifdef _DEBUG
	debugDraw();
#endif
}

void GameObject::setVelocity(const Vec2& velocity)
{
	m_velocity = velocity;
}

const Vec2& GameObject::getVelocity() const
{
	return m_velocity;
}

void GameObject::updateMovement()
{
	translate(m_velocity);
}

bool GameObject::checkcollisionWithObject(const GameObject& gameObject, /*OUT*/ Vec2& collisionNormal)
{
	//If any of the two objects has no bounding box, no collision happens
	if (m_boundingBox == nullptr || gameObject.getBoundingBox() == nullptr)
	{
		return false;
	}

	return m_boundingBox.get()->checkcollision(gameObject.getBoundingBox(), m_velocity, collisionNormal);
}

bool GameObject::checkcollisionWithBounds(float minWidth, float maxWidth, float minHeight, float maxHeight, /*OUT*/ Vec2& collisionNormal /*= Vec2()*/)
{
	if (m_boundingBox == nullptr)
	{
		return false;
	}

	return m_boundingBox.get()->checkcollisionWithBounds(minWidth, maxWidth, minHeight, maxHeight, collisionNormal);
}

const std::shared_ptr<IBoundingBox> GameObject::getBoundingBox() const
{
	return m_boundingBox;
}

void GameObject::setBoundingBox(std::shared_ptr<IBoundingBox> boundingBox)
{
	m_boundingBox = boundingBox;
}

void GameObject::reflectVelocityOnCollision(const Vec2& collisionNormal)
{
	Math::reflectVectorOnNormal(m_velocity, collisionNormal, m_velocity);
}

#ifdef _DEBUG
void GameObject::debugDraw()
{
	if (m_boundingBox)
	{
		m_boundingBox->debugDraw();
	}
}
#endif