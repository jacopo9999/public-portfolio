#include "C_Application.h"

#include "Framework\time.h"
#include "Framework\graphics.h"

#include <Math\Math.h>

C_Application::C_Application(int screenWidth, int screenHeight)
	: m_ScreenWidth(screenWidth)
	, m_ScreenHeight(screenHeight)
	, m_game(screenWidth, screenHeight)
{
	m_game.playerSetPosition(Vec2(float(screenWidth)/2, (7.f/8)*float(screenHeight)));
}


C_Application::~C_Application()
{

}

void C_Application::ClearScreen()
{
	FillRect(0, 0, m_ScreenWidth, m_ScreenHeight, GetRGB(0, 0, 0));
}

void C_Application::Tick(T_PressedKey pressedKeys)
{
	static int FiringTicksCounter = -1;

	// Clear screen
	ClearScreen();

	// Key processing
	if(pressedKeys & s_KeyLeft)
	{
		m_game.playerRotateCounterClockwise(0.02f);
	}

	if(pressedKeys & s_KeyRight)
	{
		m_game.playerRotateClockwise(0.02f);
	}

	if(pressedKeys & s_KeySpace)
	{
		if (FiringTicksCounter == -1) //Avoid firing multiple shots while keeping spacebar pressed
		{
			m_game.playerFire();
			FiringTicksCounter = 0;
		}
	}

	if (FiringTicksCounter >= 0)
	{
		FiringTicksCounter++;
	}
	if (FiringTicksCounter >= 20)
	{
		FiringTicksCounter = -1;
	}

	// Update Game Logic
	m_game.update();

	// Draw Game Objects
	m_game.drawGameObjects();
}
